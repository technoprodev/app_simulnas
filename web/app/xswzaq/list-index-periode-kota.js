$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables-periode-kota');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('xswzaq/datatables-periode-kota'),
                    type: 'POST',
                    /*data: function (d) {
                        console.log(d);
                        return $.extend(d, {});
                    },*/
                },
                columns: [
                    {
                        data: {
                            _: 'r.name',
                            filter: 'r.name',
                            sort: 'r.name',
                            display: 'kota',
                        },
                        render: function ( data, type, row ) {
                            return '<div class="text-azure">' +
                                '<i class="fa fa-map-marker margin-right-5"></i>' +
                                '<span class="">' + (data ? data.replace(
                                    /\w\S*/g, function(txt) {
                                        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                                    }) : '') +
                                '</span>' +
                            '</div>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: 'kode',
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="text-red">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: 'nama',
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="text-dark">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: 'status',
                        render: function ( data, type, row ) {
                            color = (data == 'Tidak Aktif') ? 'red' : data == 'Sedang Aktif' ? 'azure' : 'azure'
                            return '' +
                                '<div class="text-' + color + '">' +
                                '    <i class="fa fa-circle margin-right-5"></i>' + data +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {data: 'alamat'},
                    {
                        data: 'kuota',
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="text-azure">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            status = '';
                            if (row.status == 'Sedang Aktif') {
                                status = '<a href="' + fn.urlTo('xswzaq/nonaktifkan-periode-kota', {id: data}) + '" data-confirm="Apakah Anda yakin menonaktifkan kota ini ?" data-method="post"><i class="fa fa-toggle-on bg-light-azure padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>';
                            } else {
                                status = '<a href="' + fn.urlTo('xswzaq/aktifkan-periode-kota', {id: data}) + '" data-confirm="Apakah Anda yakin mengaktifkan kota ini ?" data-method="post"><i class="fa fa-toggle-off bg-light-red padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>';
                            }
                                
                            return '<div class="fs-14">' +
                                '<a href="' + fn.urlTo('xswzaq/detail-periode-kota', {id: data}) + '" modal-md="" modal-title="Detail Kota"><i class="fa fa-eye bg-lighter padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>' +
                                '<a href="' + fn.urlTo('xswzaq/update-periode-kota', {id: data}) + '"><i class="fa fa-pencil bg-light-cyan padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>' +
                                status +
                                '</div>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});