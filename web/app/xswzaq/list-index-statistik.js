$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables-statistik');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('xswzaq/datatables-statistik'),
                    type: 'POST'
                },
                pageLength: -1,
                dom: '<"clearfix margin-bottom-30"lB><"clearfix margin-bottom-30 scroll-x"rt>',
                columns: [
                    {
                        data: 'kota',
                        searchable: false,
                        render: function ( data, type, row ) {
                            return '<div class="text-red">' + data + '</div>';
                        },
                    },
                    {
                        data: 'transaksi_belum_bayar',
                        searchable: false,
                        render: function ( data, type, row ) {
                            return '<div class="text-center text-azure">' + data + '</div>';
                        },
                    },
                    {
                        data: 'transaksi_dalam_proses_konfirmasi',
                        searchable: false,
                        render: function ( data, type, row ) {
                            return '<div class="text-center text-azure">' + data + '</div>';
                        },
                    },
                    {
                        data: 'transaksi_sudah_bayar',
                        searchable: false,
                        render: function ( data, type, row ) {
                            return '<div class="text-center text-azure">' + data + '</div>';
                        },
                    },
                    {
                        data: 'transaksi_ditolak',
                        searchable: false,
                        render: function ( data, type, row ) {
                            return '<div class="text-center text-azure">' + data + '</div>';
                        },
                    },
                    {
                        data: 'total_transaksi',
                        searchable: false,
                        render: function ( data, type, row ) {
                            return '<div class="text-center text-azure fw-bold">' + data + '</div>';
                        },
                    },
                    {
                        data: 'tiket_belum_bayar',
                        searchable: false,
                        render: function ( data, type, row ) {
                            return '<div class="text-center text-chartreuse">' + data + '</div>';
                        },
                    },
                    {
                        data: 'tiket_dalam_proses_konfirmasi',
                        searchable: false,
                        render: function ( data, type, row ) {
                            return '<div class="text-center text-chartreuse">' + data + '</div>';
                        },
                    },
                    {
                        data: 'tiket_sudah_bayar',
                        searchable: false,
                        render: function ( data, type, row ) {
                            return '<div class="text-center text-chartreuse">' + data + '</div>';
                        },
                    },
                    {
                        data: 'tiket_ditolak',
                        searchable: false,
                        render: function ( data, type, row ) {
                            return '<div class="text-center text-chartreuse">' + data + '</div>';
                        },
                    },
                    {
                        data: 'total_tiket',
                        searchable: false,
                        render: function ( data, type, row ) {
                            return '<div class="text-center text-chartreuse fw-bold">' + data + '</div>';
                        },
                    },
                    {
                        data: 'tiket_saintek_ipa',
                        searchable: false,
                        render: function ( data, type, row ) {
                            return '<div class="text-center text-rose">' + data + '</div>';
                        },
                    },
                    {
                        data: 'tiket_soshum_ips',
                        searchable: false,
                        render: function ( data, type, row ) {
                            return '<div class="text-center text-rose">' + data + '</div>';
                        },
                    },
                    {
                        data: 'duta_transaksi',
                        searchable: false,
                        render: function ( data, type, row ) {
                            return '<div class="text-center text-orange">' + data + '</div>';
                        },
                    },
                    {
                        data: 'duta_tiket',
                        searchable: false,
                        render: function ( data, type, row ) {
                            return '<div class="text-center text-orange">' + data + '</div>';
                        },
                    },
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                if (index == 3) {
                    var that = this;
                    
                    var timer = null;
                    dtSearch.find('th:nth-child(' + (index + 1) + ') select').on('keyup change', function(){
                        var val = {
                            that : that,
                            this : this,
                        };

                        clearTimeout(timer); 
                           timer = setTimeout(function () {
                            if(val.that.search() !== val.this.value){
                                val.that.search( val.this.value ).draw();
                            }
                        }, 500);
                    });
                } else {
                    var that = this;
                    
                    var timer = null;
                    dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                        var val = {
                            that : that,
                            this : this,
                        };

                        clearTimeout(timer); 
                           timer = setTimeout(function () {
                            if(val.that.search() !== val.this.value){
                                val.that.search( val.this.value ).draw();
                            }
                        }, 500);
                    });
                }
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});