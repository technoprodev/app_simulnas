$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables-periode-jenis');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('xswzaq/datatables-periode-jenis'),
                    type: 'POST',
                    /*data: function (d) {
                        console.log(d);
                        return $.extend(d, {});
                    },*/
                },
                columns: [
                    {
                        data: 'nama',
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="text-azure">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: 'periode_penjualan',
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="text-red">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: 'status',
                        render: function ( data, type, row ) {
                            color = (data == 'Tidak Aktif') ? 'red' : data == 'Sedang Aktif' ? 'azure' : 'azure'
                            return '' +
                                '<div class="text-' + color + '">' +
                                '    <i class="fa fa-circle margin-right-5"></i>' + data +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: 'harga_1_tiket',
                        render: function ( data, type, row ) {
                            return '' +
                                '<div class="fw-bold">' +
                                '    <span class="margin-right-2 text-gray">Rp </span>' +
                                '    <span class="text-spring">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: 'harga_2_tiket',
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="margin-right-2 text-gray">Rp </span>' +
                                '    <span class="text-spring">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: 'harga_3_tiket',
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="margin-right-2 text-gray">Rp </span>' +
                                '    <span class="text-spring">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: 'harga_4_tiket',
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="margin-right-2 text-gray">Rp </span>' +
                                '    <span class="text-spring">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: 'harga_5_tiket',
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="margin-right-2 text-gray">Rp </span>' +
                                '    <span class="text-spring">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: 'harga_6_tiket',
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="margin-right-2 text-gray">Rp </span>' +
                                '    <span class="text-spring">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: 'harga_7_tiket',
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="margin-right-2 text-gray">Rp </span>' +
                                '    <span class="text-spring">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: 'harga_8_tiket',
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="margin-right-2 text-gray">Rp </span>' +
                                '    <span class="text-spring">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: 'harga_9_tiket',
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="margin-right-2 text-gray">Rp </span>' +
                                '    <span class="text-spring">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: 'harga_10_tiket',
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="margin-right-2 text-gray">Rp </span>' +
                                '    <span class="text-spring">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            status = '';
                            if (row.status == 'Sedang Aktif') {
                                status = '<a href="' + fn.urlTo('xswzaq/nonaktifkan-periode-jenis', {id: data}) + '" data-confirm="Apakah Anda yakin menonaktifkan jenis ini ?" data-method="post"><i class="fa fa-toggle-on bg-light-azure padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>';
                            } else {
                                status = '<a href="' + fn.urlTo('xswzaq/aktifkan-periode-jenis', {id: data}) + '" data-confirm="Apakah Anda yakin mengaktifkan jenis ini ?" data-method="post"><i class="fa fa-toggle-off bg-light-red padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>';
                            }
                                
                            return '<div class="fs-14">' +
                                '<a href="' + fn.urlTo('xswzaq/detail-periode-jenis', {id: data}) + '" modal-md="" modal-title="Detail Jenis, Periode & Harga"><i class="fa fa-eye bg-lighter padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>' +
                                '<a href="' + fn.urlTo('xswzaq/update-periode-jenis', {id: data}) + '"><i class="fa fa-pencil bg-light-cyan padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>' +
                                status +
                                '</div>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});