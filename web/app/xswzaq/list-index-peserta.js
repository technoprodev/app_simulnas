$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables-peserta');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('xswzaq/datatables-peserta'),
                    type: 'POST',
                    /*data: function (d) {
                        console.log(d);
                        return $.extend(d, {});
                    },*/
                },
                columns: [
                    {
                        data: 'status_aktif',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            statusAktif = '';
                            if (data == 'Aktif') {
                                statusAktif = '<a href="' + fn.urlTo('xswzaq/nonaktifkan-peserta', {id: row.id}) + '" class="inline-block" data-confirm="Apakah Anda yakin menonaktifkan peserta ini ?" data-method="post"><i style="width:35px;" class="fa fa-toggle-on bg-light-azure padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>';
                            } else {
                                statusAktif = '<a href="' + fn.urlTo('xswzaq/aktifkan-peserta', {id: row.id}) + '" class="inline-block" data-confirm="Apakah Anda yakin mengaktifkan peserta ini ?" data-method="post"><i style="width:35px;" class="fa fa-toggle-off bg-light-red padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>';
                            }
                            resendEmail = '';
                            if (row.status == 'Belum Bayar') {
                                resendEmail = '        <li><a href="' + fn.urlTo('xswzaq/resend-pendaftaran', {id: row.id}) + '">Resend Email Pendaftaran</a></li>';
                            } else if (row.status == 'Dalam Proses Konfirmasi') {
                                resendEmail = '        <li><a href="' + fn.urlTo('xswzaq/resend-konfirmasi', {id: row.id}) + '">Resend Email Pemintaan Konfirmasi</a></li>';
                            }
                            if (row.status == 'Sudah Bayar') {
                                resendEmail = '        <li><a href="' + fn.urlTo('xswzaq/resend-diterima', {id: row.id}) + '">Resend Email Konfirmasi Diterima</a></li>';
                            } else if (row.status == 'Ditolak') {
                                resendEmail = '        <li><a href="' + fn.urlTo('xswzaq/resend-ditolak', {id: row.id}) + '">Resend Email Konfirmasi Ditolak</a></li>';
                            }
                                
                            return '<div class="fs-14">' +
                                '<a href="' + fn.urlTo('xswzaq/detail-peserta', {id: row.id}) + '" modal-md="" modal-title="Detail Peserta"><i style="width:35px;" class="fa fa-eye bg-lighter padding-x-10 padding-y-5 rounded-md margin-right-5 margin-bottom-5"></i></a>' +
                                '<a href="' + fn.urlTo('xswzaq/update-peserta', {id: row.id}) + '" class="inline-block"><i style="width:35px;" class="fa fa-pencil bg-light-cyan padding-x-10 padding-y-5 rounded-md margin-right-5 margin-bottom-5"></i></a>' +
                                '<br>' +
                                statusAktif +
                                '<div class="dropdown inline-block">' +
                                '    <a class="hover-pointer inline-block" data-toggle="dropdown">' +
                                '        <i style="width:35px;" class="fa fa-envelope bg-light-orange padding-x-10 padding-y-5 rounded-md margin-right-5"></i>' +
                                '    </a>' +
                                '    <ul class="dropdown-menu">' +
                                resendEmail +
                                '    </ul>' +
                                '</div>' +
                                '</div>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.nama',
                            filter: 'p.nama',
                            sort: 'p.nama',
                            display: 'nama',
                        },
                        render: function ( data, type, row ) {
                            firstLetter = row.nama.match(/\b(\w)/g)[0].toUpperCase();
                            color = '';
                            if (firstLetter >= 'A' && firstLetter <= 'C') {
                                color = 'azure';
                            } else if (firstLetter >= 'D' && firstLetter <= 'F') {
                                color = 'spring';
                            } else if (firstLetter >= 'G' && firstLetter <= 'H') {
                                color = 'red';
                            } else if (firstLetter >= 'I' && firstLetter <= 'J') {
                                color = 'yellow';
                            } else if (firstLetter >= 'K' && firstLetter <= 'L') {
                                color = 'orange';
                            } else if (firstLetter >= 'M' && firstLetter <= 'N') {
                                color = 'chartreuse';
                            } else if (firstLetter >= 'O' && firstLetter <= 'P') {
                                color = 'green';
                            } else if (firstLetter >= 'Q' && firstLetter <= 'R') {
                                color = 'cyan';
                            } else if (firstLetter >= 'S' && firstLetter <= 'T') {
                                color = 'blue';
                            } else if (firstLetter >= 'U' && firstLetter <= 'V') {
                                color = 'violet';
                            } else if (firstLetter >= 'W' && firstLetter <= 'X') {
                                color = 'magenta';
                            } else if (firstLetter >= 'Y' && firstLetter <= 'Z') {
                                color = 'rose';
                            }
                            return '' +
                                '        <span class="circle-icon margin-right-5 fs-11 bg-' + color + ' border-transparent" style="width: 30px; height: 30px; line-height: 28px;">' + row.nama.match(/\b(\w)/g).slice(0, 2).join('').toUpperCase() + '</span>' +
                                '        <span class="fs-14 text-dark">' + row.nama + '</span>' +
                                
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.kode',
                            filter: 'p.kode',
                            sort: 'p.kode',
                            display: 'kode',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="text-red">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: 'email',
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <i class="fa fa-envelope margin-right-5 text-gray" style="width: 10px;"></i>' +
                                '    <span class="text-dark">' + row.email + '</span>' +
                                '</div>' +
                                '<div>' +
                                '    <i class="fa fa-phone margin-right-5 text-gray" style="width: 10px;"></i>' +
                                '    <span class="text-dark">' + row.handphone + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: 'jumlah_tiket',
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <i class="fa fa-ticket margin-right-5 text-gray"></i>' +
                                '    <span class="text-dark">' + data + ' tiket</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.tagihan',
                            filter: 'p.tagihan',
                            sort: 'p.tagihan',
                            display: 'tagihan',
                            equal: true,
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="margin-right-2 text-gray">Rp </span>' +
                                '    <span class="text-spring">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.id_periode_kota',
                            filter: 'pk.nama',
                            sort: 'p.id_periode_kota',
                            display: 'lokasi',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<i class="fa fa-map-marker margin-right-5 text-gray"></i>' +
                                '<span class="text-dark">' + data + '</span>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.id_request_kota',
                            filter: 'r.name',
                            sort: 'p.id_request_kota',
                            display: 'request',
                        },
                        visible: false,
                        render: function ( data, type, row ) {
                            return '' +
                                '<span class="text-dark">' + (data ? '<i class="fa fa-map-marker margin-right-5 text-gray"></i>' + data.replace(
                                    /\w\S*/g, function(txt) {
                                        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                                    }) : '') + '</span>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.status_bayar',
                            filter: 'p.status_bayar',
                            sort: 'p.status_bayar',
                            display: 'status',
                        },
                        render: function ( data, type, row ) {
                            color = (data == 'Belum Bayar' || data == 'Ditolak') ? 'red' : data == 'Dalam Proses Konfirmasi' ? 'orange' : 'azure'
                            konfirmasi = '';
                            if (data == 'Dalam Proses Konfirmasi') {
                                konfirmasi = '<div class="margin-top-5"><a href="' + fn.urlTo('xswzaq/konfirmasi', {id: row.id}) + '" modal-lg="" modal-title="Konfirmasi Pembayaran ' + row.kode + '" class="button button-xs border-light-azure text-azure">Konfirmasi</a></div>';
                            }
                            return '' +
                                '<div class="text-' + color + '">' +
                                '    <i class="fa fa-circle margin-right-5"></i>' + data +
                                '</div>' +
                                konfirmasi;
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.status_duta',
                            filter: 'p.status_duta',
                            sort: 'p.status_duta',
                            display: 'status_duta',
                        },
                        render: function ( data, type, row ) {
                            statusDuta = '';
                            if (data == 'Sudah Aktif') {
                                statusDuta = '<span class="text-azure"><i class="fa fa-circle margin-right-5"></i> Sudah Aktif</span><div class="margin-top-5"><a href="' + fn.urlTo('xswzaq/nonaktifkan-duta', {id: row.id}) + '" data-confirm="Apakah Anda yakin menonaktifkan duta ini ?" data-method="post" class="button button-xs border-light-red text-red">Nonaktifkan</a></div>';
                            } else {
                                statusDuta = '<span class="text-red"><i class="fa fa-circle margin-right-5"></i> Tidak Aktif</span><div class="margin-top-5"><a href="' + fn.urlTo('xswzaq/aktifkan-duta', {id: row.id}) + '" data-confirm="Apakah Anda yakin mengaktifkan duta ini ?" data-method="post" class="button button-xs border-light-azure text-azure">Aktifkan</a></div>';
                            }
                                
                            return '<div class="fs-13">' +
                                statusDuta +
                                '</div>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
                order: [[5, 'desc']],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                if (index == 3) {
                    var that = this;
                    
                    var timer = null;
                    dtSearch.find('th:nth-child(' + (index + 1) + ') select').on('keyup change', function(){
                        var val = {
                            that : that,
                            this : this,
                        };

                        clearTimeout(timer); 
                           timer = setTimeout(function () {
                            if(val.that.search() !== val.this.value){
                                val.that.search( val.this.value ).draw();
                            }
                        }, 500);
                    });
                } else {
                    var that = this;
                    
                    var timer = null;
                    dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                        var val = {
                            that : that,
                            this : this,
                        };

                        clearTimeout(timer); 
                           timer = setTimeout(function () {
                            if(val.that.search() !== val.this.value){
                                val.that.search( val.this.value ).draw();
                            }
                        }, 500);
                    });
                }
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});