<?php
$params = [
    'user.passwordResetTokenExpire' => 3600,
    'email.noreply' => 'noreply@simulasinasional.com',
    'emailName.noreply' => 'Simulasi Nasional SBMPTN 2018',
];

$config = [
    'id' => 'app_simulnas',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app_simulnas\controllers',
    'bootstrap' => [
        'log',
        function() {
            Yii::setAlias('@download-peserta-bukti_pembayaran', Yii::$app->getRequest()->getBaseUrl() . '/upload/peserta-bukti_pembayaran');
            Yii::setAlias('@upload-peserta-bukti_pembayaran', dirname(dirname(__DIR__)) . '/app_simulnas/web/upload/peserta-bukti_pembayaran');
        },
    ],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-app_simulnas',
        ],
        'user' => [
            'identityClass' => 'app_tryout\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-app_simulnas', 'httpOnly' => true],
        ],
        'session' => [
            'name' => 'session-app_simulnas',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];

return $config;