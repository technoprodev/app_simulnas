<?php
namespace app_simulnas\controllers;

use Yii;
use app_tryout\models\Peserta;
use app_tryout\models\PesertaTambahan;
use app_tryout\models\Periode;
use app_tryout\models\PeriodeJenis;
use app_tryout\models\PeriodeKota;
use app_tryout\models\Sumber;
use app_tryout\models\Jurusan;
use technosmart\yii\web\Controller;
use yii\web\NotFoundHttpException;

class DutaController extends Controller
{
    /*public static $permissions = [
        'create'
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index', 'view', 'create'], 'create'],
            ]),
        ];
    }*/

    protected function findModelPesertaByKodeEmail($kode, $email, $post = null)
    {
        if (($model['peserta'] = Peserta::find()->where(['kode' => $kode, 'email' => $email, 'status_aktif' => 'Aktif', 'status_duta' => 'Sudah Aktif'])->one()) !== null) {
            if ($post) {
                $model['peserta']->load($post);
            }
            return $model['peserta'];
        } else if (($model['peserta'] = PesertaTambahan::find()->where(['kode' => $kode, 'email' => $email])->one()) !== null && $model['peserta']->peserta->status_aktif == 'Aktif' && $model['peserta']->peserta->status_duta == 'Sudah Aktif') {
            $model['peserta'] = $model['peserta']->peserta;
            if ($post) {
                unset($post['Peserta']['kode']);
                unset($post['Peserta']['email']);
                $model['peserta']->load($post);
            }
            return $model['peserta'];
        } else {
            $model['peserta'] = new Peserta();
            $model['peserta']->kode = $kode;
            $model['peserta']->email = $email;
            $model['peserta']->validate(['kode', 'email']);
            if (!$model['peserta']->hasErrors()) {
                if ((Peserta::find()->where(['kode' => $kode, 'email' => $email])->one()) !== null ||
                    (PesertaTambahan::find()->join('INNER JOIN', 'peserta p', 'p.id = peserta_tambahan.id_peserta')->where(['peserta_tambahan.kode' => $kode, 'peserta_tambahan.email' => $email])->one()) !== null
                ) {
                    $model['peserta']->addErrors([
                        'kode' => 'Kamu tidak terdaftar sebagai duta. Silahkan hubungi Admin Line.',
                    ]);
                } else {
                    $notFound = false;
                    if ((Peserta::find()->where(['kode' => $kode])->one()) == null && (PesertaTambahan::find()->where(['kode' => $kode])->one()) == null) {
                        $model['peserta']->addErrors([
                            'kode' => 'Kode duta tidak ditemukan di periode sekarang yang sedang aktif',
                        ]);
                        $notFound = true;
                    }
                    if ((Peserta::find()->where(['email' => $email])->one()) == null && (PesertaTambahan::find()->where(['email' => $email])->one()) == null) {
                        $model['peserta']->addErrors([
                            'email' => 'Email duta tidak ditemukan di periode sekarang yang sedang aktif',
                        ]);
                        $notFound = true;
                    }
                    if (!$notFound) {
                        $model['peserta']->addErrors([
                            'kode' => 'Kombinasi kode dan email tidak ditemukan',
                            'email' => 'Kombinasi email dan kode tidak ditemukan',
                        ]);
                    }
                }
            }
            if ($post) {
                $model['peserta']->load($post);
            }
            return $model['peserta'];
        }
    }

    protected function findModelPesertaByKodeEmailDownload($kode, $email)
    {
        if (($model['peserta'] = Peserta::find()->where(['kode' => $kode, 'email' => $email])->one()) !== null) {
            return $model['peserta'];
        } else if (($model['peserta'] = PesertaTambahan::find()->where(['kode' => $kode, 'email' => $email])->one()) !== null) {
            return $model['peserta'];
        } else {
            $model['peserta'] = new Peserta();
            $model['peserta']->addErrors([
                'kode' => 'Kombinasi kode dan email tidak ditemukan',
                'email' => 'Kombinasi email dan kode tidak ditemukan',
            ]);
            return $model['peserta'];
        }
    }

    protected function findModelPeserta($id)
    {
        if (($model = Peserta::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Data peserta tidak ditemukan.');
        }
    }

    protected function findModelPesertaTambahan($id)
    {
        if (($model = PesertaTambahan::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Data peserta tidak ditemukan.');
        }
    }

    protected function findModelPeriode()
    {
        if (($model = Periode::find()->where(['status' => 'aktif'])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Pendaftaran Tryout belum dibuka.');
        }
    }

    protected function findModelPeriodeJenis($id)
    {
        if (($model = PeriodeJenis::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Kode kota tidak ditemukan.');
        }
    }

    protected function findModelPeriodeJenisByPeriode($id)
    {
        if (($model = PeriodeJenis::find()->where(['id_periode' => $id])->all()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Kode kota tidak ditemukan.');
        }
    }

    protected function findModelPeriodeKota($id)
    {
        if (($model = PeriodeKota::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Kode kota tidak ditemukan.');
        }
    }

    public function actionIndex($kode = null, $email = null)
    {
        $error = true;

        $model['duta'] = new Peserta();
        $periode = $this->findModelPeriode();
        
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['duta'] = $this->findModelPesertaByKodeEmail($post['Peserta']['kode'], $post['Peserta']['email']);

            $error = false;
        } else {
            if ($kode || $email) {
                $model['duta'] = $this->findModelPesertaByKodeEmail($kode, $email);

                if ($model['duta']->hasErrors()) {
                    if (array_key_exists('kode', $model['duta']->errors) && $model['duta']->errors['kode'][0] == 'Kamu tidak terdaftar sebagai duta. Silahkan hubungi Admin Line.')
                        Yii::$app->session->setFlash('error', 'Kamu tidak terdaftar sebagai duta. Silahkan hubungi Admin Line.');
                    else
                        Yii::$app->session->setFlash('error', 'Data duta tidak ditemukan.');
                }
            } else {
                $newSearch = true;
            }
        }

        if ($error)
            return $this->render('form-duta', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'newSearch' => isset($newSearch) ? $newSearch : false,
                'title' => 'Dashboard Duta',
            ]);
        else
            return $this->redirect(['index', 'kode' => $model['duta']->kode, 'email' => $model['duta']->email]);
    }

    public function actionPendaftaran($kode, $email)
    {
        $error = true;

        $model['duta'] = $this->findModelPesertaByKodeEmail($kode, $email);
        if ($model['duta']->hasErrors()) {
            if (array_key_exists('kode', $model['duta']->errors) && $model['duta']->errors['kode'][0] == 'Kamu tidak terdaftar sebagai duta. Silahkan hubungi Admin Line.')
                throw new NotFoundHttpException('Kamu tidak terdaftar sebagai duta. Silahkan hubungi Admin Line.');
            else
                throw new NotFoundHttpException('Data duta tidak ditemukan.');
        }
        $model['peserta'] = new Peserta();
        $periode = $this->findModelPeriode();
        $model['periode_jenis'] = $this->findModelPeriodeJenisByPeriode($periode->id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['peserta']->load($post);
            if (isset($post['PesertaTambahan'])) {
                foreach ($post['PesertaTambahan'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $pesertaTambahan = $this->findModelPesertaTambahan($value['id']);
                        $pesertaTambahan->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $pesertaTambahan = $this->findModelPesertaTambahan(($value['id']*-1));
                        $pesertaTambahan->isDeleted = true;
                    } else {
                        $pesertaTambahan = new PesertaTambahan();
                        $pesertaTambahan->setAttributes($value);
                    }
                    $model['peserta_tambahan'][] = $pesertaTambahan;
                }
            }

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                $periodeJenis = $this->findModelPeriodeJenis($model['peserta']->id_periode_jenis);
                $periodeKota = $this->findModelPeriodeKota($model['peserta']->id_periode_kota);
                $model['peserta']->id_duta = $model['duta']->id;
                $model['peserta']->id_periode = $periode->id;
                $model['peserta']->kode = $periode->kode . $periodeKota->kode . $this->sequence('peserta-kode');
                $model['peserta']->status_bayar = 'Belum Bayar';
                $model['peserta']->nama = ucwords(strtolower($model['peserta']->nama));

                $jumlahTiket = 1;
                if (isset($model['peserta_tambahan']) and is_array($model['peserta_tambahan'])) {
                    foreach ($model['peserta_tambahan'] as $key => $pesertaTambahan) {
                        if (!$pesertaTambahan->isDeleted) {
                            $jumlahTiket++;
                        }
                    }
                }
                $variableJumlahTiket = 'harga_' . $jumlahTiket . '_tiket';
                
                $model['peserta']->jumlah_tiket = $jumlahTiket;
                $model['peserta']->harga = $periodeJenis->$variableJumlahTiket;
                $model['peserta']->periode_penjualan = $periodeJenis->periode_penjualan;
                $model['peserta']->tagihan = $model['peserta']->harga;

                if (!$model['peserta']->save()) {
                    throw new \yii\base\UserException('Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                if (isset($model['peserta_tambahan']) and is_array($model['peserta_tambahan'])) {
                    foreach ($model['peserta_tambahan'] as $key => $pesertaTambahan) {
                        if ($pesertaTambahan->isDeleted) {
                            if (!$pesertaTambahan->delete()) {
                                $error = true;
                            }
                        } else {
                            if (!$pesertaTambahan->validate(['id_periode_jenis', 'id_periode_kota'])) {
                                $error = true;
                            } else {
                                $periodeJenis = $this->findModelPeriodeJenis($pesertaTambahan->id_periode_jenis);
                                $periodeKota = $this->findModelPeriodeKota($pesertaTambahan->id_periode_kota);
                                $pesertaTambahan->id_peserta = $model['peserta']->id;
                                $pesertaTambahan->kode = $periode->kode . $periodeKota->kode . $this->sequence('peserta-kode');
                                $pesertaTambahan->harga = $periodeJenis->$variableJumlahTiket;
                                $pesertaTambahan->periode_penjualan = $periodeJenis->periode_penjualan;
                                $model['peserta']->tagihan += $pesertaTambahan->harga;
                                $pesertaTambahan->nama = ucwords(strtolower($pesertaTambahan->nama));
                                if (!$pesertaTambahan->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\base\UserException('Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                if (!$model['peserta']->save()) {
                    throw new \yii\base\UserException('Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div>Pendaftaran berhasil dilakukan. Petunjuk pembayaran telah dikirim ke email yang bersangkutan.');

                if (YII_ENV == 'prod') {
                    \Yii::$app->mail->compose('email/email-pendaftaran', ['model' => $model])
                        ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                        ->setSubject('Segera Lakukan Pembayaran')->send();
                } else if (YII_ENV == 'dev') {
                    /*return $this->render('email-pendaftaran', [
                        'model' => $model,
                        'idPeriode' => $periode->id,
                        'title' => 'Pendaftaran',
                    ]);*/
                    \Yii::$app->mail->compose('email/email-pendaftaran', ['model' => $model])
                        ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                        ->setSubject('Segera Lakukan Pembayaran')->send();
                }
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\base\UserException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            foreach ($model['peserta']->pesertaTambahans as $key => $pesertaTambahan)
                $model['peserta_tambahan'][] = $pesertaTambahan;

            /*$model['peserta']->nama = 'Fandy Pradana';
            $model['peserta']->email = 'pradana.fandy@gmail.com';
            $model['peserta']->handphone = '08992045553';
            $model['peserta']->id_periode_jenis = '1';
            $model['peserta']->id_periode_kota = '1';
            $model['peserta']->jenis_kelamin = 'Laki-laki';
            $model['peserta']->sekolah = 'SMAN 8 Jakarta';
            $model['peserta']->alamat = 'Jalan Punawarman no 30, Bandung';
            $model['peserta']->facebook = 'prafandy';
            $model['peserta']->twitter = 'prafandy';
            $model['peserta']->instagram = 'prafandy';
            $model['peserta']->line = 'prafandy';
            $model['peserta']->whatsapp = '08992045553';
            $model['peserta']->id_sumber = '1';
            $model['peserta']->id_jurusan = '1';*/
        }

        if ($error)
            return $this->render('form-pendaftaran', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Pendaftaran Tiket oleh Duta',
            ]);
        else
            return $this->redirect(['index', 'kode' => $kode, 'email' => $email]);
    }
}