<?php
namespace app_simulnas\controllers;

use Yii;
use app_tryout\models\Peserta;
use app_tryout\models\PesertaTambahan;
use app_tryout\models\Periode;
use app_tryout\models\PeriodeJenis;
use app_tryout\models\PeriodeKota;
use app_tryout\models\Sumber;
use app_tryout\models\Jurusan;
use technosmart\yii\web\Controller;
use yii\web\NotFoundHttpException;

class PesertaController extends Controller
{
    /*public static $permissions = [
        'create'
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index', 'view', 'create'], 'create'],
            ]),
        ];
    }*/

    protected function findModelPesertaByKodeEmail($kode, $email, $post = null)
    {
        if (($model['peserta'] = Peserta::find()->where(['kode' => $kode, 'email' => $email, 'status_aktif' => 'Aktif'])->one()) !== null) {
            if ($post) {
                $model['peserta']->load($post);
            }
            return $model['peserta'];
        } else if (($model['peserta'] = PesertaTambahan::find()->where(['kode' => $kode, 'email' => $email])->one()) !== null && $model['peserta']->peserta->status_aktif == 'Aktif') {
            $model['peserta'] = $model['peserta']->peserta;
            if ($post) {
                unset($post['Peserta']['kode']);
                unset($post['Peserta']['email']);
                $model['peserta']->load($post);
            }
            return $model['peserta'];
        } else {
            $model['peserta'] = new Peserta();
            $model['peserta']->kode = $kode;
            $model['peserta']->email = $email;
            $model['peserta']->validate(['kode', 'email']);
            if (!$model['peserta']->hasErrors()) {
                if ((Peserta::find()->where(['kode' => $kode, 'email' => $email, 'status_aktif' => 'Nonaktif'])->one()) !== null ||
                    (PesertaTambahan::find()->join('INNER JOIN', 'peserta p', 'p.id = peserta_tambahan.id_peserta')->where(['peserta_tambahan.kode' => $kode, 'peserta_tambahan.email' => $email, 'p.status_aktif' => 'Nonaktif'])->one()) !== null
                ) {
                    $model['peserta']->addErrors([
                        'kode' => 'Peserta telah dinonaktifkan oleh Admin.',
                    ]);
                } else {
                    $notFound = false;
                    if ((Peserta::find()->where(['kode' => $kode])->one()) == null && (PesertaTambahan::find()->where(['kode' => $kode])->one()) == null) {
                        $model['peserta']->addErrors([
                            'kode' => 'Kode peserta tidak ditemukan di periode sekarang yang sedang aktif',
                        ]);
                        $notFound = true;
                    }
                    if ((Peserta::find()->where(['email' => $email])->one()) == null && (PesertaTambahan::find()->where(['email' => $email])->one()) == null) {
                        $model['peserta']->addErrors([
                            'email' => 'Email peserta tidak ditemukan di periode sekarang yang sedang aktif',
                        ]);
                        $notFound = true;
                    }
                    if (!$notFound) {
                        $model['peserta']->addErrors([
                            'kode' => 'Kombinasi kode dan email tidak ditemukan',
                            'email' => 'Kombinasi email dan kode tidak ditemukan',
                        ]);
                    }
                }
            }
            if ($post) {
                $model['peserta']->load($post);
            }
            return $model['peserta'];
        }
    }

    protected function findModelPesertaByKodeEmailDownload($kode, $email)
    {
        if (($model['peserta'] = Peserta::find()->where(['kode' => $kode, 'email' => $email])->one()) !== null) {
            return $model['peserta'];
        } else if (($model['peserta'] = PesertaTambahan::find()->where(['kode' => $kode, 'email' => $email])->one()) !== null) {
            return $model['peserta'];
        } else {
            $model['peserta'] = new Peserta();
            $model['peserta']->addErrors([
                'kode' => 'Kombinasi kode dan email tidak ditemukan',
                'email' => 'Kombinasi email dan kode tidak ditemukan',
            ]);
            return $model['peserta'];
        }
    }

    protected function findModelPeserta($id)
    {
        if (($model = Peserta::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Data peserta tidak ditemukan.');
        }
    }

    protected function findModelPesertaTambahan($id)
    {
        if (($model = PesertaTambahan::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Data peserta tidak ditemukan.');
        }
    }

    protected function findModelPeriode()
    {
        if (($model = Periode::find()->where(['status' => 'aktif'])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Pendaftaran Tryout belum dibuka.');
        }
    }

    protected function findModelPeriodeJenis($id)
    {
        if (($model = PeriodeJenis::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Kode kota tidak ditemukan.');
        }
    }

    protected function findModelPeriodeJenisByPeriode($id)
    {
        if (($model = PeriodeJenis::find()->where(['id_periode' => $id])->all()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Kode kota tidak ditemukan.');
        }
    }

    protected function findModelPeriodeKota($id)
    {
        if (($model = PeriodeKota::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Kode kota tidak ditemukan.');
        }
    }

    public function actionPendaftaran()
    {throw new NotFoundHttpException('Pendaftaran Sudah Ditutup.');
        $error = true;

        $model['peserta'] = new Peserta();
        $periode = $this->findModelPeriode();
        $model['periode_jenis'] = $this->findModelPeriodeJenisByPeriode($periode->id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['peserta']->load($post);
            if (isset($post['PesertaTambahan'])) {
                foreach ($post['PesertaTambahan'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $pesertaTambahan = $this->findModelPesertaTambahan($value['id']);
                        $pesertaTambahan->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $pesertaTambahan = $this->findModelPesertaTambahan(($value['id']*-1));
                        $pesertaTambahan->isDeleted = true;
                    } else {
                        $pesertaTambahan = new PesertaTambahan();
                        $pesertaTambahan->setAttributes($value);
                    }
                    $model['peserta_tambahan'][] = $pesertaTambahan;
                }
            }

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                $periodeJenis = $this->findModelPeriodeJenis($model['peserta']->id_periode_jenis);
                $periodeKota = $this->findModelPeriodeKota($model['peserta']->id_periode_kota);
                $model['peserta']->id_periode = $periode->id;
                $model['peserta']->kode = $periode->kode . $periodeKota->kode . $this->sequence('peserta-kode');
                $model['peserta']->status_bayar = 'Belum Bayar';
                $model['peserta']->nama = ucwords(strtolower($model['peserta']->nama));

                $jumlahTiket = 1;
                if (isset($model['peserta_tambahan']) and is_array($model['peserta_tambahan'])) {
                    foreach ($model['peserta_tambahan'] as $key => $pesertaTambahan) {
                        if (!$pesertaTambahan->isDeleted) {
                            $jumlahTiket++;
                        }
                    }
                }
                $variableJumlahTiket = 'harga_' . $jumlahTiket . '_tiket';
                
                $model['peserta']->jumlah_tiket = $jumlahTiket;
                $model['peserta']->harga = $periodeJenis->$variableJumlahTiket;
                $model['peserta']->periode_penjualan = $periodeJenis->periode_penjualan;
                $model['peserta']->tagihan = $model['peserta']->harga;

                if (!$model['peserta']->save()) {
                    throw new \yii\base\UserException('Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                if (isset($model['peserta_tambahan']) and is_array($model['peserta_tambahan'])) {
                    foreach ($model['peserta_tambahan'] as $key => $pesertaTambahan) {
                        if (!$pesertaTambahan->isDeleted) {
                            if (!$pesertaTambahan->validate(['id_periode_jenis', 'id_periode_kota'])) {
                                $error = true;
                            } else {
                                $periodeJenis = $this->findModelPeriodeJenis($pesertaTambahan->id_periode_jenis);
                                $periodeKota = $this->findModelPeriodeKota($pesertaTambahan->id_periode_kota);
                                $pesertaTambahan->id_peserta = $model['peserta']->id;
                                $pesertaTambahan->kode = $periode->kode . $periodeKota->kode . $this->sequence('peserta-kode');
                                $pesertaTambahan->harga = $periodeJenis->$variableJumlahTiket;
                                $pesertaTambahan->periode_penjualan = $periodeJenis->periode_penjualan;
                                $model['peserta']->tagihan += $pesertaTambahan->harga;
                                $pesertaTambahan->nama = ucwords(strtolower($pesertaTambahan->nama));
                                if (!$pesertaTambahan->validate()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\base\UserException('Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                } else {
                    if (isset($model['peserta_tambahan']) and is_array($model['peserta_tambahan'])) {
                        foreach ($model['peserta_tambahan'] as $key => $pesertaTambahan) {
                            if ($pesertaTambahan->isDeleted) {
                                if (!$pesertaTambahan->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pesertaTambahan->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\base\UserException('Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                if (!$model['peserta']->save()) {
                    throw new \yii\base\UserException('Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div>Pendaftaran berhasil dilakukan. Harap cek email untuk mendapat petunjuk pembayaran.');

                if (YII_ENV == 'prod') {
                    \Yii::$app->mail->compose('email/email-pendaftaran', ['model' => $model])
                        ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                        ->setSubject('Segera Lakukan Pembayaran')->send();
                } else if (YII_ENV == 'dev') {
                    /*return $this->render('/email/email-pendaftaran', [
                        'model' => $model,
                        'idPeriode' => $periode->id,
                        'title' => 'Pendaftaran',
                    ]);*/
                    \Yii::$app->mail->compose('email/email-pendaftaran', ['model' => $model])
                        ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                        ->setSubject('Segera Lakukan Pembayaran')->send();
                }
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\base\UserException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            foreach ($model['peserta']->pesertaTambahans as $key => $pesertaTambahan)
                $model['peserta_tambahan'][] = $pesertaTambahan;

            /*$model['peserta']->nama = 'Fandy Pradana';
            $model['peserta']->email = 'pradana.fandy@gmail.com';
            $model['peserta']->handphone = '08992045553';
            $model['peserta']->id_periode_jenis = '1';
            $model['peserta']->id_periode_kota = '1';
            $model['peserta']->jenis_kelamin = 'Laki-laki';
            $model['peserta']->sekolah = 'SMAN 8 Jakarta';
            $model['peserta']->alamat = 'Jalan Punawarman no 30, Bandung';
            $model['peserta']->facebook = 'prafandy';
            $model['peserta']->twitter = 'prafandy';
            $model['peserta']->instagram = 'prafandy';
            $model['peserta']->line = 'prafandy';
            $model['peserta']->whatsapp = '08992045553';
            $model['peserta']->id_sumber = '1';
            $model['peserta']->id_jurusan = '1';*/
        }

        if ($error)
            return $this->render('form-pendaftaran', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Pendaftaran',
            ]);
        else
            return $this->redirect(['site/index']);
    }

    public function actionKonfirmasi()
    {throw new NotFoundHttpException('Konfirmasi Sudah Ditutup.');
        $error = true;

        $model['peserta'] = new Peserta();
        $model['peserta']->scenario = 'konfirmasi';
        $periode = $this->findModelPeriode();
        
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['peserta'] = $this->findModelPesertaByKodeEmail($post['Peserta']['kode'], $post['Peserta']['email'], $post);
            $model['peserta']->scenario = 'konfirmasi';

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                if ($model['peserta']->hasErrors()) {
                    if (array_key_exists('kode', $model['peserta']->errors) && $model['peserta']->errors['kode'][0] == 'Peserta telah dinonaktifkan oleh Admin.')
                        throw new \yii\base\UserException('Peserta telah dinonaktifkan oleh Admin. Harap hubungi Admin melalui line jika ini adalah kesalahan Admin.');
                    else
                        throw new \yii\base\UserException('Data peserta tidak ditemukan.');
                }

                if ($model['peserta']->status_bayar == 'Sudah Bayar') {
                    throw new \yii\base\UserException('Pembayaran Anda sudah berhasil dilakukan. Anda tidak perlu melakukan konfirmasi pembayaran lagi.');
                }

                if ($model['peserta']->status_bayar == 'Hanya Daftar') {
                    throw new \yii\base\UserException('Anda tidak dapat melakukan konfirmasi karena masih dalam proses pengajuan kota. Harap menghubungi admin untuk menanyakan kepastian lokasi.');
                }

                $model['peserta']->status_bayar = 'Dalam Proses Konfirmasi';
                $model['peserta']->confirm_request_at = new \yii\db\Expression("now()");

                if (!$model['peserta']->save()) {
                    throw new \yii\base\UserException('Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/sand-clock.png\" width=\"70px;\"></div>Konfirmasi pembayaran berhasil dilakukan. Kamu akan mendapat notifikasi melalui email setelah admin melakukan verifikasi.');

                if (YII_ENV == 'prod') {
                    \Yii::$app->mail->compose('email/email-konfirmasi', ['model' => $model])
                        ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                        ->setSubject('Konfirmasi Pembayaran Sedang Diproses')->send();
                } else if (YII_ENV == 'dev') {
                    /*return $this->render('/email/email-konfirmasi', [
                        'model' => $model,
                        'idPeriode' => $periode->id,
                        'title' => 'konfirmasi',
                    ]);*/
                    \Yii::$app->mail->compose('email/email-konfirmasi', ['model' => $model])
                        ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                        ->setSubject('Konfirmasi Pembayaran Sedang Diproses')->send();
                }
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\base\UserException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            /*$model['peserta']->kode = 101000;
            $model['peserta']->email = 'pradana.fandy@gmail.com';
            $model['peserta']->id_periode_metode_pembayaran = 1;
            $model['peserta']->tanggal_pembayaran = date('d/m/Y');
            $model['peserta']->pembayaran_atas_nama = 'Fandy Pradana';
            $model['peserta']->informasi_pembayaran = 'Mohon diverifikasi';*/
        }

        if ($error)
            return $this->render('form-konfirmasi', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Konfirmasi',
            ]);
        else
            return $this->redirect(['site/index']);
    }

    public function actionCekStatus($kode = null, $email = null)
    {
        $error = true;

        $model['peserta'] = new Peserta();
        $periode = $this->findModelPeriode();
        
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['peserta'] = $this->findModelPesertaByKodeEmail($post['Peserta']['kode'], $post['Peserta']['email']);

            $error = false;
        } else {
            if ($kode || $email) {
                $model['peserta'] = $this->findModelPesertaByKodeEmail($kode, $email);

                if ($model['peserta']->hasErrors()) {
                    if (array_key_exists('kode', $model['peserta']->errors) && $model['peserta']->errors['kode'][0] == 'Peserta telah dinonaktifkan oleh Admin.')
                        Yii::$app->session->setFlash('error', 'Peserta telah dinonaktifkan oleh Admin. Harap hubungi Admin melalui line jika ini adalah kesalahan Admin.');
                    else
                        Yii::$app->session->setFlash('error', 'Data peserta tidak ditemukan.');
                }
            } else {
                $newSearch = true;
            }
        }

        if ($error)
            return $this->render('form-cek-status', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'newSearch' => isset($newSearch) ? $newSearch : false,
                'title' => 'Cek Nilai',
            ]);
        else
            return $this->redirect(['cek-status', 'kode' => $model['peserta']->kode, 'email' => $model['peserta']->email]);
    }

    public function actionDownloadKartuUjian($kode, $email)
    {throw new NotFoundHttpException('Sudah tidak bisa download kartu ujian lagi.');
        $error = true;

        $model['peserta'] = $this->findModelPesertaByKodeEmailDownload($kode, $email);
        
        if ($model['peserta']->hasErrors())
            Yii::$app->session->setFlash('error', 'Download kartu ujian gagal.');
        else
            $error = false;

        if ($error)
            return $this->redirect(['site/index']);
        else {
            // $title = 'Kartu Ujian ' . Yii::$app->params['app.name'] . ' #' . $model['peserta']->kode;
            // return $this->render('download-kartu-ujian', ['model' => $model, 'title' => $title]);
            $this->layout = 'download';
            $title = 'Kartu Ujian ' . Yii::$app->params['app.name'] . ' #' . $model['peserta']->kode;

            $dompdf = new \Dompdf\Dompdf();
            $dompdf->loadHtml($this->render('download-kartu-ujian', ['model' => $model, 'title' => $title]));
            $dompdf->setPaper('A5', 'landscape');
            $dompdf->render();
            $dompdf->stream($title . '.pdf');
            exit;
        }
    }

    public function actionDownloadSertifikat($kode, $email)
    {
        $error = true;

        $model['peserta'] = $this->findModelPesertaByKodeEmailDownload($kode, $email);
        
        if ($model['peserta']->hasErrors())
            Yii::$app->session->setFlash('error', 'Download sertifikat gagal.');
        else
            $error = false;

        if ($error)
            return $this->redirect(['site/index']);
        else {
            // $title = 'Sertifikat ' . Yii::$app->params['app.name'] . ' #' . $model['peserta']->kode;
            // return $this->render('download-sertifikat', ['model' => $model, 'title' => $title]);
            $this->layout = 'download';
            $title = 'Sertifikat ' . Yii::$app->params['app.name'] . ' #' . $model['peserta']->kode;

            $dompdf = new \Dompdf\Dompdf();
            $dompdf->loadHtml($this->render('download-sertifikat', ['model' => $model, 'title' => $title]));
            $dompdf->setPaper('A4', 'landscape');
            $dompdf->render();
            $dompdf->stream($title . '.pdf');
            exit;
        }
    }

    public function actionDetailKota($id)
    {
        $model['periode_kota'] = $this->findModelPeriodeKota($id);

        return $this->render('detail-kota', [
            'model' => $model,
            'title' => 'Detail Lokasi',
        ]);
    }
}