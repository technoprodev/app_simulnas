<?php
namespace app_simulnas\controllers;

use Yii;
use app_tryout\models\Peserta;
use app_tryout\models\PesertaTambahan;
use app_tryout\models\Periode;
use app_tryout\models\PeriodeJenis;
use app_tryout\models\PeriodeKota;
use app_tryout\models\Sumber;
use app_tryout\models\Jurusan;
use technosmart\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

class XswzaqController extends Controller
{
    public $layout = 'xswzaq';

    public static $permissions = [
        'dashboard', 'peserta', 'panitia', 'setting'
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [[
                    'datatables-statistik',
                    'index',
                ], 'dashboard'],
                [[
                    'datatables-peserta',
                    'peserta',
                    'konfirmasi',
                    'detail-peserta',
                    'update-peserta',
                    'tiket-gratis',
                    'aktifkan-peserta',
                    'nonaktifkan-peserta',
                    'aktifkan-duta',
                    'nonaktifkan-duta',
                    'resend-pendaftaran',
                    'resend-konfirmasi',
                    'resend-diterima',
                    'resend-ditolak',
                ], 'peserta'],
                [[
                    'setting',
                    'datatables-periode-kota',
                    'update-periode-kota',
                    'create-periode-kota',
                    'aktifkan-periode-kota',
                    'nonaktifkan-periode-kota',
                    'detail-periode-kota',
                    'getList-kota',
                    'datatables-periode-jenis',
                    'update-periode-jenis',
                    'create-periode-jenis',
                    'aktifkan-periode-jenis',
                    'nonaktifkan-periode-jenis',
                    'detail-periode-jenis',
                ], 'setting'],
                [[
                    'panitia',
                ], 'panitia'],
            ]),
        ];
    }

    protected function findModelPesertaKonfirmasi($id)
    {
        if (($model = Peserta::find()->where(['id' => $id, 'status_bayar' => 'Dalam Proses Konfirmasi'])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Data peserta tidak ditemukan.');
        }
    }

    protected function findModelPeserta($id)
    {
        if (($model = Peserta::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Data peserta tidak ditemukan.');
        }
    }

    protected function findModelPesertaTambahan($id)
    {
        if (($model = PesertaTambahan::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Data peserta tidak ditemukan.');
        }
    }

    protected function findModelPesertaAktifkan($id)
    {
        if (($model = Peserta::find()->where(['id' => $id, 'status_aktif' => 'Nonaktif'])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Data peserta tidak ditemukan.');
        }
    }

    protected function findModelPesertaNonaktifkan($id)
    {
        if (($model = Peserta::find()->where(['id' => $id, 'status_aktif' => 'Aktif'])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Data peserta tidak ditemukan.');
        }
    }

    protected function findModelDutaAktifkan($id)
    {
        if (($model = Peserta::find()->where(['id' => $id, 'status_duta' => 'Tidak Aktif'])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Data duta tidak ditemukan.');
        }
    }

    protected function findModelDutaNonaktifkan($id)
    {
        if (($model = Peserta::find()->where(['id' => $id, 'status_duta' => 'Sudah Aktif'])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Data duta tidak ditemukan.');
        }
    }

    protected function findModelPeriodeKota($id)
    {
        if (($model = PeriodeKota::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Kode kota tidak ditemukan.');
        }
    }

    protected function findModelPeriodeKotaAktifkan($id)
    {
        if (($model = PeriodeKota::find()->where(['id' => $id, 'status' => 'Tidak Aktif'])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Data kota tidak ditemukan.');
        }
    }

    protected function findModelPeriodeKotaNonaktifkan($id)
    {
        if (($model = PeriodeKota::find()->where(['id' => $id, 'status' => 'Sedang Aktif'])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Data kota tidak ditemukan.');
        }
    }

    protected function findModelPeriodeJenis($id)
    {
        if (($model = PeriodeJenis::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Kode kota tidak ditemukan.');
        }
    }

    protected function findModelPeriodeJenisAktifkan($id)
    {
        if (($model = PeriodeJenis::find()->where(['id' => $id, 'status' => 'Tidak Aktif'])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Data kota tidak ditemukan.');
        }
    }

    protected function findModelPeriodeJenisNonaktifkan($id)
    {
        if (($model = PeriodeJenis::find()->where(['id' => $id, 'status' => 'Sedang Aktif'])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Data kota tidak ditemukan.');
        }
    }

    protected function findModelPeriodeJenisByPeriode($id)
    {
        if (($model = PeriodeJenis::find()->where(['id_periode' => $id])->all()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Kode kota tidak ditemukan.');
        }
    }

    //////////////

    public function actionDatatablesStatistik()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'pk.nama as kota',
                'count(distinct p.id) as total_transaksi',
                'count(distinct case when status_bayar = "Belum Bayar" then p.id end) as transaksi_belum_bayar',
                'count(distinct case when status_bayar = "Dalam Proses Konfirmasi" then p.id end) as transaksi_dalam_proses_konfirmasi',
                'count(distinct case when status_bayar = "Sudah Bayar" then p.id end) as transaksi_sudah_bayar',
                'count(distinct case when status_bayar = "Ditolak" then p.id end) as transaksi_ditolak',
                'count(pt.id) + count(distinct p.id) as total_tiket',
                'count(case when pt.harga = 0 then pt.id end) + count(distinct case when p.harga = 0 then p.id end) as total_tiket_gratis',
                'count(case when status_bayar = "Belum Bayar" then pt.id end) + count(distinct case when status_bayar = "Belum Bayar" then p.id end) as tiket_belum_bayar',
                'count(case when status_bayar = "Dalam Proses Konfirmasi" then pt.id end) + count(distinct case when status_bayar = "Dalam Proses Konfirmasi" then p.id end) as tiket_dalam_proses_konfirmasi',
                'count(case when status_bayar = "Sudah Bayar" then pt.id end) + count(distinct case when status_bayar = "Sudah Bayar" then p.id end) as tiket_sudah_bayar',
                'count(case when status_bayar = "Ditolak" then pt.id end) + count(distinct case when status_bayar = "Ditolak" then p.id end) as tiket_ditolak',
                'count(case when pjpt.nama = "Saintek (IPA)" and status_bayar = "Sudah Bayar" then pt.id end) + count(distinct case when pjp.nama = "Saintek (IPA)" and status_bayar = "Sudah Bayar" then p.id end) as tiket_saintek_ipa',
                'count(case when pjpt.nama = "Soshum (IPS)" and status_bayar = "Sudah Bayar" then pt.id end) + count(distinct case when pjp.nama = "Soshum (IPS)" and status_bayar = "Sudah Bayar" then p.id end) as tiket_soshum_ips',
                'count(distinct case when status_duta = "Sudah Aktif" then p.id end) as duta_transaksi',
                'count(case when id_duta IS NOT NULL and status_bayar = "Sudah Bayar" then pt.id end) + count(distinct case when id_duta IS NOT NULL and status_bayar = "Sudah Bayar" then p.id end) as duta_tiket',
            ])
            ->from('peserta p')
            ->join('RIGHT JOIN', 'periode_kota pk', 'pk.id = p.id_periode_kota AND pk.id_periode = 1 AND status_aktif = "Aktif"')
            ->join('LEFT JOIN', 'peserta_tambahan pt', 'pt.id_peserta = p.id')
            ->join('LEFT JOIN', 'periode_jenis pjp', 'pjp.id = p.id_periode_jenis')
            ->join('LEFT JOIN', 'periode_jenis pjpt', 'pjpt.id = pt.id_periode_jenis')
            ->groupBy(['pk.nama'])
        ;

        return $this->datatables($query, $post = Yii::$app->request->post(), Peserta::getDb());
    }

    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        if (isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id)['operator']))
            return $this->redirect('xswzaq/peserta');

        $query = new \yii\db\Query();
        $query
            ->select([
                'count(distinct p.id) as total_transaksi',
                'count(distinct case when status_bayar = "Belum Bayar" then p.id end) as transaksi_belum_bayar',
                'count(distinct case when status_bayar = "Dalam Proses Konfirmasi" then p.id end) as transaksi_dalam_proses_konfirmasi',
                'count(distinct case when status_bayar = "Sudah Bayar" then p.id end) as transaksi_sudah_bayar',
                'count(distinct case when status_bayar = "Ditolak" then p.id end) as transaksi_ditolak',
                'count(pt.id) + count(distinct p.id) as total_tiket',
                'count(case when pt.harga = 0 then pt.id end) + count(distinct case when p.harga = 0 then p.id end) as total_tiket_gratis',
                'count(case when status_bayar = "Belum Bayar" then pt.id end) + count(distinct case when status_bayar = "Belum Bayar" then p.id end) as tiket_belum_bayar',
                'count(case when status_bayar = "Dalam Proses Konfirmasi" then pt.id end) + count(distinct case when status_bayar = "Dalam Proses Konfirmasi" then p.id end) as tiket_dalam_proses_konfirmasi',
                'count(case when status_bayar = "Sudah Bayar" then pt.id end) + count(distinct case when status_bayar = "Sudah Bayar" then p.id end) as tiket_sudah_bayar',
                'count(case when status_bayar = "Ditolak" then pt.id end) + count(distinct case when status_bayar = "Ditolak" then p.id end) as tiket_ditolak',
                'count(case when pjpt.nama = "Saintek (IPA)" and status_bayar = "Sudah Bayar" then pt.id end) + count(distinct case when pjp.nama = "Saintek (IPA)" and status_bayar = "Sudah Bayar" then p.id end) as tiket_saintek_ipa',
                'count(case when pjpt.nama = "Soshum (IPS)" and status_bayar = "Sudah Bayar" then pt.id end) + count(distinct case when pjp.nama = "Soshum (IPS)" and status_bayar = "Sudah Bayar" then p.id end) as tiket_soshum_ips',
                'count(distinct case when status_duta = "Sudah Aktif" then p.id end) as duta_transaksi',
                'count(case when id_duta IS NOT NULL and status_bayar = "Sudah Bayar" then pt.id end) + count(distinct case when id_duta IS NOT NULL and status_bayar = "Sudah Bayar" then p.id end) as duta_tiket',
            ])
            ->from('peserta p')
            ->join('RIGHT JOIN', 'periode_kota pk', 'pk.id = p.id_periode_kota AND pk.id_periode = 1 AND status_aktif = "Aktif"')
            ->join('LEFT JOIN', 'peserta_tambahan pt', 'pt.id_peserta = p.id')
            ->join('LEFT JOIN', 'periode_jenis pjp', 'pjp.id = p.id_periode_jenis')
            ->join('LEFT JOIN', 'periode_jenis pjpt', 'pjpt.id = pt.id_periode_jenis')
            ;
        $model['all'] = $query->one();

        $query = new \yii\db\Query();
        $query
            ->select([
                'pk.nama as kota',
                'count(distinct p.id) as total_transaksi',
                'count(distinct case when status_bayar = "Belum Bayar" then p.id end) as transaksi_belum_bayar',
                'count(distinct case when status_bayar = "Dalam Proses Konfirmasi" then p.id end) as transaksi_dalam_proses_konfirmasi',
                'count(distinct case when status_bayar = "Sudah Bayar" then p.id end) as transaksi_sudah_bayar',
                'count(distinct case when status_bayar = "Ditolak" then p.id end) as transaksi_ditolak',
                'count(pt.id) + count(distinct p.id) as total_tiket',
                'count(case when pt.harga = 0 then pt.id end) + count(distinct case when p.harga = 0 then p.id end) as total_tiket_gratis',
                'count(case when status_bayar = "Belum Bayar" then pt.id end) + count(distinct case when status_bayar = "Belum Bayar" then p.id end) as tiket_belum_bayar',
                'count(case when status_bayar = "Dalam Proses Konfirmasi" then pt.id end) + count(distinct case when status_bayar = "Dalam Proses Konfirmasi" then p.id end) as tiket_dalam_proses_konfirmasi',
                'count(case when status_bayar = "Sudah Bayar" then pt.id end) + count(distinct case when status_bayar = "Sudah Bayar" then p.id end) as tiket_sudah_bayar',
                'count(case when status_bayar = "Ditolak" then pt.id end) + count(distinct case when status_bayar = "Ditolak" then p.id end) as tiket_ditolak',
                'count(case when pjpt.nama = "Saintek (IPA)" and status_bayar = "Sudah Bayar" then pt.id end) + count(distinct case when pjp.nama = "Saintek (IPA)" and status_bayar = "Sudah Bayar" then p.id end) as tiket_saintek_ipa',
                'count(case when pjpt.nama = "Soshum (IPS)" and status_bayar = "Sudah Bayar" then pt.id end) + count(distinct case when pjp.nama = "Soshum (IPS)" and status_bayar = "Sudah Bayar" then p.id end) as tiket_soshum_ips',
                'count(distinct case when status_duta = "Sudah Aktif" then p.id end) as duta_transaksi',
                'count(case when id_duta IS NOT NULL and status_bayar = "Sudah Bayar" then pt.id end) + count(distinct case when id_duta IS NOT NULL and status_bayar = "Sudah Bayar" then p.id end) as duta_tiket',
            ])
            ->from('peserta p')
            ->join('RIGHT JOIN', 'periode_kota pk', 'pk.id = p.id_periode_kota AND pk.id_periode = 1 AND status_aktif = "Aktif"')
            ->join('LEFT JOIN', 'peserta_tambahan pt', 'pt.id_peserta = p.id')
            ->join('LEFT JOIN', 'periode_jenis pjp', 'pjp.id = p.id_periode_jenis')
            ->join('LEFT JOIN', 'periode_jenis pjpt', 'pjpt.id = pt.id_periode_jenis')
            ->groupBy(['pk.nama'])
            ;
        $model['pie'] = $query->all();
        $model['label_tiket_belum_bayar_per_kota'] = $model['data_tiket_belum_bayar_per_kota'] = [];
        $model['label_tiket_sudah_bayar_per_kota'] = $model['data_tiket_sudah_bayar_per_kota'] = [];
        $border = ['#663333', '#664d33', '#666633', '#4d6633', '#336633', '#33664d', '#336666', '#334d66', '#333366', '#4d3366', '#663366', '#66334d'];
        $background = ['#e6b8b8', '#e6cfb8', '#e6e6b8', '#cfe6b8', '#b8e6b8', '#b8e6cf', '#b8e6e6', '#b8cfe6', '#b8b8e6', '#cfb8e6', '#e6b8e6', '#e6b8cf'];
        foreach ($model['pie'] as $key => $value) {
            $model['label_tiket_belum_bayar_per_kota'][] = $value['kota'];
            $model['data_tiket_belum_bayar_per_kota'][] = $value['tiket_belum_bayar'];
            $model['background_tiket_belum_bayar_per_kota'][] = $background[$key % 12];
            $model['border_tiket_belum_bayar_per_kota'][] = $border[$key % 12];
            
            $model['label_tiket_sudah_bayar_per_kota'][] = $value['kota'];
            $model['data_tiket_sudah_bayar_per_kota'][] = $value['tiket_sudah_bayar'];
            $model['background_tiket_sudah_bayar_per_kota'][] = $background[$key % 12];
            $model['border_tiket_sudah_bayar_per_kota'][] = $border[$key % 12];
        }

        $query = new \yii\db\Query();
        $query
            ->select([
                'YEAR(p.confirm_request_at) AS year',
                'MONTH(p.confirm_request_at) AS month',
                'DAY(p.confirm_request_at) AS day',
                'count(pt.id) + count(distinct p.id) as total_tiket',
            ])
            ->from('peserta p')
            ->join('LEFT JOIN', 'peserta_tambahan pt', 'pt.id_peserta = p.id')
            ->groupBy([
                'DAY(p.confirm_request_at)',
                'MONTH(p.confirm_request_at)',
                'YEAR(p.confirm_request_at)',
            ])
            ->orderBy([
                'year' => SORT_ASC,
                'month' => SORT_ASC,
                'day' => SORT_ASC,
            ])
            ->where([
                'p.status_bayar' => 'Sudah Bayar',
                'p.status_aktif' => 'Aktif',
            ])
            ;
        $model['anak_konfirmasi'] = $query->all();
        $model['label_anak_konfirmasi'] = $model['data_anak_konfirmasi'] = [];
        foreach ($model['anak_konfirmasi'] as $key => $value) {
            $model['label_anak_konfirmasi'][] = $value['day'] . '/' . $value['month'];
            $model['data_anak_konfirmasi'][] = $value['total_tiket'];
        }

        $query = new \yii\db\Query();
        $query
            ->select([
                'YEAR(p.confirm_at) AS year',
                'MONTH(p.confirm_at) AS month',
                'DAY(p.confirm_at) AS day',
                'count(pt.id) + count(distinct p.id) as total_tiket',
            ])
            ->from('peserta p')
            ->join('LEFT JOIN', 'peserta_tambahan pt', 'pt.id_peserta = p.id')
            ->groupBy([
                'DAY(p.confirm_at)',
                'MONTH(p.confirm_at)',
                'YEAR(p.confirm_at)',
            ])
            ->orderBy([
                'year' => SORT_ASC,
                'month' => SORT_ASC,
                'day' => SORT_ASC,
            ])
            ->where([
                'p.status_bayar' => 'Sudah Bayar',
                'p.status_aktif' => 'Aktif',
            ])
            ;
        $model['anak_dikonfirmasi'] = $query->all();
        $model['label_anak_dikonfirmasi'] = $model['data_anak_dikonfirmasi'] = [];
        foreach ($model['anak_dikonfirmasi'] as $key => $value) {
            $model['label_anak_dikonfirmasi'][] = $value['day'] . '/' . $value['month'];
            $model['data_anak_dikonfirmasi'][] = $value['total_tiket'];
        }

        $query = new \yii\db\Query();
        $query
            ->select([
                'YEAR(p.created_at) AS year',
                'MONTH(p.created_at) AS month',
                'DAY(p.created_at) AS day',
                'count(pt.id) + count(distinct p.id) as total_tiket',
            ])
            ->from('peserta p')
            ->join('LEFT JOIN', 'peserta_tambahan pt', 'pt.id_peserta = p.id')
            ->groupBy([
                'DAY(p.created_at)',
                'MONTH(p.created_at)',
                'YEAR(p.created_at)',
            ])
            ->orderBy([
                'year' => SORT_ASC,
                'month' => SORT_ASC,
                'day' => SORT_ASC,
            ])
            ->where([
                'p.status_bayar' => 'Sudah Bayar',
                'p.status_aktif' => 'Aktif',
            ])
            ;
        $model['peserta_daftar'] = $query->all();
        $model['label_peserta_daftar'] = $model['data_peserta_daftar'] = [];
        foreach ($model['peserta_daftar'] as $key => $value) {
            $model['label_peserta_daftar'][] = $value['day'] . '/' . $value['month'];
            $model['data_peserta_daftar'][] = $value['total_tiket'];
        }

        return $this->render('index', [
            'model' => $model,
            'title' => 'Dashboard',
        ]);
    }

    public function actionStatistik()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        return $this->render('statistik', [
            'model' => $model,
            'title' => 'Statistik',
        ]);
    }

    //////////////

    public function actionDatatablesPeserta()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.nama',
                'p.kode',
                'p.email',
                'p.handphone',
                'FORMAT(p.tagihan, 2) AS tagihan',
                'p.jumlah_tiket',
                'pk.nama AS lokasi',
                'r.name AS request',
                'p.status_bayar AS status',
                'p.status_duta',
                'p.status_aktif',
            ])
            ->from('peserta p')
            ->join('LEFT JOIN', 'periode_jenis pj', 'p.id_periode_jenis = pj.id')
            ->join('LEFT JOIN', 'periode_kota pk', 'p.id_periode_kota = pk.id')
            ->join('LEFT JOIN', 'regencies r', 'r.id = p.id_request_kota')
            ->where(['status_aktif' => 'Aktif'])
        ;

        return $this->datatables($query, $post = Yii::$app->request->post(), Peserta::getDb());
    }

    public function actionPeserta()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $query = new \yii\db\Query();
        $query
            ->select([
                'count(distinct p.id) as total_transaksi',
                'count(distinct case when status_bayar = "Belum Bayar" then p.id end) as transaksi_belum_bayar',
                'count(distinct case when status_bayar = "Dalam Proses Konfirmasi" then p.id end) as transaksi_dalam_proses_konfirmasi',
                'count(distinct case when status_bayar = "Sudah Bayar" then p.id end) as transaksi_sudah_bayar',
                'count(distinct case when status_bayar = "Ditolak" then p.id end) as transaksi_ditolak',
                'count(pt.id) + count(distinct p.id) as total_tiket',
                'count(case when pt.harga = 0 then pt.id end) + count(distinct case when p.harga = 0 then p.id end) as total_tiket_gratis',
                'count(case when status_bayar = "Belum Bayar" then pt.id end) + count(distinct case when status_bayar = "Belum Bayar" then p.id end) as tiket_belum_bayar',
                'count(case when status_bayar = "Dalam Proses Konfirmasi" then pt.id end) + count(distinct case when status_bayar = "Dalam Proses Konfirmasi" then p.id end) as tiket_dalam_proses_konfirmasi',
                'count(case when status_bayar = "Sudah Bayar" then pt.id end) + count(distinct case when status_bayar = "Sudah Bayar" then p.id end) as tiket_sudah_bayar',
                'count(case when status_bayar = "Ditolak" then pt.id end) + count(distinct case when status_bayar = "Ditolak" then p.id end) as tiket_ditolak',
                'count(case when pjpt.nama = "Saintek (IPA)" and status_bayar = "Sudah Bayar" then pt.id end) + count(distinct case when pjp.nama = "Saintek (IPA)" and status_bayar = "Sudah Bayar" then p.id end) as tiket_saintek_ipa',
                'count(case when pjpt.nama = "Soshum (IPS)" and status_bayar = "Sudah Bayar" then pt.id end) + count(distinct case when pjp.nama = "Soshum (IPS)" and status_bayar = "Sudah Bayar" then p.id end) as tiket_soshum_ips',
                'count(distinct case when status_duta = "Sudah Aktif" then p.id end) as duta_transaksi',
                'count(case when id_duta IS NOT NULL and status_bayar = "Sudah Bayar" then pt.id end) + count(distinct case when id_duta IS NOT NULL and status_bayar = "Sudah Bayar" then p.id end) as duta_tiket',
            ])
            ->from('peserta p')
            ->join('RIGHT JOIN', 'periode_kota pk', 'pk.id = p.id_periode_kota AND pk.id_periode = 1 AND status_aktif = "Aktif"')
            ->join('LEFT JOIN', 'peserta_tambahan pt', 'pt.id_peserta = p.id')
            ->join('LEFT JOIN', 'periode_jenis pjp', 'pjp.id = p.id_periode_jenis')
            ->join('LEFT JOIN', 'periode_jenis pjpt', 'pjpt.id = pt.id_periode_jenis')
            ;
        $model['all'] = $query->one();

        $model['peserta'] = Peserta::find()
            ->select([
                'p.id',
                'p.kode',
                'p.nama',
                'p.tagihan',
                'p.tanggal_pembayaran',
            ])
            ->from('peserta p')
            ->where(['p.status_bayar' => 'Dalam Proses Konfirmasi'])
            ->andWhere(['p.status_aktif' => 'Aktif'])
            ->asArray()
            ->all();
        foreach ($model['peserta'] as $key => $peserta) {
            $pos = strpos($peserta['nama'], ' ');
            $firstLetter = strtoupper($peserta['nama'][0]);
            $secondLetter = $pos && isset($peserta['nama'][$pos + 1]) ? strtoupper($peserta['nama'][$pos + 1]) : '';
            $model['peserta'][$key]['alias'] = ($firstLetter . $secondLetter);

            if ($firstLetter >= 'A' && $firstLetter <= 'C') {
                $model['peserta'][$key]['color'] = 'azure';
            } else if ($firstLetter >= 'D' && $firstLetter <= 'F') {
                $model['peserta'][$key]['color'] = 'spring';
            } else if ($firstLetter >= 'G' && $firstLetter <= 'H') {
                $model['peserta'][$key]['color'] = 'red';
            } else if ($firstLetter >= 'I' && $firstLetter <= 'J') {
                $model['peserta'][$key]['color'] = 'yellow';
            } else if ($firstLetter >= 'K' && $firstLetter <= 'L') {
                $model['peserta'][$key]['color'] = 'orange';
            } else if ($firstLetter >= 'M' && $firstLetter <= 'N') {
                $model['peserta'][$key]['color'] = 'chartreuse';
            } else if ($firstLetter >= 'O' && $firstLetter <= 'P') {
                $model['peserta'][$key]['color'] = 'green';
            } else if ($firstLetter >= 'Q' && $firstLetter <= 'R') {
                $model['peserta'][$key]['color'] = 'cyan';
            } else if ($firstLetter >= 'S' && $firstLetter <= 'T') {
                $model['peserta'][$key]['color'] = 'blue';
            } else if ($firstLetter >= 'U' && $firstLetter <= 'V') {
                $model['peserta'][$key]['color'] = 'violet';
            } else if ($firstLetter >= 'W' && $firstLetter <= 'X') {
                $model['peserta'][$key]['color'] = 'magenta';
            } else if ($firstLetter >= 'Y' && $firstLetter <= 'Z') {
                $model['peserta'][$key]['color'] = 'rose';
            }
        }

        return $this->render('peserta', [
            'model' => $model,
            'title' => 'Kelola Peserta',
        ]);
    }

    public function actionKonfirmasi($id)
    {
        $error = true;

        $model['peserta'] = $this->findModelPesertaKonfirmasi($id);
        $periode = Periode::getPeriodeAktif();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['peserta']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                $model['peserta']->confirm_at = new \yii\db\Expression("now()");
                if (!$model['peserta']->save()) {
                    throw new \yii\base\UserException('Peserta gagal dikonfirmasi.');
                }

                $error = false;
                $transaction->commit();

                if ($model['peserta']->status_bayar == 'Sudah Bayar') {
                    Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Peserta berhasil dikonfirmasi.</div>');
                    if (YII_ENV == 'prod') {
                        \Yii::$app->mail->compose('email/email-konfirmasi-diterima', ['model' => $model])
                            ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                            ->setSubject('Tiket Tryout Nasional ' . Yii::$app->params['app.name'] . ' 2019 Milikmu Siap Dicetak')->send();
                    } else if (YII_ENV == 'dev') {
                        /*return $this->render('/email/email-konfirmasi-diterima', [
                            'model' => $model,
                            'idPeriode' => $periode->id,
                            'title' => 'Konfirmasi Diterima',
                        ]);*/
                        \Yii::$app->mail->compose('email/email-konfirmasi-diterima', ['model' => $model])
                            ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                            ->setSubject('Tiket Tryout Nasional MasukPTN 2019 Milikmu Siap Dicetak')->send();
                    }
                } else if ($model['peserta']->status_bayar == 'Ditolak') {
                    Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Pembayaran berhasil ditolak.</div>');
                    if (YII_ENV == 'prod') {
                        \Yii::$app->mail->compose('email/email-konfirmasi-ditolak', ['model' => $model])
                            ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                            ->setSubject('Konfirmasi Pembayaran Ditolak/Bermasalah')->send();
                    } else if (YII_ENV == 'dev') {
                        /*return $this->render('/email/email-konfirmasi-ditolak', [
                            'model' => $model,
                            'idPeriode' => $periode->id,
                            'title' => 'Konfirmasi Ditolak',
                        ]);*/
                        \Yii::$app->mail->compose('email/email-konfirmasi-ditolak', ['model' => $model])
                            ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                            ->setSubject('Konfirmasi Pembayaran Ditolak/Bermasalah')->send();
                    }
                }
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\base\UserException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            $model['peserta']->catatan = 'Terima kasih ya Kak atas Pembayarannya.';
        }

        if ($error)
            return $this->render('form-konfirmasi', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Konfirmasi Pembayaran ' . $model['peserta']->kode,
            ]);
        else
            return $this->redirect(['peserta']);
    }

    public function actionDetailPeserta($id)
    {
        $error = true;

        $model['peserta'] = $this->findModelPeserta($id);
        $periode = Periode::getPeriodeAktif();
        
        foreach ($model['peserta']->pesertaTambahans as $key => $pesertaTambahan)
            $model['peserta_tambahan'][] = $pesertaTambahan;

        if ($error)
            return $this->render('detail-peserta', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Detail Peserta',
            ]);
        else
            return $this->redirect(['xswzaq/index']);
    }

    public function actionUpdatePeserta($id)
    {
        $error = true;

        $model['peserta'] = $this->findModelPeserta($id);
        $periode = Periode::getPeriodeAktif();
        $model['periode_jenis'] = $this->findModelPeriodeJenisByPeriode($periode->id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['peserta']->load($post);
            if (isset($post['PesertaTambahan'])) {
                foreach ($post['PesertaTambahan'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $pesertaTambahan = $this->findModelPesertaTambahan($value['id']);
                        $pesertaTambahan->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $pesertaTambahan = $this->findModelPesertaTambahan(($value['id']*-1));
                        $pesertaTambahan->isDeleted = true;
                    } else {
                        $pesertaTambahan = new PesertaTambahan();
                        $pesertaTambahan->setAttributes($value);
                    }
                    $model['peserta_tambahan'][] = $pesertaTambahan;
                }
            }

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                $periodeJenis = $this->findModelPeriodeJenis($model['peserta']->id_periode_jenis);
                $periodeKota = $this->findModelPeriodeKota($model['peserta']->id_periode_kota);
                // $model['peserta']->id_periode = $periode->id;
                // $model['peserta']->kode = $periode->kode . $periodeKota->kode . $this->sequence('peserta-kode');
                // $model['peserta']->status_bayar = 'Sudah Bayar';

                $jumlahTiket = 1;
                if (isset($model['peserta_tambahan']) and is_array($model['peserta_tambahan'])) {
                    foreach ($model['peserta_tambahan'] as $key => $pesertaTambahan) {
                        if (!$pesertaTambahan->isDeleted) {
                            $jumlahTiket++;
                        }
                    }
                }
                $variableJumlahTiket = 'harga_' . $jumlahTiket . '_tiket';
                
                // $model['peserta']->jumlah_tiket = $jumlahTiket;
                // $model['peserta']->harga = 0;
                // $model['peserta']->periode_penjualan = 'Gratis';
                // $model['peserta']->tagihan = 0;
                // $model['peserta']->harga = $periodeJenis->$variableJumlahTiket;
                // $model['peserta']->periode_penjualan = $periodeJenis->periode_penjualan;
                // $model['peserta']->tagihan = $model['peserta']->harga;

                if (!$model['peserta']->save()) {
                    throw new \yii\base\UserException('Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                if (isset($model['peserta_tambahan']) and is_array($model['peserta_tambahan'])) {
                    foreach ($model['peserta_tambahan'] as $key => $pesertaTambahan) {
                        if ($pesertaTambahan->isDeleted) {
                            if (!$pesertaTambahan->delete()) {
                                $error = true;
                            }
                        } else {
                            if (!$pesertaTambahan->validate(['id_periode_jenis', 'id_periode_kota'])) {
                                $error = true;
                            } else {
                                $periodeJenis = $this->findModelPeriodeJenis($pesertaTambahan->id_periode_jenis);
                                $periodeKota = $this->findModelPeriodeKota($pesertaTambahan->id_periode_kota);
                                // $pesertaTambahan->id_peserta = $model['peserta']->id;
                                // $pesertaTambahan->kode = $periode->kode . $periodeKota->kode . $this->sequence('peserta-kode');
                                // $pesertaTambahan->harga = 0;
                                // $pesertaTambahan->periode_penjualan = 'Gratis';
                                // $pesertaTambahan->harga = $periodeJenis->$variableJumlahTiket;
                                // $pesertaTambahan->periode_penjualan = $periodeJenis->periode_penjualan;
                                // $model['peserta']->tagihan += $pesertaTambahan->harga;
                                if (!$pesertaTambahan->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\base\UserException('Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                /*if (!$model['peserta']->save()) {
                    throw new \yii\base\UserException('Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }*/

                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Data peserta berhasil diupdate.</div>');

                if (YII_ENV == 'prod' && false) {
                    \Yii::$app->mail->compose('email/email-pendaftaran', ['model' => $model])
                        ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                        ->setSubject('Segera Lakukan Pembayaran')->send();
                } else if (YII_ENV == 'dev' && false) {
                    /*return $this->render('/email/email-pendaftaran', [
                        'model' => $model,
                        'idPeriode' => $periode->id,
                        'title' => 'Pendaftaran',
                    ]);*/
                    \Yii::$app->mail->compose('email/email-pendaftaran', ['model' => $model])
                        ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                        ->setSubject('Segera Lakukan Pembayaran')->send();
                }
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\base\UserException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            foreach ($model['peserta']->pesertaTambahans as $key => $pesertaTambahan)
                $model['peserta_tambahan'][] = $pesertaTambahan;

            /*$model['peserta']->nama = 'Fandy Pradana';
            $model['peserta']->email = 'pradana.fandy@gmail.com';
            $model['peserta']->handphone = '08992045553';
            $model['peserta']->id_periode_jenis = '1';
            $model['peserta']->id_periode_kota = '1';
            $model['peserta']->jenis_kelamin = 'Laki-laki';
            $model['peserta']->sekolah = 'SMAN 8 Jakarta';
            $model['peserta']->alamat = 'Jalan Punawarman no 30, Bandung';
            $model['peserta']->facebook = 'prafandy';
            $model['peserta']->twitter = 'prafandy';
            $model['peserta']->instagram = 'prafandy';
            $model['peserta']->line = 'prafandy';
            $model['peserta']->whatsapp = '08992045553';
            $model['peserta']->id_sumber = '1';
            $model['peserta']->id_jurusan = '1';*/
        }

        if ($error)
            return $this->render('form-update-peserta', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Form Update Peserta',
            ]);
        else
            return $this->redirect(['xswzaq/index']);
    }

    public function actionTiketGratis()
    {
        $error = true;

        $model['peserta'] = new Peserta();
        $periode = Periode::getPeriodeAktif();
        $model['periode_jenis'] = $this->findModelPeriodeJenisByPeriode($periode->id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['peserta']->load($post);
            if (isset($post['PesertaTambahan'])) {
                foreach ($post['PesertaTambahan'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $pesertaTambahan = $this->findModelPesertaTambahan($value['id']);
                        $pesertaTambahan->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $pesertaTambahan = $this->findModelPesertaTambahan(($value['id']*-1));
                        $pesertaTambahan->isDeleted = true;
                    } else {
                        $pesertaTambahan = new PesertaTambahan();
                        $pesertaTambahan->setAttributes($value);
                    }
                    $model['peserta_tambahan'][] = $pesertaTambahan;
                }
            }

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                $periodeJenis = $this->findModelPeriodeJenis($model['peserta']->id_periode_jenis);
                $periodeKota = $this->findModelPeriodeKota($model['peserta']->id_periode_kota);
                $model['peserta']->id_periode = $periode->id;
                $model['peserta']->kode = $periode->kode . $periodeKota->kode . $this->sequence('peserta-kode');
                $model['peserta']->status_bayar = 'Sudah Bayar';

                $jumlahTiket = 1;
                if (isset($model['peserta_tambahan']) and is_array($model['peserta_tambahan'])) {
                    foreach ($model['peserta_tambahan'] as $key => $pesertaTambahan) {
                        if (!$pesertaTambahan->isDeleted) {
                            $jumlahTiket++;
                        }
                    }
                }
                $variableJumlahTiket = 'harga_' . $jumlahTiket . '_tiket';
                
                $model['peserta']->jumlah_tiket = $jumlahTiket;
                $model['peserta']->harga = 0;
                $model['peserta']->periode_penjualan = 'Gratis';
                $model['peserta']->tagihan = 0;
                // $model['peserta']->harga = $periodeJenis->$variableJumlahTiket;
                // $model['peserta']->periode_penjualan = $periodeJenis->periode_penjualan;
                // $model['peserta']->tagihan = $model['peserta']->harga;

                if (!$model['peserta']->save()) {
                    throw new \yii\base\UserException('Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                if (isset($model['peserta_tambahan']) and is_array($model['peserta_tambahan'])) {
                    foreach ($model['peserta_tambahan'] as $key => $pesertaTambahan) {
                        if ($pesertaTambahan->isDeleted) {
                            if (!$pesertaTambahan->delete()) {
                                $error = true;
                            }
                        } else {
                            if (!$pesertaTambahan->validate(['id_periode_jenis', 'id_periode_kota'])) {
                                $error = true;
                            } else {
                                $periodeJenis = $this->findModelPeriodeJenis($pesertaTambahan->id_periode_jenis);
                                $periodeKota = $this->findModelPeriodeKota($pesertaTambahan->id_periode_kota);
                                $pesertaTambahan->id_peserta = $model['peserta']->id;
                                $pesertaTambahan->kode = $periode->kode . $periodeKota->kode . $this->sequence('peserta-kode');
                                $pesertaTambahan->harga = 0;
                                $pesertaTambahan->periode_penjualan = 'Gratis';
                                // $pesertaTambahan->harga = $periodeJenis->$variableJumlahTiket;
                                // $pesertaTambahan->periode_penjualan = $periodeJenis->periode_penjualan;
                                // $model['peserta']->tagihan += $pesertaTambahan->harga;
                                if (!$pesertaTambahan->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\base\UserException('Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                /*if (!$model['peserta']->save()) {
                    throw new \yii\base\UserException('Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }*/

                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Tiket gratis berhasil ditambahkan.</div>');

                if (YII_ENV == 'prod') {
                    \Yii::$app->mail->compose('email/email-tiket-gratis', ['model' => $model])
                        ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                        ->setSubject('Selamat Anda Telah Terdaftar')->send();
                } else if (YII_ENV == 'dev') {
                    return $this->render('/email/email-tiket-gratis', [
                        'model' => $model,
                        'idPeriode' => $periode->id,
                        'title' => 'Konfirmasi Diterima',
                    ]);
                    \Yii::$app->mail->compose('email/email-tiket-gratis', ['model' => $model])
                        ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                        ->setSubject('Selamat Anda Telah Terdaftar')->send();
                }
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\base\UserException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            $model['peserta']->catatan = 'Semoga berhasil ya Kak !';

            foreach ($model['peserta']->pesertaTambahans as $key => $pesertaTambahan)
                $model['peserta_tambahan'][] = $pesertaTambahan;

            /*$model['peserta']->nama = 'Fandy Pradana';
            $model['peserta']->email = 'pradana.fandy@gmail.com';
            $model['peserta']->handphone = '08992045553';
            $model['peserta']->id_periode_jenis = '1';
            $model['peserta']->id_periode_kota = '1';
            $model['peserta']->jenis_kelamin = 'Laki-laki';
            $model['peserta']->sekolah = 'SMAN 8 Jakarta';
            $model['peserta']->alamat = 'Jalan Punawarman no 30, Bandung';
            $model['peserta']->facebook = 'prafandy';
            $model['peserta']->twitter = 'prafandy';
            $model['peserta']->instagram = 'prafandy';
            $model['peserta']->line = 'prafandy';
            $model['peserta']->whatsapp = '08992045553';
            $model['peserta']->id_sumber = '1';
            $model['peserta']->id_jurusan = '1';*/
        }

        if ($error)
            return $this->render('form-tiket-gratis', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Form Tiket Gratis',
            ]);
        else
            return $this->redirect(['xswzaq/index']);
    }

    public function actionAktifkanPeserta($id)
    {
        $error = true;

        $model['peserta'] = $this->findModelPesertaAktifkan($id);
        $periode = Periode::getPeriodeAktif();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['peserta']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                $model['peserta']->status_aktif = 'Aktif';
                if (!$model['peserta']->save()) {
                    throw new \yii\base\UserException('Peserta gagal diaktifkan.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Peserta berhasil diaktifkan.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\base\UserException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        return $this->redirect(['index']);
    }

    public function actionNonaktifkanPeserta($id)
    {
        $error = true;

        $model['peserta'] = $this->findModelPesertaNonaktifkan($id);
        $periode = Periode::getPeriodeAktif();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['peserta']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                $model['peserta']->status_aktif = 'Nonaktif';
                $model['peserta']->nonaktif_at = new \yii\db\Expression('now()');
                if (!$model['peserta']->save()) {
                    throw new \yii\base\UserException('Peserta gagal dinonaktifkan.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Peserta berhasil dinonaktifkan.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\base\UserException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        return $this->redirect(['index']);
    }

    public function actionAktifkanDuta($id)
    {
        $error = true;

        $model['peserta'] = $this->findModelDutaAktifkan($id);
        $periode = Periode::getPeriodeAktif();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['peserta']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                $model['peserta']->status_duta = 'Sudah Aktif';
                if (!$model['peserta']->save()) {
                    throw new \yii\base\UserException('Duta gagal diaktifkan.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Duta berhasil diaktifkan.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\base\UserException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        return $this->redirect(['index']);
    }

    public function actionNonaktifkanDuta($id)
    {
        $error = true;

        $model['peserta'] = $this->findModelDutaNonaktifkan($id);
        $periode = Periode::getPeriodeAktif();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['peserta']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                $model['peserta']->status_duta = 'Tidak Aktif';
                // $model['peserta']->nonaktif_at = new \yii\db\Expression('now()');
                if (!$model['peserta']->save()) {
                    throw new \yii\base\UserException('Duta gagal dinonaktifkan.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Duta berhasil dinonaktifkan.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\base\UserException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        return $this->redirect(['index']);
    }

    public function actionResendPendaftaran($id)
    {
        $model['peserta'] = $this->findModelPeserta($id);
        $periode = Periode::getPeriodeAktif();
        
        Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Email pendaftaran diterima berhasil dikirim ulang.</div>');
        if (YII_ENV == 'prod') {
            \Yii::$app->mail->compose('email/email-pendaftaran', ['model' => $model])
                ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                ->setSubject('Segera Lakukan Pembayaran')->send();
        } else if (YII_ENV == 'dev') {
            /*return $this->render('/email/email-pendaftaran', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Pendaftaran',
            ]);*/
            \Yii::$app->mail->compose('email/email-pendaftaran', ['model' => $model])
                ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                ->setSubject('Segera Lakukan Pembayaran')->send();
        }

        return $this->redirect(['index']);
    }

    public function actionResendKonfirmasi($id)
    {
        $model['peserta'] = $this->findModelPeserta($id);
        $periode = Periode::getPeriodeAktif();
        
        Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Email dalam proses konfirmasi berhasil dikirim ulang.</div>');
        if (YII_ENV == 'prod') {
            \Yii::$app->mail->compose('email/email-konfirmasi', ['model' => $model])
                ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                ->setSubject('Konfirmasi Pembayaran Sedang Diproses')->send();
        } else if (YII_ENV == 'dev') {
            /*return $this->render('/email/email-konfirmasi', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'konfirmasi',
            ]);*/
            \Yii::$app->mail->compose('email/email-konfirmasi', ['model' => $model])
                ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                ->setSubject('Konfirmasi Pembayaran Sedang Diproses')->send();
        }

        return $this->redirect(['index']);
    }

    public function actionResendDiterima($id)
    {
        $model['peserta'] = $this->findModelPeserta($id);
        $periode = Periode::getPeriodeAktif();
        
        Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Email konfirmasi diterima berhasil dikirim ulang.</div>');
        if (YII_ENV == 'prod') {
            \Yii::$app->mail->compose('email/email-konfirmasi-diterima', ['model' => $model])
                ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                ->setSubject('Pembayaran Sudah Diterima')->send();
        } else if (YII_ENV == 'dev') {
            /*return $this->render('/email/email-konfirmasi-diterima', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Konfirmasi Diterima',
            ]);*/
            \Yii::$app->mail->compose('email/email-konfirmasi-diterima', ['model' => $model])
                ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                ->setSubject('Pembayaran Sudah Diterima')->send();
        }

        return $this->redirect(['index']);
    }

    public function actionResendDitolak($id)
    {
        $model['peserta'] = $this->findModelPeserta($id);
        $periode = Periode::getPeriodeAktif();

        Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Email konfirmasi ditolak berhasil dikirim ulang.</div>');
        if (YII_ENV == 'prod') {
            \Yii::$app->mail->compose('email/email-konfirmasi-ditolak', ['model' => $model])
                ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                ->setSubject('Konfirmasi Pembayaran Ditolak/Bermasalah')->send();
        } else if (YII_ENV == 'dev') {
            /*return $this->render('/email/email-konfirmasi-ditolak', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Konfirmasi Ditolak',
            ]);*/
            \Yii::$app->mail->compose('email/email-konfirmasi-ditolak', ['model' => $model])
                ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                ->setSubject('Konfirmasi Pembayaran Ditolak/Bermasalah')->send();
        }

        return $this->redirect(['index']);
    }

    //////////////

    public function actionSetting()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        return $this->render('setting', [
            // 'model' => $model,
            'title' => 'Setting',
        ]);
    }

    public function actionDatatablesPeriodeKota()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'pk.id',
                'pk.id_periode',
                'r.name as kota',
                'pk.kode',
                'pk.nama',
                'pk.status',
                'pk.alamat',
                'pk.kuota',
            ])
            ->from('periode_kota pk')
            ->join('INNER JOIN', 'regencies r', 'r.id = pk.id_regencies')
            ->where(['id_periode' => (Periode::getPeriodeAktif()->id)])
        ;


        return $this->datatables($query, $post = Yii::$app->request->post(), PeriodeKota::getDb());
    }

    public function actionUpdatePeriodeKota($id)
    {
        $error = true;

        $model['periode_kota'] = $this->findModelPeriodeKota($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['periode_kota']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                if ($model['periode_kota']->isNewRecord) {
                    $model['periode_kota']->id_periode = (Periode::getPeriodeAktif())->id;
                }
                if (!$model['periode_kota']->save()) {
                    throw new \yii\base\UserException('Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Data kota berhasil diupdate.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\base\UserException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-periode_kota', [
                'model' => $model,
                'title' => 'Form Update Kota',
            ]);
        else
            return $this->redirect(['xswzaq/setting']);
    }

    public function actionCreatePeriodeKota()
    {
        $error = true;

        $model['periode_kota'] = new PeriodeKota();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['periode_kota']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                if ($model['periode_kota']->isNewRecord) {
                    $model['periode_kota']->id_periode = (Periode::getPeriodeAktif())->id;
                }
                if (!$model['periode_kota']->save()) {
                    throw new \yii\base\UserException('Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Data kota berhasil diupdate.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\base\UserException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-periode_kota', [
                'model' => $model,
                'title' => 'Form Tambah Kota',
            ]);
        else
            return $this->redirect(['xswzaq/setting']);
    }

    public function actionAktifkanPeriodeKota($id)
    {
        $error = true;

        $model['periode_kota'] = $this->findModelPeriodeKotaAktifkan($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['periode_kota']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                $model['periode_kota']->status = 'Sedang Aktif';
                if (!$model['periode_kota']->save()) {
                    throw new \yii\base\UserException('Kota gagal diaktifkan.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Kota berhasil diaktifkan.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\base\UserException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        return $this->redirect(['setting']);
    }

    public function actionNonaktifkanPeriodeKota($id)
    {
        $error = true;

        $model['periode_kota'] = $this->findModelPeriodeKotaNonaktifkan($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['periode_kota']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                $model['periode_kota']->status = 'Tidak Aktif';
                if (!$model['periode_kota']->save()) {
                    throw new \yii\base\UserException('Kota gagal dinonaktifkan.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Kota berhasil dinonaktifkan.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\base\UserException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        return $this->redirect(['setting']);
    }

    public function actionDetailPeriodeKota($id)
    {
        $error = true;

        $model['periode_kota'] = $this->findModelPeriodeKota($id);
        
        if ($error)
            return $this->render('detail-periode_kota', [
                'model' => $model,
                'title' => 'Detail Kota',
            ]);
        else
            return $this->redirect(['xswzaq/setting']);
    }

    public function actionGetListKota($idProvinces = null)
    {
        $regenciesOriginal = \technosmart\modules\location\models\Regencies::find()->where(['province_id' => $idProvinces])->orderBy('name')->asArray()->all();
        $regencies = [];
        foreach ($regenciesOriginal as $key => $value) {
            $regencies[] = [
                'value' => $value['id'],
                'text' => $value['id'] . ' - '. ucwords(strtolower($value['name'])),
            ];
        }
        return $this->json($regencies);
    }

    public function actionDatatablesPeriodeJenis()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'pj.id',
                'pj.id_periode',
                'pj.nama',
                'pj.status',
                'pj.periode_penjualan',
                'FORMAT(pj.harga_1_tiket, 2) AS harga_1_tiket',
                'FORMAT(pj.harga_2_tiket, 2) AS harga_2_tiket',
                'FORMAT(pj.harga_3_tiket, 2) AS harga_3_tiket',
                'FORMAT(pj.harga_4_tiket, 2) AS harga_4_tiket',
                'FORMAT(pj.harga_5_tiket, 2) AS harga_5_tiket',
                'FORMAT(pj.harga_6_tiket, 2) AS harga_6_tiket',
                'FORMAT(pj.harga_7_tiket, 2) AS harga_7_tiket',
                'FORMAT(pj.harga_8_tiket, 2) AS harga_8_tiket',
                'FORMAT(pj.harga_9_tiket, 2) AS harga_9_tiket',
                'FORMAT(pj.harga_10_tiket, 2) AS harga_10_tiket',
            ])
            ->from('periode_jenis pj')
            ->where(['id_periode' => (Periode::getPeriodeAktif()->id)])
        ;
        return $this->datatables($query, $post = Yii::$app->request->post(), PeriodeJenis::getDb());
    }

    public function actionUpdatePeriodeJenis($id)
    {
        $error = true;

        $model['periode_jenis'] = $this->findModelPeriodeJenis($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['periode_jenis']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                if ($model['periode_jenis']->isNewRecord) {
                    $model['periode_jenis']->id_periode = (Periode::getPeriodeAktif())->id;
                }
                if (!$model['periode_jenis']->save()) {
                    throw new \yii\base\UserException('Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Data jenis, periode & harga berhasil diupdate.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\base\UserException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-periode_jenis', [
                'model' => $model,
                'title' => 'Form Update Jenis, Periode & Harga',
            ]);
        else
            return $this->redirect(['xswzaq/setting']);
    }

    public function actionCreatePeriodeJenis()
    {
        $error = true;

        $model['periode_jenis'] = new PeriodeJenis();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['periode_jenis']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                if ($model['periode_jenis']->isNewRecord) {
                    $model['periode_jenis']->id_periode = (Periode::getPeriodeAktif())->id;
                }
                if (!$model['periode_jenis']->save()) {
                    throw new \yii\base\UserException('Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Data jenis, periode & harga berhasil diupdate.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\base\UserException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-periode_jenis', [
                'model' => $model,
                'title' => 'Form Tambah Jenis, Periode & Harga',
            ]);
        else
            return $this->redirect(['xswzaq/setting']);
    }

    public function actionAktifkanPeriodeJenis($id)
    {
        $error = true;

        $model['periode_jenis'] = $this->findModelPeriodeJenisAktifkan($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['periode_jenis']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                $model['periode_jenis']->status = 'Sedang Aktif';
                if (!$model['periode_jenis']->save()) {
                    throw new \yii\base\UserException('Jenis, Periode & Harga gagal diaktifkan.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Jenis, Periode & Harga berhasil diaktifkan.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\base\UserException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        return $this->redirect(['setting']);
    }

    public function actionNonaktifkanPeriodeJenis($id)
    {
        $error = true;

        $model['periode_jenis'] = $this->findModelPeriodeJenisNonaktifkan($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['periode_jenis']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                $model['periode_jenis']->status = 'Tidak Aktif';
                if (!$model['periode_jenis']->save()) {
                    throw new \yii\base\UserException('Jenis, Periode & Harga gagal dinonaktifkan.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Jenis, Periode & Harga berhasil dinonaktifkan.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\base\UserException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        return $this->redirect(['setting']);
    }

    public function actionDetailPeriodeJenis($id)
    {
        $error = true;

        $model['periode_jenis'] = $this->findModelPeriodeJenis($id);
        
        if ($error)
            return $this->render('detail-periode_jenis', [
                'model' => $model,
                'title' => 'Detail Jenis, Periode & Harga',
            ]);
        else
            return $this->redirect(['xswzaq/setting']);
    }

    public function actionPanitia()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        return $this->render('panitia', [
            // 'model' => $model,
            'title' => 'Panitia',
        ]);
    }
}