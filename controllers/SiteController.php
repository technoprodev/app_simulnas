<?php
namespace app_simulnas\controllers;

use Yii;
use technosmart\controllers\SiteController as SiteControl;
use app_tryout\models\User;
use app_tryout\models\Login;

class SiteController extends SiteControl
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest)
            return $this->goHome();

        $model['login'] = new Login(['scenario' => 'using-login']);
        
        if ($model['login']->load(Yii::$app->request->post()) && $model['login']->login()) {
            return $this->redirect(['xswzaq/index']);
        } else {
            $this->layout = 'auth';
            return $this->render('login', [
                'model' => $model,
                'title' => 'Login',
            ]);
        }
    }
}