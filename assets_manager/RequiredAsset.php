<?php
namespace app_simulnas\assets_manager;

use yii\web\AssetBundle;

class RequiredAsset extends AssetBundle
{
	public $sourcePath = '@app_simulnas/assets';
    
    public $css = [];
    
    public $js = [];
    
    public $depends = [
        'technosmart\assets_manager\RequiredAsset',
    ];
}