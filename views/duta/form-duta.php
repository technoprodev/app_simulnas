<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

/*$this->registerJsFile('@web/app/dev/form.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);*/

// technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
// technosmart\assets_manager\AutosizeAsset::register($this);
// technosmart\assets_manager\FileInputAsset::register($this);
// technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
// technosmart\assets_manager\JqueryMaskedInputAsset::register($this);

//
/*$devChildren = [];
if (isset($model['dev_child']))
    foreach ($model['dev_child'] as $key => $devChild)
        $devChildren[] = $devChild->attributes;

$this->registerJs(
    'vm.$data.dev.virtual_category = ' . json_encode($model['duta']->virtual_category) . ';' .
    'vm.$data.dev.devChildren = vm.$data.dev.devChildren.concat(' . json_encode($devChildren) . ');',
    // 'vm.$data.dev.devChildren = Object.assign({}, vm.$data.dev.devChildren, ' . json_encode($devChildren) . ');',
    3
);*/

//
$errorMessage = '';
$errorVue = false;
if ($model['duta']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['duta'], ['class' => '']);
}

/*if (isset($model['dev_child'])) foreach ($model['dev_child'] as $key => $devChild) {
    if ($devChild->hasErrors()) {
        $errorMessage .= Html::errorSummary($devChild, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}*/
$jumlahTiket = 0;
if ($model['duta']->status_bayar == 'Sudah Bayar') {
    $jumlahTiket++;
}
foreach ($model['duta']->pesertaTambahans as $key => $pesertaTambahan) {
    if ($model['duta']->status_bayar == 'Sudah Bayar') {
        $jumlahTiket++;
    }
}
foreach ($model['duta']->pesertas as $key => $peserta) {
    if ($peserta->status_bayar == 'Sudah Bayar') {
        $jumlahTiket++;
    }
    foreach ($peserta->pesertaTambahans as $key => $pesertaTambahan) {
        if ($peserta->status_bayar == 'Sudah Bayar') {
            $jumlahTiket++;
        }
    }
}
?>
<style type="text/css">
.form-text:focus,
.form-textarea:focus,
.form-dropdown:focus {
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
}
</style>

<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<?php if (!$model['duta']->isNewRecord) : ?>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-azure text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Akses Khusus Duta
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-red" style="max-width: 900px; width: 100%; margin-left: auto; margin-right: auto;">

    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left text-gray">Total tiket :</div>
        <div class="box-10 m-padding-x-0 text-dark"><?= $jumlahTiket ?></div>
    </div>
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left text-gray">Total poin :</div>
        <div class="box-10 m-padding-x-0 text-dark"><?= $jumlahTiket * 10 ?></div>
    </div>

    <div class="margin-bottom-15"></div>

    <div class="scroll-x">
    <table class="table margin-0">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Kontak</th>
                <th>Jenis</th>
                <th>Lokasi</th>
                <th>Status</th>
                <th>Kartu Ujian</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <div class="fs-14 text-dark"><?= $model['duta']->nama ?></div>
                    <div>
                        <span class="text-gray">kode: </span>
                        <span class="text-dark"><?= $model['duta']->kode ?></span>
                    </div>
                </td>
                <td>
                    <div>
                        <i class="fa fa-envelope margin-right-5 text-gray" style="width: 10px;"></i>
                        <span class="text-dark"><?= $model['duta']->email ?></span>
                    </div>
                    <div>
                        <i class="fa fa-phone margin-right-5 text-gray" style="width: 10px;"></i>
                        <span class="text-dark"><?= $model['duta']->handphone ?></span>
                    </div>
                </td>
                <td>
                    <div class="fs-14 text-dark"><?= $model['duta']->periodeJenis->nama ?></div>
                    <div>
                        <span class="text-gray">Rp </span>
                        <span class="text-dark"><?= number_format($model['duta']->harga, 2) ?></span>
                    </div>
                </td>
                <td>
                    <div>
                        <i class="fa fa-map-marker margin-right-5 text-gray"></i>
                        <span class="text-dark"><?= $model['duta']->periodeKota->nama ?></span>
                    </div>
                    <div>
                        <?= Html::a('Lihat Detail', ['detail-kota', 'id' => $model['duta']->id_periode_kota], ['modal-md' => '', 'modal-title' => 'Detail Lokasi']) ?>
                    </div>
                </td>
                <td class="<?= $model['duta']->status_bayar == 'Belum Bayar' || $model['duta']->status_bayar == 'Ditolak' ? 'text-red' : ($model['duta']->status_bayar == 'Dalam Proses Konfirmasi' ? 'text-orange' : 'text-azure') ?>">
                    <i class="fa fa-circle margin-right-2"></i>
                    <?= $model['duta']->status_bayar ?>
                </td>
                <td>
                    <?php if ($model['duta']->status_bayar == 'Belum Bayar') : ?>
                        Belum tersedia
                    <?php elseif ($model['duta']->status_bayar == 'Ditolak') : ?>
                        Belum tersedia
                    <?php elseif ($model['duta']->status_bayar == 'Dalam Proses Konfirmasi') : ?>
                        Belum tersedia
                    <?php elseif ($model['duta']->status_bayar == 'Sudah Bayar') : ?>
                        <?= Html::a('Download', ['/peserta/download-kartu-ujian', 'kode' => $model['duta']->kode, 'email' => $model['duta']->email], ['class' => 'button button-sm border-azure text-azure']) ?>
                    <?php endif; ?>
                </td>
            </tr>
            <?php foreach ($model['duta']->pesertaTambahans as $key => $pesertaTambahan) : ?>
                <tr>
                    <td>
                        <div class="fs-14 text-dark"><?= $pesertaTambahan->nama ?></div>
                        <div>
                            <span class="text-gray">kode: </span>
                            <span class="text-dark"><?= $pesertaTambahan->kode ?></span>
                        </div>
                    </td>
                    <td>
                        <div>
                            <i class="fa fa-envelope margin-right-5 text-gray" style="width: 10px;"></i>
                            <span class="text-dark"><?= $pesertaTambahan->email ?></span>
                        </div>
                        <div>
                            <i class="fa fa-phone margin-right-5 text-gray" style="width: 10px;"></i>
                            <span class="text-dark"><?= $pesertaTambahan->handphone ?></span>
                        </div>
                    </td>
                    <td>
                        <div class="fs-14 text-dark"><?= $pesertaTambahan->periodeJenis->nama ?></div>
                        <div>
                            <span class="text-gray">Rp </span>
                            <span class="text-dark"><?= number_format($pesertaTambahan->harga, 2) ?></span>
                        </div>
                    </td>
                    <td>
                        <div>
                            <i class="fa fa-map-marker margin-right-5 text-gray"></i>
                            <span class="text-dark"><?= $pesertaTambahan->periodeKota->nama ?></span>
                        </div>
                        <div>
                            <?= Html::a('Lihat Detail', ['detail-kota', 'id' => $pesertaTambahan->id_periode_kota], ['modal-md' => '', 'modal-title' => 'Detail Lokasi']) ?>
                        </div>
                    </td>
                    <td class="<?= $model['duta']->status_bayar == 'Belum Bayar' || $model['duta']->status_bayar == 'Ditolak' ? 'text-red' : ($model['duta']->status_bayar == 'Dalam Proses Konfirmasi' ? 'text-orange' : 'text-azure') ?>">
                        <i class="fa fa-circle margin-right-2"></i>
                        <?= $model['duta']->status_bayar ?>
                    </td>
                    <td>
                        <?php if ($model['duta']->status_bayar == 'Belum Bayar') : ?>
                            Belum tersedia
                        <?php elseif ($model['duta']->status_bayar == 'Ditolak') : ?>
                            Belum tersedia
                        <?php elseif ($model['duta']->status_bayar == 'Dalam Proses Konfirmasi') : ?>
                            Belum tersedia
                        <?php elseif ($model['duta']->status_bayar == 'Sudah Bayar') : ?>
                            <?= Html::a('Download', ['/peserta/download-kartu-ujian', 'kode' => $pesertaTambahan->kode, 'email' => $pesertaTambahan->email], ['class' => 'button button-sm border-azure text-azure']) ?>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php foreach ($model['duta']->pesertas as $key => $peserta) : ?>
                <tr>
                    <td>
                        <div class="fs-14 text-dark"><?= $peserta->nama ?></div>
                        <div>
                            <span class="text-gray">kode: </span>
                            <span class="text-dark"><?= $peserta->kode ?></span>
                        </div>
                    </td>
                    <td>
                        <div>
                            <i class="fa fa-envelope margin-right-5 text-gray" style="width: 10px;"></i>
                            <span class="text-dark"><?= $peserta->email ?></span>
                        </div>
                        <div>
                            <i class="fa fa-phone margin-right-5 text-gray" style="width: 10px;"></i>
                            <span class="text-dark"><?= $peserta->handphone ?></span>
                        </div>
                    </td>
                    <td>
                        <div class="fs-14 text-dark"><?= $peserta->periodeJenis->nama ?></div>
                        <div>
                            <span class="text-gray">Rp </span>
                            <span class="text-dark"><?= number_format($peserta->harga, 2) ?></span>
                        </div>
                    </td>
                    <td>
                        <div>
                            <i class="fa fa-map-marker margin-right-5 text-gray"></i>
                            <span class="text-dark"><?= $peserta->periodeKota->nama ?></span>
                        </div>
                        <div>
                            <?= Html::a('Lihat Detail', ['detail-kota', 'id' => $peserta->id_periode_kota], ['modal-md' => '', 'modal-title' => 'Detail Lokasi']) ?>
                        </div>
                    </td>
                    <td class="<?= $peserta->status_bayar == 'Belum Bayar' || $peserta->status_bayar == 'Ditolak' ? 'text-red' : ($peserta->status_bayar == 'Dalam Proses Konfirmasi' ? 'text-orange' : 'text-azure') ?>">
                        <i class="fa fa-circle margin-right-2"></i>
                        <?= $peserta->status_bayar ?>
                    </td>
                    <td>
                        <?php if ($peserta->status_bayar == 'Belum Bayar') : ?>
                            Belum tersedia
                        <?php elseif ($peserta->status_bayar == 'Ditolak') : ?>
                            Belum tersedia
                        <?php elseif ($peserta->status_bayar == 'Dalam Proses Konfirmasi') : ?>
                            Belum tersedia
                        <?php elseif ($peserta->status_bayar == 'Sudah Bayar') : ?>
                            <?= Html::a('Download', ['/peserta/download-kartu-ujian', 'kode' => $peserta->kode, 'email' => $peserta->email], ['class' => 'button button-sm border-azure text-azure']) ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <?php foreach ($peserta->pesertaTambahans as $key => $pesertaTambahan) : ?>
                    <tr>
                        <td>
                            <div class="fs-14 text-dark"><?= $pesertaTambahan->nama ?></div>
                            <div>
                                <span class="text-gray">kode: </span>
                                <span class="text-dark"><?= $pesertaTambahan->kode ?></span>
                            </div>
                        </td>
                        <td>
                            <div>
                                <i class="fa fa-envelope margin-right-5 text-gray" style="width: 10px;"></i>
                                <span class="text-dark"><?= $pesertaTambahan->email ?></span>
                            </div>
                            <div>
                                <i class="fa fa-phone margin-right-5 text-gray" style="width: 10px;"></i>
                                <span class="text-dark"><?= $pesertaTambahan->handphone ?></span>
                            </div>
                        </td>
                        <td>
                            <div class="fs-14 text-dark"><?= $pesertaTambahan->periodeJenis->nama ?></div>
                            <div>
                                <span class="text-gray">Rp </span>
                                <span class="text-dark"><?= number_format($pesertaTambahan->harga, 2) ?></span>
                            </div>
                        </td>
                        <td>
                            <div>
                                <i class="fa fa-map-marker margin-right-5 text-gray"></i>
                                <span class="text-dark"><?= $pesertaTambahan->periodeKota->nama ?></span>
                            </div>
                            <div>
                                <?= Html::a('Lihat Detail', ['detail-kota', 'id' => $pesertaTambahan->id_periode_kota], ['modal-md' => '', 'modal-title' => 'Detail Lokasi']) ?>
                            </div>
                        </td>
                        <td class="<?= $peserta->status_bayar == 'Belum Bayar' || $peserta->status_bayar == 'Ditolak' ? 'text-red' : ($peserta->status_bayar == 'Dalam Proses Konfirmasi' ? 'text-orange' : 'text-azure') ?>">
                            <i class="fa fa-circle margin-right-2"></i>
                            <?= $peserta->status_bayar ?>
                        </td>
                        <td>
                            <?php if ($peserta->status_bayar == 'Belum Bayar') : ?>
                                Belum tersedia
                            <?php elseif ($peserta->status_bayar == 'Ditolak') : ?>
                                Belum tersedia
                            <?php elseif ($peserta->status_bayar == 'Dalam Proses Konfirmasi') : ?>
                                Belum tersedia
                            <?php elseif ($peserta->status_bayar == 'Sudah Bayar') : ?>
                                <?= Html::a('Download', ['/peserta/download-kartu-ujian', 'kode' => $pesertaTambahan->kode, 'email' => $pesertaTambahan->email], ['class' => 'button button-sm border-azure text-azure']) ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>

    <div class="margin-top-30"></div>

    <div class="clearfix">
        <?= Html::a('Tambah Tiket', ['pendaftaran', 'kode' => $model['duta']->kode, 'email' => $model['duta']->email], ['class' => 'button button-sm border-azure bg-azure pull-left m-pull-none m-button-block']) ?>
        <?= Html::a('Logout', ['index'], ['class' => 'button button-sm border-azure text-azure pull-right m-pull-none m-button-block']) ?>
    </div>

    </div>
</div>

<div class="margin-top-30"></div>

<?php elseif (!$newSearch): ?>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-azure text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Akses Khusus Duta
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-red" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">
        
    <div class="text-red text-uppercase text-center">
        <div>
            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/error.png" width="50px;">
        </div>
        <div class="margin-top-15"></div>
        <div>
            <span class="fs-20 m-fs-18">Data Tidak Ditemukan</span>
        </div>
    </div>

    </div>
</div>

<div class="margin-top-30"></div>

<?php endif; ?>

<?php if ($model['duta']->isNewRecord) : ?>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    <?php if (!$newSearch): ?>
        Harap perbaiki kesalahan sebelum melakukan pencarian kembali
    <?php else: ?>
        Isi kode peserta dan email Kamu untuk mengakses halaman khusus duta
    <?php endif; ?>

    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-red" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
      
        <div class="box box-break-sm box-gutter box-equal">
            <div class="box-6">
                <?= $form->field($model['duta'], 'kode', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['duta'], 'kode', ['class' => 'form-label fw-bold', 'label' =>'Kode peserta']); ?>
                     <!-- <span class="margin-y-5 fw-normal">- Kode pendaftaran yang Kamu dapatkan melalui email</span> -->
                    <?= Html::activeTextInput($model['duta'], 'kode', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
                    <?= Html::error($model['duta'], 'kode', ['class' => 'form-info']); ?>
                <?= $form->field($model['duta'], 'kode')->end(); ?>
            </div>
            <div class="box-6">
                <?= $form->field($model['duta'], 'email', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['duta'], 'email', ['class' => 'form-label fw-bold', 'label' =>'Email']); ?>
                     <!-- <span class="margin-y-5 fw-normal">- Alamat email yang Kamu gunakan untuk mendaftar.</span> -->
                    <?= Html::activeTextInput($model['duta'], 'email', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
                    <?= Html::error($model['duta'], 'email', ['class' => 'form-info']); ?>
                <?= $form->field($model['duta'], 'email')->end(); ?>
            </div>
        </div>

        <div class="margin-top-30"></div>
        
        <div class="form-wrapper clearfix">
            <?= Html::submitButton('<i class="fa fa-user margin-right-5"></i> Masuk', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

    </div>
</div>

<?php endif; ?>

<div class="margin-top-50"></div>
