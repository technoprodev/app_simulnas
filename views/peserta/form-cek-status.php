<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

/*$this->registerJsFile('@web/app/dev/form.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);*/

// technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
// technosmart\assets_manager\AutosizeAsset::register($this);
// technosmart\assets_manager\FileInputAsset::register($this);
// technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
// technosmart\assets_manager\JqueryMaskedInputAsset::register($this);

//
/*$devChildren = [];
if (isset($model['dev_child']))
    foreach ($model['dev_child'] as $key => $devChild)
        $devChildren[] = $devChild->attributes;

$this->registerJs(
    'vm.$data.dev.virtual_category = ' . json_encode($model['peserta']->virtual_category) . ';' .
    'vm.$data.dev.devChildren = vm.$data.dev.devChildren.concat(' . json_encode($devChildren) . ');',
    // 'vm.$data.dev.devChildren = Object.assign({}, vm.$data.dev.devChildren, ' . json_encode($devChildren) . ');',
    3
);*/

//
$errorMessage = '';
$errorVue = false;
if ($model['peserta']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['peserta'], ['class' => '']);
}

/*if (isset($model['dev_child'])) foreach ($model['dev_child'] as $key => $devChild) {
    if ($devChild->hasErrors()) {
        $errorMessage .= Html::errorSummary($devChild, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}*/
?>
<style type="text/css">
.form-text:focus,
.form-textarea:focus,
.form-dropdown:focus {
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
}
</style>

<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<?php if (!$model['peserta']->isNewRecord) : ?>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-azure text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Hasil Pencarian
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-red" style="max-width: 900px; width: 100%; margin-left: auto; margin-right: auto;">

    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left text-gray">Total tiket :</div>
        <div class="box-10 m-padding-x-0 text-dark"><?= $model['peserta']->jumlah_tiket ?></div>
    </div>
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left text-gray">Total tagihan :</div>
        <div class="box-10 m-padding-x-0 text-spring">Rp <?= number_format($model['peserta']->tagihan, 2) ?></div>
    </div>
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left text-gray">Status pembayaran : </div>
        <div class="box-10 m-padding-x-0 <?= $model['peserta']->status_bayar == 'Belum Bayar' || $model['peserta']->status_bayar == 'Ditolak' ? 'text-red' : $model['peserta']->status_bayar == 'Dalam Proses Konfirmasi' ? 'text-orange' : 'text-azure' ?>">
            <i class="fa fa-circle margin-right-2"></i>
            <?= $model['peserta']->status_bayar ?>
        </div>
    </div>

    <div class="margin-bottom-15"></div>

    <div class="scroll-x">
    <table class="table margin-0">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Kontak</th>
                <th>Jenis</th>
                <th>Lokasi</th>
                <th>Sertifikat</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <div class="fs-14 text-dark"><?= $model['peserta']->nama ?></div>
                    <div>
                        <span class="text-gray">kode: </span>
                        <span class="text-dark"><?= $model['peserta']->kode ?></span>
                    </div>
                </td>
                <td>
                    <div>
                        <i class="fa fa-envelope margin-right-5 text-gray" style="width: 10px;"></i>
                        <span class="text-dark"><?= $model['peserta']->email ?></span>
                    </div>
                    <div>
                        <i class="fa fa-phone margin-right-5 text-gray" style="width: 10px;"></i>
                        <span class="text-dark"><?= $model['peserta']->handphone ?></span>
                    </div>
                </td>
                <td>
                    <div class="fs-14 text-dark"><?= $model['peserta']->periodeJenis->nama ?></div>
                    <div>
                        <span class="text-gray">Rp </span>
                        <span class="text-dark"><?= number_format($model['peserta']->harga, 2) ?></span>
                    </div>
                </td>
                <td>
                    <div>
                        <i class="fa fa-map-marker margin-right-5 text-gray"></i>
                        <span class="text-dark"><?= $model['peserta']->periodeKota->nama ?></span>
                    </div>
                    <div>
                        <?= Html::a('Lihat Detail', ['detail-kota', 'id' => $model['peserta']->id_periode_kota], ['modal-md' => '', 'modal-title' => 'Detail Lokasi']) ?>
                    </div>
                </td>
                <td>
                    <?php if ($model['peserta']->status_bayar == 'Belum Bayar') : ?>
                        Belum tersedia
                    <?php elseif ($model['peserta']->status_bayar == 'Ditolak') : ?>
                        Belum tersedia
                    <?php elseif ($model['peserta']->status_bayar == 'Dalam Proses Konfirmasi') : ?>
                        Belum tersedia
                    <?php elseif ($model['peserta']->status_bayar == 'Sudah Bayar') : ?>
                        <?= Html::a('Download', ['download-sertifikat', 'kode' => $model['peserta']->kode, 'email' => $model['peserta']->email], ['class' => 'button button-sm border-azure text-azure']) ?>
                    <?php endif; ?>
                </td>
            </tr>
            <?php foreach ($model['peserta']->pesertaTambahans as $key => $pesertaTambahan) : ?>
                <tr>
                    <td>
                        <div class="fs-14 text-dark"><?= $pesertaTambahan->nama ?></div>
                        <div>
                            <span class="text-gray">kode: </span>
                            <span class="text-dark"><?= $pesertaTambahan->kode ?></span>
                        </div>
                    </td>
                    <td>
                        <div>
                            <i class="fa fa-envelope margin-right-5 text-gray" style="width: 10px;"></i>
                            <span class="text-dark"><?= $pesertaTambahan->email ?></span>
                        </div>
                        <div>
                            <i class="fa fa-phone margin-right-5 text-gray" style="width: 10px;"></i>
                            <span class="text-dark"><?= $pesertaTambahan->handphone ?></span>
                        </div>
                    </td>
                    <td>
                        <div class="fs-14 text-dark"><?= $pesertaTambahan->periodeJenis->nama ?></div>
                        <div>
                            <span class="text-gray">Rp </span>
                            <span class="text-dark"><?= number_format($pesertaTambahan->harga, 2) ?></span>
                        </div>
                    </td>
                    <td>
                        <div>
                            <i class="fa fa-map-marker margin-right-5 text-gray"></i>
                            <span class="text-dark"><?= $pesertaTambahan->periodeKota->nama ?></span>
                        </div>
                        <div>
                            <?= Html::a('Lihat Detail', ['detail-kota', 'id' => $pesertaTambahan->id_periode_kota], ['modal-md' => '', 'modal-title' => 'Detail Lokasi']) ?>
                        </div>
                    </td>
                    <td>
                        <?php if ($model['peserta']->status_bayar == 'Belum Bayar') : ?>
                            Belum tersedia
                        <?php elseif ($model['peserta']->status_bayar == 'Ditolak') : ?>
                            Belum tersedia
                        <?php elseif ($model['peserta']->status_bayar == 'Dalam Proses Konfirmasi') : ?>
                            Belum tersedia
                        <?php elseif ($model['peserta']->status_bayar == 'Sudah Bayar') : ?>
                            <?= Html::a('Download', ['download-sertifikat', 'kode' => $pesertaTambahan->kode, 'email' => $pesertaTambahan->email], ['class' => 'button button-sm border-azure text-azure']) ?>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>

    <div class="margin-top-15"></div>

    <div class="padding-x-15">
        <?php if ($model['peserta']->status_bayar == 'Belum Bayar' || $model['peserta']->status_bayar == 'Belum Bayar') : ?>
            <span class="fs-italic"><i class="fa fa-warning margin-right-5"></i> tombol download kartu ujian akan muncul setelah Kamu melakukan pembayaran</span>
        <?php elseif ($model['peserta']->status_bayar == 'Dalam Proses Konfirmasi') : ?>
            <span class="fs-italic"><i class="fa fa-warning margin-right-5"></i> tombol download kartu ujian akan muncul setelah admin melakukan konfirmasi</span>
        <?php endif; ?>
    </div>

    </div>
</div>

<div class="margin-top-30"></div>

<?php elseif (!$newSearch): ?>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-azure text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Hasil Pencarian
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-red" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">
        
    <div class="text-red text-uppercase text-center">
        <div>
            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/error.png" width="50px;">
        </div>
        <div class="margin-top-15"></div>
        <div>
            <span class="fs-20 m-fs-18">Data Tidak Ditemukan</span>
        </div>
    </div>

    </div>
</div>

<div class="margin-top-30"></div>

<?php endif; ?>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    <?php if (!$model['peserta']->isNewRecord) : ?>
        Cek ulang nilai Kamu pada formulir dibawah ini
    <?php elseif (!$newSearch): ?>
        Harap perbaiki kesalahan sebelum melakukan pencarian kembali
    <?php else: ?>
        Cek nilai Kamu pada formulir dibawah ini
    <?php endif; ?>

    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-red" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
      
        <div class="box box-break-sm box-gutter box-equal">
            <div class="box-6">
                <?= $form->field($model['peserta'], 'kode', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'kode', ['class' => 'form-label fw-bold', 'label' =>'Kode peserta']); ?>
                     <!-- <span class="margin-y-5 fw-normal">- Kode pendaftaran yang Kamu dapatkan melalui email</span> -->
                    <?= Html::activeTextInput($model['peserta'], 'kode', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
                    <?= Html::error($model['peserta'], 'kode', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'kode')->end(); ?>
            </div>
            <div class="box-6">
                <?= $form->field($model['peserta'], 'email', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'email', ['class' => 'form-label fw-bold', 'label' =>'Email']); ?>
                     <!-- <span class="margin-y-5 fw-normal">- Alamat email yang Kamu gunakan untuk mendaftar.</span> -->
                    <?= Html::activeTextInput($model['peserta'], 'email', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
                    <?= Html::error($model['peserta'], 'email', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'email')->end(); ?>
            </div>
        </div>

        <div class="margin-top-30"></div>
        
        <div class="form-wrapper clearfix">
            <?= Html::submitButton('<i class="fa fa-qrcode margin-right-5"></i> Cek Nilai', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

    </div>
</div>

<div class="margin-top-50"></div>
