<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/peserta/form-pendaftaran.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);

// technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
// technosmart\assets_manager\AutosizeAsset::register($this);
// technosmart\assets_manager\FileInputAsset::register($this);
// technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
// technosmart\assets_manager\JqueryMaskedInputAsset::register($this);

//
$pesertaTambahans = [];
if (isset($model['peserta_tambahan']))
    foreach ($model['peserta_tambahan'] as $key => $pesertaTambahan)
        $pesertaTambahans[] = $pesertaTambahan->attributes;

$periodeJenises = [];
if (isset($model['periode_jenis']))
    foreach ($model['periode_jenis'] as $key => $periodeJenis)
        $periodeJenises[] = $periodeJenis->attributes;

$this->registerJs(
    'vm.changeHarga();' .
    'vm.$data.peserta.id_periode_jenis = ' . (int)$model['peserta']->id_periode_jenis . ';' .
    'vm.$data.peserta.harga = ' . (int)$model['peserta']->harga . ';' .
    'vm.$data.peserta.pesertaTambahans = vm.$data.peserta.pesertaTambahans.concat(' . json_encode($pesertaTambahans) . ');' .
    'vm.$data.periodeJenises = vm.$data.periodeJenises.concat(' . json_encode($periodeJenises) . ');',
    3
);

//
$errorMessage = '';
$errorVue = false;
if ($model['peserta']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['peserta'], ['class' => '']);
}

if (isset($model['peserta_tambahan'])) foreach ($model['peserta_tambahan'] as $key => $pesertaTambahan) {
    if ($pesertaTambahan->hasErrors()) {
        $errorMessage .= Html::errorSummary($pesertaTambahan, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}
?>
<style type="text/css">
.form-text:focus,
.form-textarea:focus,
.form-dropdown:focus {
  box-shadow: 0 0 10px rgba(51, 118, 184, 0.3);
}
</style>

<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Daftar Tryout online pada formulir dibawah ini
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-red" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
      
        <?php if ($errorMessage) : ?>
            <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                <?= $errorMessage ?>
            </div>
        <?php endif; ?>

        <div class="box box-break-sm box-gutter box-equal">
            <div class="box-6">
                <?= $form->field($model['peserta'], 'email', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'email', ['class' => 'form-label fw-bold', 'label' =>'Email']); ?>
                     <!-- <span class="margin-y-5 fw-normal">- Pastikan email Kamu benar dan bisa dibuka, kami akan kirimkan nomor peserta ke email tersebut. Alamat email yang sama <b>BOLEH</b> digunakan berulang kali.</span> -->
                    <?= Html::activeTextInput($model['peserta'], 'email', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
                    <?= Html::error($model['peserta'], 'email', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'email')->end(); ?>
            </div>
            <div class="box-6">
                <?= $form->field($model['peserta'], 'nama', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'nama', ['class' => 'form-label fw-bold', 'label' =>'Nama lengkap']); ?>
                    <?= Html::activeTextInput($model['peserta'], 'nama', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
                    <?= Html::error($model['peserta'], 'nama', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'nama')->end(); ?>
            </div>
            <div class="box-4">
                <?= $form->field($model['peserta'], 'jenis_kelamin', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'jenis_kelamin', ['class' => 'form-label fw-bold', 'label' =>'Jenis Kelamin']); ?>
                    <?= Html::activeDropDownList($model['peserta'], 'jenis_kelamin', $model['peserta']->getEnum('jenis_kelamin'), ['prompt' => 'Pilih jenis kelamin', 'class' => 'form-dropdown rounded-xs']); ?>
                    <?= Html::error($model['peserta'], 'jenis_kelamin', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'jenis_kelamin')->end(); ?>
            </div>
            <div class="box-4">
                <?= $form->field($model['peserta'], 'handphone', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'handphone', ['class' => 'form-label fw-bold', 'label' =>'Handphone']); ?>
                     <!-- <span class="margin-y-5 fw-normal">- No. HP peserta, akan digunakan untuk menghubungi peserta dalam keadaan mendesak.</span> -->
                    <!-- <div class="form-icon">
                        <span class="icon-prepend padding-right-0" style="line-height: 18px">0 </span>
                    </div> -->
                    <?= Html::activeTextInput($model['peserta'], 'handphone', ['class' => 'form-text rounded-xs', 'maxlength' => true, 'placeholder' => '08xxxxx']); ?>
                    <?= Html::error($model['peserta'], 'handphone', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'handphone')->end(); ?>
            </div>
            <div class="box-4">
                <?= $form->field($model['peserta'], 'sekolah', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'sekolah', ['class' => 'form-label fw-bold', 'label' =>'Asal sekolah peserta']); ?>
                    <?= Html::activeTextInput($model['peserta'], 'sekolah', ['class' => 'form-text rounded-xs', 'maxlength' => true, 'placeholder' => 'contoh: SMAN 8 Jakarta']); ?>
                    <?= Html::error($model['peserta'], 'sekolah', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'sekolah')->end(); ?>
            </div>
            <div class="box-6">
                <?= $form->field($model['peserta'], 'id_periode_jenis', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'id_periode_jenis', ['class' => 'form-label fw-bold', 'label' =>'Jenis tryout']); ?>
                    <!-- Pilih jenis tryout SBMPTN yang ingin Kamu ikuti -->
                    <?= Html::activeDropDownList($model['peserta'], 'id_periode_jenis', ArrayHelper::map(\app_tryout\models\PeriodeJenis::find()->where(['id_periode' => $idPeriode, 'status' => 'Sedang Aktif'])->indexBy('id')->asArray()->all(), 'id', 'nama'), ['prompt' => 'Pilih jenis tryout', 'class' => 'form-dropdown rounded-xs', 'v-model' => 'peserta.id_periode_jenis', 'v-on:change' => 'changeHarga']); ?>
                    <?= Html::error($model['peserta'], 'id_periode_jenis', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'id_periode_jenis')->end(); ?>
            </div>
            <div class="box-6">
                <?= $form->field($model['peserta'], 'id_periode_kota', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'id_periode_kota', ['class' => 'form-label fw-bold', 'label' =>'Lokasi tryout']); ?>
                    <?= Html::activeDropDownList($model['peserta'], 'id_periode_kota', ArrayHelper::map(\app_tryout\models\PeriodeKota::find()->where(['id_periode' => $idPeriode, 'status' => 'Sedang Aktif'])->orderBy('nama')->asArray()->all(), 'id', 'nama'), ['prompt' => 'Pilih lokasi', 'class' => 'form-dropdown rounded-xs']); ?>
                    <?= Html::error($model['peserta'], 'id_periode_kota', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'id_periode_kota')->end(); ?>
            </div>
            <div class="box-12">
                <label class="form-label fw-bold">Social media / instant messenger <span class="margin-y-5 fw-normal fs-italic">- Isi minimal 2 dari 5.</span></label>
            </div>
            <div class="box-4">
                <?= $form->field($model['peserta'], 'facebook', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="form-icon">
                        <i class="icon-prepend fa fa-facebook"></i>
                        <?= Html::activeTextInput($model['peserta'], 'facebook', ['class' => 'form-text rounded-xs', 'maxlength' => true, 'placeholder' => 'facebook']); ?>
                    </div>
                    <?= Html::error($model['peserta'], 'facebook', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'facebook')->end(); ?>
            </div>
            <div class="box-4">
                <?= $form->field($model['peserta'], 'twitter', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="form-icon">
                        <i class="icon-prepend fa fa-twitter"></i>
                        <?= Html::activeTextInput($model['peserta'], 'twitter', ['class' => 'form-text rounded-xs', 'maxlength' => true, 'placeholder' => 'twitter']); ?>
                    </div>
                    <?= Html::error($model['peserta'], 'twitter', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'twitter')->end(); ?>
            </div>
            <div class="box-4">
                <?= $form->field($model['peserta'], 'instagram', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="form-icon">
                        <i class="icon-prepend fa fa-instagram"></i>
                        <?= Html::activeTextInput($model['peserta'], 'instagram', ['class' => 'form-text rounded-xs', 'maxlength' => true, 'placeholder' => 'instagram']); ?>
                    </div>
                    <?= Html::error($model['peserta'], 'instagram', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'instagram')->end(); ?>
            </div>
            <div class="box-6">
                <?= $form->field($model['peserta'], 'line', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="form-icon">
                        <i class="icon-prepend fa fa-commenting-o"></i>
                        <?= Html::activeTextInput($model['peserta'], 'line', ['class' => 'form-text rounded-xs', 'maxlength' => true, 'placeholder' => 'line']); ?>
                    </div>
                    <?= Html::error($model['peserta'], 'line', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'line')->end(); ?>
            </div>
            <div class="box-6">
                <?= $form->field($model['peserta'], 'whatsapp', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="form-icon">
                        <i class="icon-prepend fa fa-whatsapp"></i>
                        <?= Html::activeTextInput($model['peserta'], 'whatsapp', ['class' => 'form-text rounded-xs', 'maxlength' => true, 'placeholder' => 'whatsapp']); ?>
                    </div>
                    <?= Html::error($model['peserta'], 'whatsapp', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'whatsapp')->end(); ?>
            </div>
            <div class="box-12">
                <?= $form->field($model['peserta'], 'id_sumber', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'id_sumber', ['class' => 'form-label fw-bold', 'label' =>'Dari sumber mana Kamu mengetahui tryout ini ?']); ?>
                    <?= Html::activeDropDownList($model['peserta'], 'id_sumber', ArrayHelper::map(\app_tryout\models\Sumber::find()->indexBy('id')->asArray()->all(), 'id', 'nama'), ['prompt' => 'Pilih sumber', 'class' => 'form-dropdown rounded-xs']); ?>
                    <?= Html::error($model['peserta'], 'id_sumber', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'id_sumber')->end(); ?>
            </div>
            <div class="box-12">
                <?= $form->field($model['peserta'], 'alamat', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'alamat', ['class' => 'form-label fw-bold', 'label' =>'Alamat tinggal']); ?>
                    <?= Html::activeTextArea($model['peserta'], 'alamat', ['class' => 'form-textarea rounded-xs', 'maxlength' => true]); ?>
                    <?= Html::error($model['peserta'], 'alamat', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'alamat')->end(); ?>
            </div>
        </div>

        <?php if (isset($model['peserta_tambahan'])) foreach ($model['peserta_tambahan'] as $key => $value): ?>
            <?php
                $this->registerJs(
                    "vm.changeHargaTambahan($key);vm.changeHarga();",
                    3
                );
            ?>

            <?= $form->field($model['peserta_tambahan'][$key], "[$key]nama", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta_tambahan'][$key], "[$key]nama", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta_tambahan'][$key], "[$key]email", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta_tambahan'][$key], "[$key]email", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta_tambahan'][$key], "[$key]handphone", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta_tambahan'][$key], "[$key]handphone", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta_tambahan'][$key], "[$key]id_periode_jenis", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta_tambahan'][$key], "[$key]id_periode_jenis", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta_tambahan'][$key], "[$key]id_periode_kota", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta_tambahan'][$key], "[$key]id_periode_kota", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta_tambahan'][$key], "[$key]facebook", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta_tambahan'][$key], "[$key]facebook", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta_tambahan'][$key], "[$key]twitter", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta_tambahan'][$key], "[$key]twitter", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta_tambahan'][$key], "[$key]instagram", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta_tambahan'][$key], "[$key]instagram", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta_tambahan'][$key], "[$key]line", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta_tambahan'][$key], "[$key]line", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta_tambahan'][$key], "[$key]whatsapp", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta_tambahan'][$key], "[$key]whatsapp", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>
        <?php endforeach; ?>

        <template v-if="typeof peserta.pesertaTambahans == 'object'">
            <template v-for="(value, key, index) in peserta.pesertaTambahans">
                <div v-show="!(value.id < 0)">
                    <hr class="border-light-azure border-top margin-top-50">

                    <div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-azure text-center">
                      <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
                      Tiket Ke-{{key+2}}
                      <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
                    </div>
                    <div class="margin-top-15"></div>

                    <input type="hidden" v-bind:id="'pesertatambahan-' + key + '-id'" v-bind:name="'PesertaTambahan[' + key + '][id]'" type="text" v-model="peserta.pesertaTambahans[key].id">

                    <div class="box box-break-sm box-gutter box-equal">
                        <div class="box-6">
                            <div v-bind:class="'form-wrapper field-pesertatambahan-' + key + '-email'">
                                <label v-bind:for="'pesertatambahan-' + key + '-email'" class="form-label fw-bold">Email</label>
                                <input v-bind:id="'pesertatambahan-' + key + '-email'" v-bind:name="'PesertaTambahan[' + key + '][email]'" class="form-text rounded-xs" type="text" v-model="peserta.pesertaTambahans[key].email">
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-6">
                            <div v-bind:class="'form-wrapper field-pesertatambahan-' + key + '-nama'">
                                <label v-bind:for="'pesertatambahan-' + key + '-nama'" class="form-label fw-bold">Nama lengkap</label>
                                <input v-bind:id="'pesertatambahan-' + key + '-nama'" v-bind:name="'PesertaTambahan[' + key + '][nama]'" class="form-text rounded-xs" type="text" v-model="peserta.pesertaTambahans[key].nama">
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-pesertatambahan-' + key + '-handphone'">
                                <label v-bind:for="'pesertatambahan-' + key + '-handphone'" class="form-label fw-bold">Handphone</label>
                                <!-- <div class="form-icon">
                                    <span class="icon-prepend padding-right-0" style="line-height: 18px">0 </span>
                                </div> -->
                                <input v-bind:id="'pesertatambahan-' + key + '-handphone'" v-bind:name="'PesertaTambahan[' + key + '][handphone]'" class="form-text rounded-xs" type="text" v-model="peserta.pesertaTambahans[key].handphone" placeholder="08xxxxx">
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-6">
                            <div v-bind:class="'form-wrapper field-pesertatambahan-' + key + '-id_periode_jenis'">
                                <label v-bind:for="'pesertatambahan-' + key + '-id_periode_jenis'" class="form-label fw-bold">Jenis tryout</label>
                                <select v-bind:id="'pesertatambahan-' + key + '-id_periode_jenis'" v-bind:name="'PesertaTambahan[' + key + '][id_periode_jenis]'" class="form-dropdown rounded-xs" v-model="peserta.pesertaTambahans[key].id_periode_jenis" v-on:change="changeHargaTambahan(key); changeHarga();">
                                    <option value="">Pilih jenis tryout</option>
                                    <?php foreach (ArrayHelper::map(\app_tryout\models\PeriodeJenis::find()->where(['id_periode' => $idPeriode, 'status' => 'Sedang Aktif'])->indexBy('id')->asArray()->all(), 'id', 'nama') as $id => $nama) : ?>
                                        <option value="<?= $id ?>"><?= $nama ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-6">
                            <div v-bind:class="'form-wrapper field-pesertatambahan-' + key + '-id_periode_kota'">
                                <label v-bind:for="'pesertatambahan-' + key + '-id_periode_kota'" class="form-label fw-bold">Lokasi tryout</label>
                                <select v-bind:id="'pesertatambahan-' + key + '-id_periode_kota'" v-bind:name="'PesertaTambahan[' + key + '][id_periode_kota]'" class="form-dropdown rounded-xs" v-model="peserta.pesertaTambahans[key].id_periode_kota">
                                    <option value="">Pilih lokasi</option>
                                    <?php foreach (ArrayHelper::map(\app_tryout\models\PeriodeKota::find()->where(['id_periode' => $idPeriode, 'status' => 'Sedang Aktif'])->indexBy('id')->asArray()->all(), 'id', 'nama') as $id => $nama) : ?>
                                        <option value="<?= $id ?>"><?= $nama ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-12">
                            <label class="form-label fw-bold">Social media / instant messenger <span class="margin-y-5 fw-normal fs-italic">- Isi minimal 2 dari 5.</span></label>
                        </div>
                        <div class="box-4">
                            <div v-bind:class="'form-wrapper field-pesertatambahan-' + key + '-facebook'">
                                <div class="form-icon">
                                    <i class="icon-prepend fa fa-facebook"></i>
                                    <input v-bind:id="'pesertatambahan-' + key + '-facebook'" v-bind:name="'PesertaTambahan[' + key + '][facebook]'" class="form-text rounded-xs" type="text" v-model="peserta.pesertaTambahans[key].facebook" placeholder="facebook">
                                </div>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-4">
                            <div v-bind:class="'form-wrapper field-pesertatambahan-' + key + '-twitter'">
                                <div class="form-icon">
                                    <i class="icon-prepend fa fa-twitter"></i>
                                    <input v-bind:id="'pesertatambahan-' + key + '-twitter'" v-bind:name="'PesertaTambahan[' + key + '][twitter]'" class="form-text rounded-xs" type="text" v-model="peserta.pesertaTambahans[key].twitter" placeholder="twitter">
                                </div>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-4">
                            <div v-bind:class="'form-wrapper field-pesertatambahan-' + key + '-instagram'">
                                <div class="form-icon">
                                    <i class="icon-prepend fa fa-instagram"></i>
                                    <input v-bind:id="'pesertatambahan-' + key + '-instagram'" v-bind:name="'PesertaTambahan[' + key + '][instagram]'" class="form-text rounded-xs" type="text" v-model="peserta.pesertaTambahans[key].instagram" placeholder="instagram">
                                </div>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-6">
                            <div v-bind:class="'form-wrapper field-pesertatambahan-' + key + '-line'">
                                <div class="form-icon">
                                    <i class="icon-prepend fa fa-commenting-o"></i>
                                    <input v-bind:id="'pesertatambahan-' + key + '-line'" v-bind:name="'PesertaTambahan[' + key + '][line]'" class="form-text rounded-xs" type="text" v-model="peserta.pesertaTambahans[key].line" placeholder="line">
                                </div>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-6">
                            <div v-bind:class="'form-wrapper field-pesertatambahan-' + key + '-whatsapp'">
                                <div class="form-icon">
                                    <i class="icon-prepend fa fa-whatsapp"></i>
                                    <input v-bind:id="'pesertatambahan-' + key + '-whatsapp'" v-bind:name="'PesertaTambahan[' + key + '][whatsapp]'" class="form-text rounded-xs" type="text" v-model="peserta.pesertaTambahans[key].whatsapp" placeholder="whatsapp">
                                </div>
                                <div class="form-info"></div>
                            </div>
                        </div>
                    </div>

                    <div class="margin-top-15"></div>
                    
                    <div class="text-center">
                        <a v-on:click="removePesertaTambahan(key)" class="button button-sm border-light-red bg-light-red">Hapus Tiket Ke-{{key+2}}</a>
                    </div>
                </div>
            </template>
        </template>

        <hr class="border-light-azure border-top margin-top-50">

        <h6>Ringkasan</h6>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-3 padding-x-0 m-text-left text-gray">Total tiket :</div>
            <div class="box-9 m-padding-x-0 text-dark">{{totalTiket}}</div>
        </div>
        <div class="box box-break-sm margin-bottom-10">
            <div class="box-3 padding-x-0 m-text-left text-gray">Harga tiket ke-1 :</div>
            <div class="box-9 m-padding-x-0 text-dark">
                <b>Rp {{peserta.harga.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}}</b>
                <span class="text-grayest margin-left-15">hemat Rp {{(peserta.harga_asli - peserta.harga).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}}</span>
            </div>
        </div>
        <template v-if="typeof peserta.pesertaTambahans == 'object'">
            <template v-for="(value, key, index) in peserta.pesertaTambahans">
                <div v-show="!(value.id < 0)">
                    <div class="box box-break-sm margin-bottom-10">
                        <div class="box-3 padding-x-0 m-text-left text-gray">Harga tiket ke-{{key+2}} :</div>
                        <div class="box-9 m-padding-x-0 text-dark">
                            <b>Rp {{value.harga.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}}</b>
                            <span class="text-grayest margin-left-15">hemat Rp {{(value.harga_asli - value.harga).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}}</span>
                        </div>
                    </div>
                </div>
            </template>
        </template>
        <div class="box box-break-sm margin-bottom-10">
            <div class="box-3 padding-x-0 m-text-left text-gray">Total tagihan :</div>
            <div class="box-9 m-padding-x-0 text-spring">
                <b>Rp {{tagihan.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}}</b>
                <span class="text-grayest margin-left-15">hemat <u>Rp {{(tagihanAsli - tagihan).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}}</u></span>
            </div>
        </div>

        <div class="border-light-azure bg-light-azure padding-15 text-center" v-if="peserta.pesertaTambahans.length <= 8">
            * Dapatkan diskon dengan membeli lebih banyak tiket ! <a v-on:click="addPesertaTambahan" class="button border-azure bg-azure margin-left-15">Tambah Tiket</a>
        </div>

        <div class="border-light-azure bg-light-azure padding-15 text-center" v-else>
            * Maksimum pembelian tiket tercapai !
            <br>
            Kamu telah melakukan penghematan maksimum :)
        </div>

        <div class="margin-top-30"></div>
        
        <div class="form-wrapper clearfix">
            <?= Html::submitButton('<i class="fa fa-rocket margin-right-5"></i> Daftar', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

    </div>
</div>

<div class="margin-top-50"></div>