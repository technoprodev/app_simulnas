<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

/*$this->registerJsFile('@web/app/dev/form.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);*/

// technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
// technosmart\assets_manager\AutosizeAsset::register($this);
technosmart\assets_manager\FileInputAsset::register($this);
technosmart\assets_manager\FlatpickrAsset::register($this);
// technosmart\assets_manager\JqueryMaskedInputAsset::register($this);

//
/*$devChildren = [];
if (isset($model['dev_child']))
    foreach ($model['dev_child'] as $key => $devChild)
        $devChildren[] = $devChild->attributes;

$this->registerJs(
    'vm.$data.dev.virtual_category = ' . json_encode($model['peserta']->virtual_category) . ';' .
    'vm.$data.dev.devChildren = vm.$data.dev.devChildren.concat(' . json_encode($devChildren) . ');',
    // 'vm.$data.dev.devChildren = Object.assign({}, vm.$data.dev.devChildren, ' . json_encode($devChildren) . ');',
    3
);*/

//
$errorMessage = '';
$errorVue = false;
if ($model['peserta']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['peserta'], ['class' => '']);
}

/*if (isset($model['dev_child'])) foreach ($model['dev_child'] as $key => $devChild) {
    if ($devChild->hasErrors()) {
        $errorMessage .= Html::errorSummary($devChild, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}*/
?>
<style type="text/css">
.form-text:focus,
.form-textarea:focus,
.form-dropdown:focus {
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
}
</style>

<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Konfirmasi pembayaran Kamu pada formulir dibawah ini
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-red" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>

        <div class="box box-break-sm box-gutter box-equal">
            <div class="box-6">
                <?= $form->field($model['peserta'], 'kode', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'kode', ['class' => 'form-label fw-bold', 'label' =>'Kode peserta']); ?>
                     <!-- <span class="margin-y-5 fw-normal">- Kode pendaftaran yang Kamu dapatkan melalui email</span> -->
                    <?= Html::activeTextInput($model['peserta'], 'kode', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
                    <?= Html::error($model['peserta'], 'kode', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'kode')->end(); ?>
            </div>
            <div class="box-6">
                <?= $form->field($model['peserta'], 'email', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'email', ['class' => 'form-label fw-bold', 'label' =>'Email']); ?>
                     <!-- <span class="margin-y-5 fw-normal">- Alamat email yang Kamu gunakan untuk mendaftar.</span> -->
                    <?= Html::activeTextInput($model['peserta'], 'email', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
                    <?= Html::error($model['peserta'], 'email', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'email')->end(); ?>
            </div>
            <div class="box-6">
                <?= $form->field($model['peserta'], 'id_periode_metode_pembayaran', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'id_periode_metode_pembayaran', ['class' => 'form-label fw-bold', 'label' =>'Metode pembayaran']); ?>
                    <!-- Pilih metode pembayaran yang Kamu gunakan -->
                    <?= Html::activeDropDownList($model['peserta'], 'id_periode_metode_pembayaran', ArrayHelper::map(\app_tryout\models\PeriodeMetodePembayaran::find()->where(['id_periode' => $idPeriode])->indexBy('id')->asArray()->all(), 'id', 'nama'), ['prompt' => 'Pilih metode pembayaran', 'class' => 'form-dropdown rounded-xs']); ?>
                    <?= Html::error($model['peserta'], 'id_periode_metode_pembayaran', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'id_periode_metode_pembayaran')->end(); ?>
            </div>
            <div class="box-6">
                <?= $form->field($model['peserta'], 'tanggal_pembayaran', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'tanggal_pembayaran', ['class' => 'form-label fw-bold', 'label' =>'Tanggal pembayaran']); ?>
                    <?= Html::activeTextInput($model['peserta'], 'tanggal_pembayaran', ['class' => 'input-flatpickr form-text rounded-xs', 'maxlength' => true]); ?>
                    <?= Html::error($model['peserta'], 'tanggal_pembayaran', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'tanggal_pembayaran')->end(); ?>
            </div>
            <div class="box-6">
                <?= $form->field($model['peserta'], 'pembayaran_atas_nama', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'pembayaran_atas_nama', ['class' => 'form-label fw-bold', 'label' =>'Nama Pengirim Rekening']); ?>
                    <!-- Atas Nama <span class="margin-y-5 fw-normal">- Nama pemilik rekening / penyetor yang Kamu gunakan untuk transfer pembayaran.</span> -->
                    <?= Html::activeTextInput($model['peserta'], 'pembayaran_atas_nama', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
                    <?= Html::error($model['peserta'], 'pembayaran_atas_nama', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'pembayaran_atas_nama')->end(); ?>
            </div>
            <div class="box-6">
                <?= $form->field($model['peserta'], 'virtual_bukti_pembayaran_upload', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'virtual_bukti_pembayaran_upload', ['class' => 'form-label fw-bold', 'label' =>'Upload Bukti Pembayaran']); ?>
                     <!-- <span class="margin-y-5 fw-normal">- Foto / screenshot bukti transfer bank. Ukuran maksimal <b>1 MB</b>.</span> -->
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <a href="#" class="input-group-addon btn btn-default square fileinput-exists" data-dismiss="fileinput"><i class="fa fa-close"></i></a>
                        <div class="form-text rounded-xs">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                            <span class="fileinput-filename"><a href="<?= $model['peserta']->virtual_bukti_pembayaran_download ?>"><?= $model['peserta']->bukti_pembayaran ?></a></span>
                        </div>
                        <span class="input-group-addon btn btn-default square btn-file">
                            <span class="fileinput-new">Select file</span>
                            <span class="fileinput-exists">Change</span>
                            <?= Html::activeFileInput($model['peserta'], 'virtual_bukti_pembayaran_upload'); ?>
                        </span>
                    </div>
                    <?= Html::error($model['peserta'], 'virtual_bukti_pembayaran_upload', ['class' => 'form-info fs-14']); ?>
                <?= $form->field($model['peserta'], 'virtual_bukti_pembayaran_upload')->end(); ?>
            </div>
            <div class="box-12">
                <?= $form->field($model['peserta'], 'informasi_pembayaran', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'informasi_pembayaran', ['class' => 'form-label fw-bold', 'label' =>'Informasi tambahan']); ?>
                     <!-- <span class="margin-y-5 fw-normal">- Jika diperlukan, isi informasi tambahan agar admin dapat melakukan verifikasi dengan benar. -->
                    <?= Html::activeTextArea($model['peserta'], 'informasi_pembayaran', ['class' => 'form-textarea rounded-xs', 'maxlength' => true]); ?>
                    <?= Html::error($model['peserta'], 'informasi_pembayaran', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'informasi_pembayaran')->end(); ?>
            </div>
        </div>

        <div class="margin-top-30"></div>
        
        <div class="form-wrapper clearfix">
            <?= Html::submitButton('<i class="fa fa-tasks margin-right-5"></i> Konfirmasi', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

    </div>
</div>

<div class="margin-top-50"></div>