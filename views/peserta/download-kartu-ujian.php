<!-- <div class="margin-y-30 padding-30 shadow" style="max-width: 900px; width: 100%; margin-left: auto; margin-right: auto;"> -->

<style type="text/css">@page { margin: 30px; }</style>

<div class="has-bg-img">
	<div class="bg-img" style="background-image: url('<?= $this->render('img-kartu-ujian-background'); ?>')"></div>

	<div class="text-center text-middle">
		<img alt="<?= Yii::$app->params['app.name'] ?> logo" class="text-middle" src="<?= $this->render('img-kartu-ujian'); ?>" style="width: 100%; max-width: 300px; height: auto;">
	</div>

	<hr class="margin-0">

	<h1 class="text-center text-uppercase text-red">Kartu Ujian</h1>

	<table class="table" style="max-width: 500px; width: 100%; margin-left: auto; margin-right: auto;">
		<tbody>
			<tr>
				<td class="padding-y-5 ">Nama :</td>
				<td class="padding-y-5 fw-bold"><?= $model['peserta']->nama ?></td>
			</tr>
			<tr>
				<td class="padding-y-5 ">Email :</td>
				<td class="padding-y-5 fw-bold"><?= $model['peserta']->email ?></td>
			</tr>
			<tr>
				<td class="padding-y-5 ">Nomor Hp :</td>
				<td class="padding-y-5 fw-bold"><?= $model['peserta']->handphone ?></td>
			</tr>
			<tr>
				<td class="padding-y-5 ">Jenis :</td>
				<td class="padding-y-5 fw-bold text-red"><?= $model['peserta']->periodeJenis->nama ?></td>
			</tr>
			<tr>
				<td class="padding-y-5 ">Periode Tiket :</td>
				<td class="padding-y-5 fw-bold"><?= $model['peserta']->periode_penjualan ?></td>
			</tr>
		</tbody>
	</table>

	<table class="table" style="max-width: 500px; width: 100%; margin-left: auto; margin-right: auto; margin-top: 0; margin-bottom: 0;">
		<tbody>
			<tr>
				<td class="text-center border-lighter border-thin" style="width: 33%;">
					<div class="text-gray">Kode Peserta</div>
					<div class="text-azure fs-18 fw-bold"><?= $model['peserta']->kode ?></div>
				</td>
				<td class="text-center border-lighter border-thin" style="width: 33%;">
					<div class="text-gray">Lokasi</div>
					<div class="text-azure fs-18 fw-bold"><?= $model['peserta']->periodeKota->nama ?></div>
					<div class="margin-top-5"></div>
					<div class="text-azure fs-18 fw-bold"><?= $model['peserta']->periodeKota->alamat ?></div>
				</td>
				<td class="text-center border-lighter border-thin" style="width: 33%;">
					<div class="text-gray">Waktu</div>
					<div class="text-azure fs-18 fw-bold">Oktober 2018</div>
					<div class="margin-top-5"></div>
					<div class="text-azure fs-18 fw-bold">27</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<!-- </div> -->
