<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-top-15">
    <div class="box-6">
<?php endif; ?>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">Nama Tempat</div>
    <div class="box-10 m-padding-x-0 text-dark">
        <div class="padding-y-5"><?= $model['periode_kota']->nama ?></div>
    </div>
</div>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">Alamat Lengkap</div>
    <div class="box-10 m-padding-x-0 text-dark">
        <div class="padding-y-5"><?= $model['periode_kota']->alamat ?></div>
    </div>
</div>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>