<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\ChartAsset::register($this);

$this->registerJsFile('@web/app/xswzaq/list-index-periode-kota.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/xswzaq/list-index-periode-jenis.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Halaman setting sistem
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">

    <div class="box box-space-md box-gutter box-break-sm box-equal">
        <div class="box-12 padding-20 border-light-azure">
            <div class="fs-15 text-dark m-text-center clearfix">
                <span class="border-azure text-azure padding-y-5 padding-x-15 rounded-lg text-middle">Kota</span>
                <a href="<?= Yii::$app->urlManager->createUrl('xswzaq/create-periode-kota') ?>" class="button border-azure bg-azure hover-bg-lightest hover-text-azure pull-right m-pull-none m-button-block m-margin-top-30">Tambah Kota</a>
            </div>
            <div class="margin-top-30 m-margin-top-15"></div>
            <table class="datatables-periode-kota table table-nowrap">
                <thead>
                    <tr class="text-dark">
                        <th>Kab/Kota</th>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>Status</th>
                        <th>Alamat</th>
                        <th>Kuota</th>
                        <th></th>
                    </tr>
                    <tr class="dt-search">
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search kota..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search kode..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search nama..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search status..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search alamat..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search kuota..."/></th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <div class="margin-top-20"></div>

    <div class="box box-space-md box-gutter box-break-sm box-equal">
        <div class="box-12 padding-20 border-light-azure">
            <div class="fs-15 text-dark m-text-center clearfix">
                <span class="border-azure text-azure padding-y-5 padding-x-15 rounded-lg text-middle">Jenis, Periode & Harga</span>
                <a href="<?= Yii::$app->urlManager->createUrl('xswzaq/create-periode-jenis') ?>" class="button border-azure bg-azure hover-bg-lightest hover-text-azure pull-right m-pull-none m-button-block m-margin-top-30">Tambah Baru</a>
            </div>
            <div class="margin-top-30 m-margin-top-15"></div>
            <table class="datatables-periode-jenis table table-nowrap">
                <thead>
                    <tr class="text-dark">
                        <th>Jenis</th>
                        <th>Periode</th>
                        <th>Status</th>
                        <th>Harga 1</th>
                        <th>Harga 2</th>
                        <th>Harga 3</th>
                        <th>Harga 4</th>
                        <th>Harga 5</th>
                        <th>Harga 6</th>
                        <th>Harga 7</th>
                        <th>Harga 8</th>
                        <th>Harga 9</th>
                        <th>Harga 10</th>
                        <th></th>
                    </tr>
                    <tr class="dt-search">
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search jenis..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search periode..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search status..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search harga 1..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search harga 2..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search harga 3..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search harga 4..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search harga 5..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search harga 6..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search harga 7..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search harga 8..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search harga 9..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search harga 10..."/></th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>