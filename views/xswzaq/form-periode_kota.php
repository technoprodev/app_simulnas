<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/xswzaq/form-periode_kota.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);

//
$provincesOriginal = [];
$provincesOriginal = array_map('ucwords', array_map('strtolower', ArrayHelper::map(\technosmart\modules\location\models\Provinces::find()->orderBy('name')->asArray()->all(), 'id', 'name')));

$provinces = [];
foreach ($provincesOriginal as $key => $value) {
    $provinces[] = [
        'value' => $key,
        'text' => $value,
    ];
}
// ddx($provinces);
$this->registerJs(
    'vm.$data.periode_kota.id_provinces = ' . json_encode($model['periode_kota']->regencies ? $model['periode_kota']->regencies->province_id : '') . ';' .
    'vm.onProvinceChange();' .
    'vm.$data.periode_kota.id_regencies = ' . json_encode($model['periode_kota']->id_regencies ? $model['periode_kota']->id_regencies : '') . ';' .
    'vm.$data.provinces = ' . json_encode($provinces) . ';',
    3
);

//
$errorMessage = '';
if ($model['periode_kota']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['periode_kota'], ['class' => '']);
}
?>

<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Kelola data kota pada formulir dibawah ini
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-red" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
      
        <?php if ($errorMessage) : ?>
            <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                <?= $errorMessage ?>
            </div>
        <?php endif; ?>

        <?= $form->field($model['periode_kota'], 'id_regencies', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['periode_kota'], 'id_regencies', ['class' => 'form-label fw-bold', 'label' =>'Pilih Kota']); ?>
            
            <select id="periode_kota-id_provinces" name="PeriodeKota[id_provinces]" class="form-dropdown rounded-xs" aria-invalid="false" v-model="periode_kota.id_provinces" v-on:change="onProvinceChange">
                <option value="">Pilih provinsi</option>
                <option v-for="option in provinces" v-bind:value="option.value">
                    {{ option.text }}
                </option>
            </select>

            <div class="margin-top-15"></div>

            <select id="periode_kota-id_regencies" name="PeriodeKota[id_regencies]" class="form-dropdown rounded-xs" aria-invalid="false" v-model="periode_kota.id_regencies">
                <option value="">Pilih kota</option>
                <option v-for="option in regencies" v-bind:value="option.value">
                    {{ option.text }}
                </option>
            </select>

            <?= Html::error($model['periode_kota'], 'id_regencies', ['class' => 'form-info']); ?>

        <?= $form->field($model['periode_kota'], 'id_regencies')->end(); ?>

        <?= $form->field($model['periode_kota'], 'kode', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['periode_kota'], 'kode', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['periode_kota'], 'kode', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['periode_kota'], 'kode', ['class' => 'form-info']); ?>
        <?= $form->field($model['periode_kota'], 'kode')->end(); ?>

        <?= $form->field($model['periode_kota'], 'nama', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['periode_kota'], 'nama', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['periode_kota'], 'nama', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['periode_kota'], 'nama', ['class' => 'form-info']); ?>
        <?= $form->field($model['periode_kota'], 'nama')->end(); ?>

        <?= $form->field($model['periode_kota'], 'status', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['periode_kota'], 'status', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeRadioList($model['periode_kota'], 'status', $model['periode_kota']->getEnum('status'), ['class' => 'form-radio', 'unselect' => null,
                'item' => function($index, $label, $name, $checked, $value){
                    $checked = $checked ? 'checked' : '';
                    $disabled = in_array($value, []) ? 'disabled' : '';
                    return "<label><input type='radio' name='$name' value='$value' $checked $disabled><i></i>$label</label>";
                }]); ?>
            <?= Html::error($model['periode_kota'], 'status', ['class' => 'form-info']); ?>
        <?= $form->field($model['periode_kota'], 'status')->end(); ?>

        <?= $form->field($model['periode_kota'], 'alamat', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['periode_kota'], 'alamat', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextArea($model['periode_kota'], 'alamat', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['periode_kota'], 'alamat', ['class' => 'form-info']); ?>
        <?= $form->field($model['periode_kota'], 'alamat')->end(); ?>

        <?= $form->field($model['periode_kota'], 'kuota', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['periode_kota'], 'kuota', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['periode_kota'], 'kuota', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['periode_kota'], 'kuota', ['class' => 'form-info']); ?>
        <?= $form->field($model['periode_kota'], 'kuota')->end(); ?>

        <div class="margin-top-30"></div>
        
        <div class="form-wrapper clearfix">
            <?= Html::submitButton('<i class="fa fa-rocket margin-right-5"></i> Submit', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

    </div>
</div>

<div class="margin-top-50"></div>