<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Detail Kota
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-red" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">
<?php endif; ?>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Kab/Kota</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['periode_kota']->id_regencies ? ucwords(strtolower($model['periode_kota']->regencies->name)) : '(kosong)' ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Kode</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['periode_kota']->kode ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Nama</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['periode_kota']->nama ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Status</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['periode_kota']->status ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Alamat</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['periode_kota']->alamat ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Kuota</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['periode_kota']->kuota ?></div>
        </div>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>

<div class="margin-top-50"></div>
<?php endif; ?>