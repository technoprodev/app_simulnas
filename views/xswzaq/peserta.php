<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\ChartPieLabelAsset::register($this);

$this->registerJsFile('@web/app/xswzaq/list-index-statistik.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/xswzaq/list-index-peserta.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);

$jumlahKota = (new \yii\db\Query())->select(['count(*)'])->from('periode_kota pk')->where('pk.id_periode = 1')->scalar();
?>

<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Kelola data peserta pada halaman ini
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="box box-space-md box-gutter box-break-sm">
        <div class="box-4 padding-20 border-light-azure hover-scroll-y" style="height: 500px;">
            <div class="fs-15 text-red m-text-center">
                <span class="border-red text-red inline-block padding-y-5 padding-x-15 rounded-lg text-middle">Segera Konfirmasi !</span>
            </div>
            <div class="margin-top-30 m-margin-top-15"></div>

            <?php foreach ($model['peserta'] as $key => $peserta) : ?>
            
            <div class="box box-gutter">
                <div class="box-2">
                    <span class="circle-icon fs-11 bg-<?= $peserta['color'] ?> border-transparent" style="width: 30px; height: 30px; line-height: 28px;"><?= $peserta['alias'] ?></span>
                </div>
                <div class="box-10">
                    <div class="fs-14 text-dark"><?= $peserta['nama'] ?></div>
                    <div class="margin-top-2"></div>
                    <div class="">
                        <span class="text-spring">Rp <?= number_format($peserta['tagihan'], 2) ?></span>
                        <span class="margin-x-5"></span>
                        <span class="text-gray"><i class="fa fa-calendar margin-right-5"></i><?= DateTime::createFromFormat('Y-m-d', $peserta['tanggal_pembayaran'])->format("d M y") ?></span>
                    </div>
                </div>
            </div>
            <div class="margin-top-10"></div>
            <div class="">
                <a class="button button-xs border-light-azure text-azure button-block" href="<?= Yii::$app->urlManager->createUrl(['xswzaq/konfirmasi', 'id' => $peserta['id']]) ?>" modal-lg="" modal-title="Konfirmasi Pembayaran <?= $peserta['kode'] ?>">Konfirmasi</a>
            </div>

            <hr class="border-lighter border-top">

            <?php endforeach; ?>

            <div class="margin-top-15"></div>

            <?php if (count($model['peserta'])) : ?>

            <div class="text-center">
                <button class="button padding-y-5 padding-x-20 rounded-lg border-azure bg-azure hover-bg-transparent hover-text-azure">Total <?= count($model['peserta']) ?> Transaksi</button>
            </div>
            <div class="margin-top-5"></div>
            <div class="text-center text-gray">
                * hanya ditampilkan maks 10 transaksi konfirmasi
            </div>

            <?php else : ?>

            <div class="text-center border-light-azure padding-15">
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/check.png" width="90px;">
                <div class="margin-top-5"></div>   
                <div>Semua transaksi sudah dikonfirmasi.</div>
            </div>

            <?php endif; ?>

            <div class="margin-top-30"></div>
        </div>
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Total Pendaftar</div>
            <div class="text-azure">
                <span class="fs-30 text-middle fw-bold"><?= $model['all']['total_tiket'] ?></span>
            </div>
        </div>
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Belum Bayar</div>
            <div class="text-red">
                <span class="fs-30 text-middle fw-bold"><?= $model['all']['tiket_belum_bayar'] ?></span>
            </div>
        </div>
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Konfirmasi</div>
            <div class="text-orange">
                <span class="fs-30 text-middle fw-bold"><?= $model['all']['tiket_dalam_proses_konfirmasi'] ?></span>
            </div>
        </div>
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Pendaftaran Selesai</div>
            <div class="text-green">
                <span class="fs-30 text-middle fw-bold"><?= $model['all']['tiket_sudah_bayar'] ?></span>
            </div>
        </div>
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Total Saintek</div>
            <div class="text-magenta">
                <span class="fs-30 text-middle fw-bold"><?= $model['all']['tiket_saintek_ipa'] ?></span>
            </div>
        </div>
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Total Soshum</div>
            <div class="text-blue">
                <span class="fs-30 text-middle fw-bold"><?= $model['all']['tiket_soshum_ips'] ?></span>
            </div>
        </div>
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Pendaftaran Ditolak</div>
            <div class="text-red">
                <span class="fs-30 text-middle fw-bold"><?= $model['all']['tiket_ditolak'] ?></span>
            </div>
        </div>
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Rata-rata per Kota</div>
            <div class="text-rose">
                <span class="fs-30 text-middle fw-bold"><?= round($model['all']['tiket_sudah_bayar'] / $jumlahKota, 2) ?></span>
            </div>
        </div>
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Tiket Gratis</div>
            <div class="text-chartreuse">
                <span class="fs-30 text-middle fw-bold"><?= $model['all']['total_tiket_gratis'] ?></span>
            </div>
        </div>
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Jumlah Ambass</div>
            <div class="text-cyan">
                <span class="fs-30 text-middle fw-bold"><?= $model['all']['duta_transaksi'] ?></span>
            </div>
        </div>
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Tiket Hasil Ambass</div>
            <div class="text-spring">
                <span class="fs-30 text-middle fw-bold"><?= $model['all']['duta_tiket'] ?></span>
            </div>
        </div>
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Total Volunteer</div>
            <div class="text-yellow">
                <span class="fs-30 text-middle fw-bold"><?= 0 ?></span>
            </div>
        </div>
    </div>

    <div class="margin-top-20"></div>

    <div class="box box-space-md box-gutter box-break-sm">
        <div class="box-12 padding-20 border-light-azure">
            <div class="fs-15 text-dark m-text-center clearfix">
                <span class="border-azure text-azure inline-block padding-y-5 padding-x-15 rounded-lg text-middle">Peserta</span>
                <a href="<?= Yii::$app->urlManager->createUrl('xswzaq/tiket-gratis') ?>" class="button border-azure bg-azure hover-bg-lightest hover-text-azure pull-right m-pull-none m-button-block m-margin-top-30">Tiket Gratis</a>
            </div>
            <div class="margin-top-30 m-margin-top-15"></div>
            <table class="datatables-peserta table table-nowrap">
                <thead>
                    <tr class="text-dark">
                        <th></th>
                        <th>Nama</th>
                        <th>Kode</th>
                        <th>Kontak</th>
                        <th>Jumlah Tiket</th>
                        <th>Tagihan</th>
                        <th>Lokasi</th>
                        <th>Request</th>
                        <th>Status</th>
                        <th>Duta</th>
                    </tr>
                    <tr class="dt-search">
                        <th></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search nama..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search kode..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search kontak..."/></th>
                        <th><select class="form-dropdown border-none padding-0">
                            <option value="">all tiket</option>
                            <option value="1">1 tiket</option>
                            <option value="2">2 tiket</option>
                            <option value="3">3 tiket</option>
                            <option value="4">4 tiket</option>
                            <option value="5">5 tiket</option>
                            <option value="6">6 tiket</option>
                            <option value="7">7 tiket</option>
                            <option value="8">8 tiket</option>
                            <option value="9">9 tiket</option>
                            <option value="10">10 tiket</option>
                        </select></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search tagihan..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search lokasi..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search requests..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search status..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search duta..."/></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<div class="margin-top-50"></div>