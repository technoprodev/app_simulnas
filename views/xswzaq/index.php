<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\ChartPieLabelAsset::register($this);

$this->registerJsFile('@web/app/xswzaq/list-index-statistik.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/xswzaq/list-index-statistik-sudah-bayar.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/xswzaq/list-index-peserta.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);

$jumlahKota = (new \yii\db\Query())->select(['count(*)'])->from('periode_kota pk')->where('pk.id_periode = 1')->scalar();
?>
<script type="text/javascript">
    tiketSudahBayar = <?= $model['all']['tiket_sudah_bayar'] ?>;
</script>

<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Dashboard Utama
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="box box-space-md box-gutter box-break-sm">
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Total Pendaftar</div>
            <div class="text-azure">
                <span class="fs-30 text-middle fw-bold"><?= $model['all']['total_tiket'] ?></span>
            </div>
        </div>
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Belum Bayar</div>
            <div class="text-red">
                <span class="fs-30 text-middle fw-bold"><?= $model['all']['tiket_belum_bayar'] ?></span>
            </div>
        </div>
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Konfirmasi</div>
            <div class="text-orange">
                <span class="fs-30 text-middle fw-bold"><?= $model['all']['tiket_dalam_proses_konfirmasi'] ?></span>
            </div>
        </div>
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Pendaftaran Selesai</div>
            <div class="text-green">
                <span class="fs-30 text-middle fw-bold"><?= $model['all']['tiket_sudah_bayar'] ?></span>
            </div>
        </div>
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Total Saintek</div>
            <div class="text-magenta">
                <span class="fs-30 text-middle fw-bold"><?= $model['all']['tiket_saintek_ipa'] ?></span>
            </div>
        </div>
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Total Soshum</div>
            <div class="text-blue">
                <span class="fs-30 text-middle fw-bold"><?= $model['all']['tiket_soshum_ips'] ?></span>
            </div>
        </div>
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Pendaftaran Ditolak</div>
            <div class="text-red">
                <span class="fs-30 text-middle fw-bold"><?= $model['all']['tiket_ditolak'] ?></span>
            </div>
        </div>
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Rata-rata per Kota</div>
            <div class="text-rose">
                <span class="fs-30 text-middle fw-bold"><?= round($model['all']['tiket_sudah_bayar'] / $jumlahKota, 2) ?></span>
            </div>
        </div>
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Tiket Gratis</div>
            <div class="text-chartreuse">
                <span class="fs-30 text-middle fw-bold"><?= $model['all']['total_tiket_gratis'] ?></span>
            </div>
        </div>
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Jumlah Ambass</div>
            <div class="text-cyan">
                <span class="fs-30 text-middle fw-bold"><?= $model['all']['duta_transaksi'] ?></span>
            </div>
        </div>
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Tiket Hasil Ambass</div>
            <div class="text-spring">
                <span class="fs-30 text-middle fw-bold"><?= $model['all']['duta_tiket'] ?></span>
            </div>
        </div>
        <div class="box-2 padding-15 border-light-azure text-center" style="height: 82px;">
            <div class="fw-bold">Total Volunteer</div>
            <div class="text-yellow">
                <span class="fs-30 text-middle fw-bold"><?= 0 ?></span>
            </div>
        </div>
    </div>

    <div class="margin-top-20"></div>

    <div class="box box-space-md box-gutter box-break-sm box-equal">
        <div class="box-12 padding-20 border-light-azure hover-scroll-y" style="height: 500px;">
            <div class="fs-15 text-dark m-text-center">
                <span class="border-azure text-azure inline-block padding-y-5 padding-x-15 rounded-lg text-middle">Anak Konfirmasi</span>
            </div>
            <div class="margin-top-30 m-margin-top-15"></div>
            <div>
                <canvas id="anakKonfirmasi" height="400"></canvas>
                <script>
                    window.addEventListener('load', function() {
                        var ctx = document.getElementById('anakKonfirmasi').getContext('2d');
                        var config = {
                            type: 'line',
                            data: {
                              labels: <?= json_encode($model['label_anak_konfirmasi']) ?>,
                              datasets: [{
                                data: <?= json_encode($model['data_anak_konfirmasi']) ?>,
                                borderColor: '#3376b8',
                                borderWidth: 1,
                                fill: false,
                                label: '',
                              }]
                            },
                            options: {
                                maintainAspectRatio: false,
                                elements: {
                                    line: {
                                        tension: 0, // disables bezier curves
                                    }
                                },
                                animation: {
                                    duration: 0, // general animation time
                                },
                                hover: {
                                    animationDuration: 0, // duration of animations when hovering an item
                                },
                                responsiveAnimationDuration: 0, // animation duration after a resize
                                responsive: true,
                                tooltips: {
                                    mode: 'index',
                                    intersect: false,
                                },
                                scales: {
                                    xAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Tanggal'
                                        }
                                    }],
                                    yAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Total Tiket'
                                        },
                                    }]
                                },
                                legend: {
                                    display: false
                                },
                            }
                        };
                        window.anakKonfirmasi = new Chart(ctx, config);
                    });
                </script>
            </div>
        </div>
        <div class="box-12 padding-20 border-light-azure hover-scroll-y" style="height: 500px;">
            <div class="fs-15 text-dark m-text-center">
                <span class="border-azure text-azure inline-block padding-y-5 padding-x-15 rounded-lg text-middle">Anak Dikonfirmasi</span>
            </div>
            <div class="margin-top-30 m-margin-top-15"></div>
            <div>
                <canvas id="anakDikonfirmasi" height="400"></canvas>
                <script>
                    window.addEventListener('load', function() {
                        var ctx = document.getElementById('anakDikonfirmasi').getContext('2d');
                        var config = {
                            type: 'line',
                            data: {
                              labels: <?= json_encode($model['label_anak_dikonfirmasi']) ?>,
                              datasets: [{
                                data: <?= json_encode($model['data_anak_dikonfirmasi']) ?>,
                                borderColor: '#3376b8',
                                borderWidth: 1,
                                fill: false,
                                label: '',
                              }]
                            },
                            options: {
                                maintainAspectRatio: false,
                                elements: {
                                    line: {
                                        tension: 0, // disables bezier curves
                                    }
                                },
                                animation: {
                                    duration: 0, // general animation time
                                },
                                hover: {
                                    animationDuration: 0, // duration of animations when hovering an item
                                },
                                responsiveAnimationDuration: 0, // animation duration after a resize
                                responsive: true,
                                tooltips: {
                                    mode: 'index',
                                    intersect: false,
                                },
                                scales: {
                                    xAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Tanggal'
                                        }
                                    }],
                                    yAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Total Tiket'
                                        },
                                    }]
                                },
                                legend: {
                                    display: false
                                },
                            }
                        };
                        window.anakDikonfirmasi = new Chart(ctx, config);
                    });
                </script>
            </div>
        </div>
        <div class="box-12 padding-20 border-light-azure hover-scroll-y" style="height: 500px;">
            <div class="fs-15 text-dark m-text-center">
                <span class="border-azure text-azure inline-block padding-y-5 padding-x-15 rounded-lg text-middle">Peserta Daftar</span>
            </div>
            <div class="margin-top-30 m-margin-top-15"></div>
            <div>
                <canvas id="pesertaDaftar" height="400"></canvas>
                <script>
                    window.addEventListener('load', function() {
                        var ctx = document.getElementById('pesertaDaftar').getContext('2d');
                        var config = {
                            type: 'line',
                            data: {
                              labels: <?= json_encode($model['label_peserta_daftar']) ?>,
                              datasets: [{
                                data: <?= json_encode($model['data_peserta_daftar']) ?>,
                                borderColor: '#3376b8',
                                borderWidth: 1,
                                fill: false,
                                label: '',
                              }]
                            },
                            options: {
                                maintainAspectRatio: false,
                                elements: {
                                    line: {
                                        tension: 0, // disables bezier curves
                                    }
                                },
                                animation: {
                                    duration: 0, // general animation time
                                },
                                hover: {
                                    animationDuration: 0, // duration of animations when hovering an item
                                },
                                responsiveAnimationDuration: 0, // animation duration after a resize
                                responsive: true,
                                tooltips: {
                                    mode: 'index',
                                    intersect: false,
                                },
                                scales: {
                                    xAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Tanggal'
                                        }
                                    }],
                                    yAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Total Tiket'
                                        },
                                    }]
                                },
                                legend: {
                                    display: false
                                },
                            }
                        };
                        window.pesertaDaftar = new Chart(ctx, config);
                    });
                </script>
            </div>
        </div>
    </div>

    <div class="margin-top-20"></div>

    <div class="box box-space-md box-gutter box-break-sm">
        <div class="box-12 padding-20 border-light-azure">
            <div class="fs-15 text-dark m-text-center clearfix">
                <span class="border-azure text-azure inline-block padding-y-5 padding-x-15 rounded-lg text-middle">Laporan Statistik Sudah Bayar</span>
            </div>
            <div class="margin-top-30 m-margin-top-15"></div>
            <table class="datatables-statistik-sudah-bayar table table-nowrap table-striped table-bordered table-hover">
                <thead>
                    <tr class="text-dark">
                        <th rowspan="1" class="border-bottom border-red bg-light-red" style="font-weight: bold; text-align: center;">Kota</th>
                        <th colspan="5" class="border-bottom border-azure bg-light-azure" style="font-weight: bold; text-align: center;">Sudah Bayar</th>
                    </tr>
                    <tr class="text-dark">
                        <th></th>
                        <th style="text-align: center;">saintek</th>
                        <th style="text-align: center;">soshum</th>
                        <th style="text-align: center;">jumlah</th>
                        <th style="text-align: center;">persentase</th>
                    </tr>
                    <tr>
                        <th class="bg-red padding-2" style="text-align: center;">Sum All</th>
                        <th class="bg-azure padding-2" style="text-align: center;"><?= $model['all']['tiket_saintek_ipa'] ?></th>
                        <th class="bg-azure padding-2" style="text-align: center;"><?= $model['all']['tiket_soshum_ips'] ?></th>
                        <th class="bg-azure padding-2" style="text-align: center;"><?= $model['all']['tiket_sudah_bayar'] ?></th>
                        <th class="bg-azure padding-2" style="text-align: center;">100%</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <div class="margin-top-20"></div>

    <div class="box box-space-md box-gutter box-break-sm">
        <div class="box-12 padding-20 border-light-azure">
            <div class="fs-15 text-dark m-text-center clearfix">
                <span class="border-azure text-azure inline-block padding-y-5 padding-x-15 rounded-lg text-middle">Laporan Statistik Lengkap</span>
            </div>
            <div class="margin-top-30 m-margin-top-15"></div>
            <table class="datatables-statistik table table-nowrap table-striped table-bordered table-hover">
                <thead>
                    <tr class="text-dark">
                        <th rowspan="1" class="border-bottom border-red bg-light-red" style="font-weight: bold; text-align: center;">Kota</th>
                        <th colspan="5" class="border-bottom border-azure bg-light-azure" style="font-weight: bold; text-align: center;">Transaksi</th>
                        <th colspan="5" class="border-bottom border-chartreuse bg-light-chartreuse" style="font-weight: bold; text-align: center;">Tiket</th>
                        <th colspan="2" class="border-bottom border-rose bg-light-rose" style="font-weight: bold; text-align: center;">Tkt Sdh Byr</th>
                        <th colspan="2" class="border-bottom border-orange bg-light-orange" style="font-weight: bold; text-align: center;">Duta</th>
                    </tr>
                    <tr class="text-dark">
                        <th></th>
                        <th style="text-align: center;">blm byr</th>
                        <th style="text-align: center;">konf</th>
                        <th style="text-align: center;">sdh byr</th>
                        <th style="text-align: center;">tlk</th>
                        <th style="text-align: center;">tot</th>
                        <th style="text-align: center;">blm byr</th>
                        <th style="text-align: center;">konf</th>
                        <th style="text-align: center;">sdh byr</th>
                        <th style="text-align: center;">tlk</th>
                        <th style="text-align: center;">tot</th>
                        <th style="text-align: center;">ipa</th>
                        <th style="text-align: center;">ips</th>
                        <th style="text-align: center;">jml</th>
                        <th style="text-align: center;">tkt</th>
                    </tr>
                    <tr>
                        <th class="bg-red padding-2" style="text-align: center;">Sum All</th>
                        <th class="bg-azure padding-2" style="text-align: center;"><?= $model['all']['transaksi_belum_bayar'] ?></th>
                        <th class="bg-azure padding-2" style="text-align: center;"><?= $model['all']['transaksi_dalam_proses_konfirmasi'] ?></th>
                        <th class="bg-azure padding-2" style="text-align: center;"><?= $model['all']['transaksi_sudah_bayar'] ?></th>
                        <th class="bg-azure padding-2" style="text-align: center;"><?= $model['all']['transaksi_ditolak'] ?></th>
                        <th class="bg-azure padding-2" style="text-align: center;"><?= $model['all']['total_transaksi'] ?></th>
                        <th class="bg-chartreuse padding-2" style="text-align: center;"><?= $model['all']['tiket_belum_bayar'] ?></th>
                        <th class="bg-chartreuse padding-2" style="text-align: center;"><?= $model['all']['tiket_dalam_proses_konfirmasi'] ?></th>
                        <th class="bg-chartreuse padding-2" style="text-align: center;"><?= $model['all']['tiket_sudah_bayar'] ?></th>
                        <th class="bg-chartreuse padding-2" style="text-align: center;"><?= $model['all']['tiket_ditolak'] ?></th>
                        <th class="bg-chartreuse padding-2" style="text-align: center;"><?= $model['all']['total_tiket'] ?></th>
                        <th class="bg-rose padding-2" style="text-align: center;"><?= $model['all']['tiket_saintek_ipa'] ?></th>
                        <th class="bg-rose padding-2" style="text-align: center;"><?= $model['all']['tiket_soshum_ips'] ?></th>
                        <th class="bg-orange padding-2" style="text-align: center;"><?= $model['all']['duta_transaksi'] ?></th>
                        <th class="bg-orange padding-2" style="text-align: center;"><?= $model['all']['duta_tiket'] ?></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <div class="margin-top-20"></div>

    <div class="box box-space-md box-gutter box-break-sm box-equal">
        <div class="box-6 padding-20 border-light-azure hover-scroll-y" style="height: 500px;">
            <div class="fs-15 text-dark m-text-center">
                <span class="border-azure text-azure inline-block padding-y-5 padding-x-15 rounded-lg text-middle">Tiket Belum Bayar Per Kota</span>
            </div>
            <div class="margin-top-30 m-margin-top-15"></div>
            <div>
                <canvas id="tiketBelumBayarPerKota" height="400"></canvas>
                <script>
                    window.addEventListener('load', function() {
                        var ctx = document.getElementById('tiketBelumBayarPerKota').getContext('2d');
                        var config = {
                            type: 'pie',
                            data: {
                              labels: <?= json_encode($model['label_tiket_belum_bayar_per_kota']) ?>,
                              datasets: [{
                                data: <?= json_encode($model['data_tiket_belum_bayar_per_kota']) ?>,
                                backgroundColor: <?= json_encode($model['background_tiket_belum_bayar_per_kota']) ?>,
                                borderColor: <?= json_encode($model['border_tiket_belum_bayar_per_kota']) ?>,
                                borderWidth: 1,
                                label: 'asdf',
                              }]
                            },
                            options: {
                                maintainAspectRatio: false,
                                elements: {
                                    line: {
                                        tension: 0, // disables bezier curves
                                    }
                                },
                                animation: {
                                    duration: 0, // general animation time
                                },
                                hover: {
                                    animationDuration: 0, // duration of animations when hovering an item
                                },
                                responsiveAnimationDuration: 0, // animation duration after a resize
                                responsive: true,
                                legend: {
                                    display: false
                                },
                                plugins: {
                                    labels: {
                                        // render 'label', 'value', 'percentage', 'image' or custom function, default is 'percentage'
                                        render: function (args) {
                                            return args.label + ' ' + args.percentage + '%';
                                        },
                                        fontSize: 12,
                                        fontColor: <?= json_encode($model['border_tiket_belum_bayar_per_kota']) ?>,
                                    },
                                },
                            }
                        };
                        window.tiketBelumBayarPerKota = new Chart(ctx, config);
                    });
                </script>
            </div>
        </div>
        <div class="box-6 padding-20 border-light-azure hover-scroll-y" style="height: 500px;">
            <div class="fs-15 text-dark m-text-center">
                <span class="border-azure text-azure inline-block padding-y-5 padding-x-15 rounded-lg text-middle">Tiket Sudah Bayar Per Kota</span>
            </div>
            <div class="margin-top-30 m-margin-top-15"></div>
            <div>
                <canvas id="tiketSudahBayarPerKota" height="400"></canvas>
                <script>
                    window.addEventListener('load', function() {
                        var ctx = document.getElementById('tiketSudahBayarPerKota').getContext('2d');
                        var config = {
                            type: 'pie',
                            data: {
                              labels: <?= json_encode($model['label_tiket_sudah_bayar_per_kota']) ?>,
                              datasets: [{
                                data: <?= json_encode($model['data_tiket_sudah_bayar_per_kota']) ?>,
                                backgroundColor: <?= json_encode($model['background_tiket_sudah_bayar_per_kota']) ?>,
                                borderColor: <?= json_encode($model['border_tiket_sudah_bayar_per_kota']) ?>,
                                borderWidth: 1,
                                label: 'asdf',
                              }]
                            },
                            options: {
                                maintainAspectRatio: false,
                                elements: {
                                    line: {
                                        tension: 0, // disables bezier curves
                                    }
                                },
                                animation: {
                                    duration: 0, // general animation time
                                },
                                hover: {
                                    animationDuration: 0, // duration of animations when hovering an item
                                },
                                responsiveAnimationDuration: 0, // animation duration after a resize
                                responsive: true,
                                legend: {
                                    display: false
                                },
                                plugins: {
                                    labels: {
                                        // render 'label', 'value', 'percentage', 'image' or custom function, default is 'percentage'
                                        render: function (args) {
                                            return args.label + ' ' + args.percentage + '%';
                                        },
                                        fontSize: 12,
                                        fontColor: <?= json_encode($model['border_tiket_sudah_bayar_per_kota']) ?>,
                                    },
                                },
                            }
                        };
                        window.tiketSudahBayarPerKota = new Chart(ctx, config);
                    });
                </script>
            </div>
        </div>
    </div>
</div>

<div class="margin-top-50"></div>