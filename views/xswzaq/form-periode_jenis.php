<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

//
$errorMessage = '';
if ($model['periode_jenis']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['periode_jenis'], ['class' => '']);
}
?>

<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Kelola data jenis, periode & harga pada formulir dibawah ini
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-red" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
      
        <?php if ($errorMessage) : ?>
            <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                <?= $errorMessage ?>
            </div>
        <?php endif; ?>

        <?= $form->field($model['periode_jenis'], 'nama', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['periode_jenis'], 'nama', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['periode_jenis'], 'nama', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['periode_jenis'], 'nama', ['class' => 'form-info']); ?>
        <?= $form->field($model['periode_jenis'], 'nama')->end(); ?>

        <?= $form->field($model['periode_jenis'], 'periode_penjualan', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['periode_jenis'], 'periode_penjualan', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['periode_jenis'], 'periode_penjualan', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['periode_jenis'], 'periode_penjualan', ['class' => 'form-info']); ?>
        <?= $form->field($model['periode_jenis'], 'periode_penjualan')->end(); ?>

        <?= $form->field($model['periode_jenis'], 'status', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['periode_jenis'], 'status', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeRadioList($model['periode_jenis'], 'status', $model['periode_jenis']->getEnum('status'), ['class' => 'form-radio', 'unselect' => null,
                'item' => function($index, $label, $name, $checked, $value){
                    $checked = $checked ? 'checked' : '';
                    $disabled = in_array($value, []) ? 'disabled' : '';
                    return "<label><input type='radio' name='$name' value='$value' $checked $disabled><i></i>$label</label>";
                }]); ?>
            <?= Html::error($model['periode_jenis'], 'status', ['class' => 'form-info']); ?>
        <?= $form->field($model['periode_jenis'], 'status')->end(); ?>

        <?= $form->field($model['periode_jenis'], 'harga_1_tiket', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['periode_jenis'], 'harga_1_tiket', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['periode_jenis'], 'harga_1_tiket', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['periode_jenis'], 'harga_1_tiket', ['class' => 'form-info']); ?>
        <?= $form->field($model['periode_jenis'], 'harga_1_tiket')->end(); ?>

        <?= $form->field($model['periode_jenis'], 'harga_2_tiket', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['periode_jenis'], 'harga_2_tiket', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['periode_jenis'], 'harga_2_tiket', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['periode_jenis'], 'harga_2_tiket', ['class' => 'form-info']); ?>
        <?= $form->field($model['periode_jenis'], 'harga_2_tiket')->end(); ?>

        <?= $form->field($model['periode_jenis'], 'harga_3_tiket', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['periode_jenis'], 'harga_3_tiket', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['periode_jenis'], 'harga_3_tiket', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['periode_jenis'], 'harga_3_tiket', ['class' => 'form-info']); ?>
        <?= $form->field($model['periode_jenis'], 'harga_3_tiket')->end(); ?>

        <?= $form->field($model['periode_jenis'], 'harga_4_tiket', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['periode_jenis'], 'harga_4_tiket', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['periode_jenis'], 'harga_4_tiket', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['periode_jenis'], 'harga_4_tiket', ['class' => 'form-info']); ?>
        <?= $form->field($model['periode_jenis'], 'harga_4_tiket')->end(); ?>

        <?= $form->field($model['periode_jenis'], 'harga_5_tiket', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['periode_jenis'], 'harga_5_tiket', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['periode_jenis'], 'harga_5_tiket', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['periode_jenis'], 'harga_5_tiket', ['class' => 'form-info']); ?>
        <?= $form->field($model['periode_jenis'], 'harga_5_tiket')->end(); ?>

        <?= $form->field($model['periode_jenis'], 'harga_6_tiket', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['periode_jenis'], 'harga_6_tiket', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['periode_jenis'], 'harga_6_tiket', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['periode_jenis'], 'harga_6_tiket', ['class' => 'form-info']); ?>
        <?= $form->field($model['periode_jenis'], 'harga_6_tiket')->end(); ?>

        <?= $form->field($model['periode_jenis'], 'harga_7_tiket', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['periode_jenis'], 'harga_7_tiket', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['periode_jenis'], 'harga_7_tiket', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['periode_jenis'], 'harga_7_tiket', ['class' => 'form-info']); ?>
        <?= $form->field($model['periode_jenis'], 'harga_7_tiket')->end(); ?>

        <?= $form->field($model['periode_jenis'], 'harga_8_tiket', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['periode_jenis'], 'harga_8_tiket', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['periode_jenis'], 'harga_8_tiket', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['periode_jenis'], 'harga_8_tiket', ['class' => 'form-info']); ?>
        <?= $form->field($model['periode_jenis'], 'harga_8_tiket')->end(); ?>

        <?= $form->field($model['periode_jenis'], 'harga_9_tiket', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['periode_jenis'], 'harga_9_tiket', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['periode_jenis'], 'harga_9_tiket', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['periode_jenis'], 'harga_9_tiket', ['class' => 'form-info']); ?>
        <?= $form->field($model['periode_jenis'], 'harga_9_tiket')->end(); ?>

        <?= $form->field($model['periode_jenis'], 'harga_10_tiket', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['periode_jenis'], 'harga_10_tiket', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['periode_jenis'], 'harga_10_tiket', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['periode_jenis'], 'harga_10_tiket', ['class' => 'form-info']); ?>
        <?= $form->field($model['periode_jenis'], 'harga_10_tiket')->end(); ?>

        <div class="margin-top-30"></div>
        
        <div class="form-wrapper clearfix">
            <?= Html::submitButton('<i class="fa fa-rocket margin-right-5"></i> Submit', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

    </div>
</div>

<div class="margin-top-50"></div>