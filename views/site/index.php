<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Home';
$this->params['breadcrumbs'][] = $this->title;
?>

<style type="text/css">
    .big-triangle-1 {
        position: relative;
        height: 100px;
        overflow: hidden;
    }
    .big-triangle-1:after,
    .big-triangle-1:before {
        content: '';
        position: absolute;
        bottom: 0;
        width: 50%;
        z-index: 2;
        border-bottom: 100px solid transparent;
    }
    .big-triangle-1:before {
        right: 50%;
        border-right: 800px solid #fafafa;;
    }
    .big-triangle-1:after {
        left: 50%;
        border-left: 800px solid #fafafa;;
    }
    .big-triangle-2 {
        position: relative;
        height: 100px;
        overflow: hidden;
    }
    .big-triangle-2:after,
    .big-triangle-2:before {
        content: '';
        position: absolute;
        bottom: 0;
        width: 50%;
        z-index: 2;
        border-bottom: 100px solid #fff;
    }
    .big-triangle-2:before {
        right: 50%;
        border-right: 800px solid transparent;
    }
    .big-triangle-2:after {
        left: 50%;
        border-left: 800px solid transparent;
    }
</style>

<div class="bg-lighter">
    <div class="container padding-y-15 text-center">
        <!-- <div>
            <div class="inline-block fs-20 m-fs-15 padding-5 padding-x-15 darker-50 text-lightest">Start a new project</div>
        </div>
        <div class="margin-top-5"></div> -->
        <!-- <div>
            <div class="inline-block fs-20 m-fs-15 padding-5 padding-x-15 darker-50 text-lightest">Mau Tryout Online ?</div>
        </div> -->
        <div class="margin-top-50"></div>
        <div>
            <img alt="Simulasi Nasional logo" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo-full.png" style="width:100%;max-width:700px;height:auto;">
        </div>
        <div class="margin-top-50"></div>
        <div class="box box-gutter box-break-sm box-break-lg-6 box-equal text-center">
            <div class="box-2 padding-15 border-transparent">
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/icon2/serentak.png" style="height:auto;width:100%">
            </div>
            <div class="box-2 padding-15 border-transparent">
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/icon2/kualitas soal.png" style="height:auto;width:100%">
            </div>
            <div class="box-2 padding-15 border-transparent">
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/icon2/hadiah juara.png" style="height:auto;width:100%">
            </div>
            <div class="box-2 padding-15 border-transparent">
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/icon2/skala nasional.png" style="height:auto;width:100%">
            </div>
            <div class="box-2 padding-15 border-transparent">
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/icon2/peserta.png" style="height:auto;width:100%">
            </div>
            <div class="box-2 padding-15 border-transparent">
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/icon2/info update.png" style="height:auto;width:100%">
            </div>
        </div>
        <div class="margin-top-30"></div>
        <!-- <div>
            <div class="inline-block fs-26 m-fs-21 padding-5 padding-x-15 fw-light border-thin border-azure bg-lightest text-azure">MasukPTN - Terbesar dan Serentak</div>
        </div> -->
        <div class="margin-top-50"></div>
        <div>
            <a href="<?= Yii::$app->urlManager->createUrl('peserta/cek-status') ?>" class="button fs-30 m-fs-25 padding-10 padding-x-30 fw-light border-thin border-lightest bg-red text-lightest rounded-sm">Cek Nilai</a>
        </div>
        <div class="margin-top-50"></div>
    </div>
</div>

<div class="text-center has-bg-img margin-top-min-15">
    <div class="bg-img" style="background-image: url('<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/triangle.jpg');"></div>
    <div class="bg-img red-50"></div>
    <div class="big-triangle-1"></div>
    <div class="margin-top-30"></div>
    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/map.png" style="width:100%;max-width:900px;height:auto;padding:0 15px">
    <div class="big-triangle-2 margin-top-min-15"></div>
</div>

<div class="padding-y-30">
    <h1 class="text-red text-center">List Kota</h1>
    <div class="text-center">
        <hr class="border-red border-top border-thin margin-0 inline-block text-middle" style="width: 40px;">
    </div>
    <div class="container">
        <div class="box box-space-lg box-gutter box-break-sm box-equal">
            <div class="box-4 padding-10 shadow-bottom-right bg-red hover-bg-lightest hover-text-red hover-border-red rounded-xs has-fancy-corner">
                <div class="corner-light top-right"></div>
                <div class="corner-dark bottom-left"></div>
                <div class="clearfix text-left">
                    <div class="pull-left circle-icon border-none bg-light-red margin-right-15 fs-18"><i class="fa fa-map-marker"></i></div>
                    <div class="pull-left">
                        <div class="fs-24 fw-bold padding-y-5">
                            <div>Bandung</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-4 padding-10 shadow-bottom-right bg-red hover-bg-lightest hover-text-red hover-border-red rounded-xs has-fancy-corner">
                <div class="corner-light top-right"></div>
                <div class="corner-dark bottom-left"></div>
                <div class="clearfix text-left">
                    <div class="pull-left circle-icon border-none bg-light-red margin-right-15 fs-18"><i class="fa fa-map-marker"></i></div>
                    <div class="pull-left">
                        <div class="fs-24 fw-bold padding-y-5">
                            <div>Bekasi</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-4 padding-10 shadow-bottom-right bg-red hover-bg-lightest hover-text-red hover-border-red rounded-xs has-fancy-corner">
                <div class="corner-light top-right"></div>
                <div class="corner-dark bottom-left"></div>
                <div class="clearfix text-left">
                    <div class="pull-left circle-icon border-none bg-light-red margin-right-15 fs-18"><i class="fa fa-map-marker"></i></div>
                    <div class="pull-left">
                        <div class="fs-24 fw-bold padding-y-5">
                            <div>Bogor</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-4 padding-10 shadow-bottom-right bg-red hover-bg-lightest hover-text-red hover-border-red rounded-xs has-fancy-corner">
                <div class="corner-light top-right"></div>
                <div class="corner-dark bottom-left"></div>
                <div class="clearfix text-left">
                    <div class="pull-left circle-icon border-none bg-light-red margin-right-15 fs-18"><i class="fa fa-map-marker"></i></div>
                    <div class="pull-left">
                        <div class="fs-24 fw-bold padding-y-5">
                            <div>Brebes</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-4 padding-10 shadow-bottom-right bg-red hover-bg-lightest hover-text-red hover-border-red rounded-xs has-fancy-corner">
                <div class="corner-light top-right"></div>
                <div class="corner-dark bottom-left"></div>
                <div class="clearfix text-left">
                    <div class="pull-left circle-icon border-none bg-light-red margin-right-15 fs-18"><i class="fa fa-map-marker"></i></div>
                    <div class="pull-left">
                        <div class="fs-24 fw-bold padding-y-5">
                            <div>Cirebon</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-4 padding-10 shadow-bottom-right bg-red hover-bg-lightest hover-text-red hover-border-red rounded-xs has-fancy-corner">
                <div class="corner-light top-right"></div>
                <div class="corner-dark bottom-left"></div>
                <div class="clearfix text-left">
                    <div class="pull-left circle-icon border-none bg-light-red margin-right-15 fs-18"><i class="fa fa-map-marker"></i></div>
                    <div class="pull-left">
                        <div class="fs-24 fw-bold padding-y-5">
                            <div>Denpasar</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-4 padding-10 shadow-bottom-right bg-red hover-bg-lightest hover-text-red hover-border-red rounded-xs has-fancy-corner">
                <div class="corner-light top-right"></div>
                <div class="corner-dark bottom-left"></div>
                <div class="clearfix text-left">
                    <div class="pull-left circle-icon border-none bg-light-red margin-right-15 fs-18"><i class="fa fa-map-marker"></i></div>
                    <div class="pull-left">
                        <div class="fs-24 fw-bold padding-y-5">
                            <div>Jakarta</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-4 padding-10 shadow-bottom-right bg-red hover-bg-lightest hover-text-red hover-border-red rounded-xs has-fancy-corner">
                <div class="corner-light top-right"></div>
                <div class="corner-dark bottom-left"></div>
                <div class="clearfix text-left">
                    <div class="pull-left circle-icon border-none bg-light-red margin-right-15 fs-18"><i class="fa fa-map-marker"></i></div>
                    <div class="pull-left">
                        <div class="fs-24 fw-bold padding-y-5">
                            <div>Kediri</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-4 padding-10 shadow-bottom-right bg-red hover-bg-lightest hover-text-red hover-border-red rounded-xs has-fancy-corner">
                <div class="corner-light top-right"></div>
                <div class="corner-dark bottom-left"></div>
                <div class="clearfix text-left">
                    <div class="pull-left circle-icon border-none bg-light-red margin-right-15 fs-18"><i class="fa fa-map-marker"></i></div>
                    <div class="pull-left">
                        <div class="fs-24 fw-bold padding-y-5">
                            <div>Lampung</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-4 padding-10 shadow-bottom-right bg-red hover-bg-lightest hover-text-red hover-border-red rounded-xs has-fancy-corner">
                <div class="corner-light top-right"></div>
                <div class="corner-dark bottom-left"></div>
                <div class="clearfix text-left">
                    <div class="pull-left circle-icon border-none bg-light-red margin-right-15 fs-18"><i class="fa fa-map-marker"></i></div>
                    <div class="pull-left">
                        <div class="fs-24 fw-bold padding-y-5">
                            <div>Malang</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-4 padding-10 shadow-bottom-right bg-red hover-bg-lightest hover-text-red hover-border-red rounded-xs has-fancy-corner">
                <div class="corner-light top-right"></div>
                <div class="corner-dark bottom-left"></div>
                <div class="clearfix text-left">
                    <div class="pull-left circle-icon border-none bg-light-red margin-right-15 fs-18"><i class="fa fa-map-marker"></i></div>
                    <div class="pull-left">
                        <div class="fs-24 fw-bold padding-y-5">
                            <div>Medan</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-4 padding-10 shadow-bottom-right bg-red hover-bg-lightest hover-text-red hover-border-red rounded-xs has-fancy-corner">
                <div class="corner-light top-right"></div>
                <div class="corner-dark bottom-left"></div>
                <div class="clearfix text-left">
                    <div class="pull-left circle-icon border-none bg-light-red margin-right-15 fs-18"><i class="fa fa-map-marker"></i></div>
                    <div class="pull-left">
                        <div class="fs-24 fw-bold padding-y-5">
                            <div>Padang</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-4 padding-10 shadow-bottom-right bg-red hover-bg-lightest hover-text-red hover-border-red rounded-xs has-fancy-corner">
                <div class="corner-light top-right"></div>
                <div class="corner-dark bottom-left"></div>
                <div class="clearfix text-left">
                    <div class="pull-left circle-icon border-none bg-light-red margin-right-15 fs-18"><i class="fa fa-map-marker"></i></div>
                    <div class="pull-left">
                        <div class="fs-24 fw-bold padding-y-5">
                            <div>Palembang</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-4 padding-10 shadow-bottom-right bg-red hover-bg-lightest hover-text-red hover-border-red rounded-xs has-fancy-corner">
                <div class="corner-light top-right"></div>
                <div class="corner-dark bottom-left"></div>
                <div class="clearfix text-left">
                    <div class="pull-left circle-icon border-none bg-light-red margin-right-15 fs-18"><i class="fa fa-map-marker"></i></div>
                    <div class="pull-left">
                        <div class="fs-24 fw-bold padding-y-5">
                            <div>Pontianak</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-4 padding-10 shadow-bottom-right bg-red hover-bg-lightest hover-text-red hover-border-red rounded-xs has-fancy-corner">
                <div class="corner-light top-right"></div>
                <div class="corner-dark bottom-left"></div>
                <div class="clearfix text-left">
                    <div class="pull-left circle-icon border-none bg-light-red margin-right-15 fs-18"><i class="fa fa-map-marker"></i></div>
                    <div class="pull-left">
                        <div class="fs-24 fw-bold padding-y-5">
                            <div>Semarang</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-4 padding-10 shadow-bottom-right bg-red hover-bg-lightest hover-text-red hover-border-red rounded-xs has-fancy-corner">
                <div class="corner-light top-right"></div>
                <div class="corner-dark bottom-left"></div>
                <div class="clearfix text-left">
                    <div class="pull-left circle-icon border-none bg-light-red margin-right-15 fs-18"><i class="fa fa-map-marker"></i></div>
                    <div class="pull-left">
                        <div class="fs-24 fw-bold padding-y-5">
                            <div>Surakarta</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-4 padding-10 shadow-bottom-right bg-red hover-bg-lightest hover-text-red hover-border-red rounded-xs has-fancy-corner">
                <div class="corner-light top-right"></div>
                <div class="corner-dark bottom-left"></div>
                <div class="clearfix text-left">
                    <div class="pull-left circle-icon border-none bg-light-red margin-right-15 fs-18"><i class="fa fa-map-marker"></i></div>
                    <div class="pull-left">
                        <div class="fs-24 fw-bold padding-y-5">
                            <div>Surabaya</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-4 padding-10 shadow-bottom-right bg-red hover-bg-lightest hover-text-red hover-border-red rounded-xs has-fancy-corner">
                <div class="corner-light top-right"></div>
                <div class="corner-dark bottom-left"></div>
                <div class="clearfix text-left">
                    <div class="pull-left circle-icon border-none bg-light-red margin-right-15 fs-18"><i class="fa fa-map-marker"></i></div>
                    <div class="pull-left">
                        <div class="fs-24 fw-bold padding-y-5">
                            <div>Tangerang</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-4 padding-10 shadow-bottom-right bg-red hover-bg-lightest hover-text-red hover-border-red rounded-xs has-fancy-corner">
                <div class="corner-light top-right"></div>
                <div class="corner-dark bottom-left"></div>
                <div class="clearfix text-left">
                    <div class="pull-left circle-icon border-none bg-light-red margin-right-15 fs-18"><i class="fa fa-map-marker"></i></div>
                    <div class="pull-left">
                        <div class="fs-24 fw-bold padding-y-5">
                            <div>Tang-Sel</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-4 padding-10 shadow-bottom-right bg-red hover-bg-lightest hover-text-red hover-border-red rounded-xs has-fancy-corner">
                <div class="corner-light top-right"></div>
                <div class="corner-dark bottom-left"></div>
                <div class="clearfix text-left">
                    <div class="pull-left circle-icon border-none bg-light-red margin-right-15 fs-18"><i class="fa fa-map-marker"></i></div>
                    <div class="pull-left">
                        <div class="fs-24 fw-bold padding-y-5">
                            <div>Tegal</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-4 padding-10 shadow-bottom-right bg-red hover-bg-lightest hover-text-red hover-border-red rounded-xs has-fancy-corner">
                <div class="corner-light top-right"></div>
                <div class="corner-dark bottom-left"></div>
                <div class="clearfix text-left">
                    <div class="pull-left circle-icon border-none bg-light-red margin-right-15 fs-18"><i class="fa fa-map-marker"></i></div>
                    <div class="pull-left">
                        <div class="fs-24 fw-bold padding-y-5">
                            <div>Yogyakarta</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-4 padding-10 shadow-bottom-right bg-red hover-bg-lightest hover-text-red hover-border-red rounded-xs has-fancy-corner">
                <div class="corner-light top-right"></div>
                <div class="corner-dark bottom-left"></div>
                <div class="clearfix text-left">
                    <div class="pull-left circle-icon border-none bg-light-red margin-right-15 fs-18"><i class="fa fa-map-marker"></i></div>
                    <div class="pull-left">
                        <div class="fs-24 fw-bold padding-y-5">
                            <div>Lainnya</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="padding-y-30">
    <h1 class="text-red text-center">Sekilas Tryout Simulnas</h1>
    <div class="text-center">
        <hr class="border-red border-top border-thin margin-0 inline-block text-middle" style="width: 40px;">
    </div>
    <div class="container">
        <div class="box box-space-lg box-gutter box-break-sm box-equal">
            <div class="box-4 padding-5 border-light bg-lighter">
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/1.jpg" style="width:100%;height:auto">
            </div>
            <div class="box-4 padding-5 border-light bg-lighter">
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/2.jpg" style="width:100%;height:auto">
            </div>
            <div class="box-4 padding-5 border-light bg-lighter">
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/3.jpg" style="width:100%;height:auto">
            </div>
            <div class="box-4 padding-5 border-light bg-lighter">
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/4.jpg" style="width:100%;height:auto">
            </div>
            <div class="box-4 padding-5 border-light bg-lighter">
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/5.jpg" style="width:100%;height:auto">
            </div>
            <div class="box-4 padding-5 border-light bg-lighter">
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/6.jpg" style="width:100%;height:auto">
            </div>
        </div>
    </div>
</div>

<div class="margin-top-100"></div>

<div class="has-bg-img text-lightest text-center padding-y-15">
    <div class="bg-img" style="background-image: url('<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/partnership.jpg');"></div>
    <div class="bg-img red-50"></div>
    <div class="bg-img darker-50"></div>
    <div class="fs-18 m-fs-13 padding-x-20">
        <span class="margin-right-10">Mari tumbuh bersama, join partnership dengan kami melalui</span>
    </div>
    <div class="margin-top-5"></div>
    <div class="fs-22 m-fs-16 padding-x-20 fw-bold">
        <div class="circle-icon border-none darker-50 margin-right-5"><i class="fa fa-envelope"></i></div>
        <p>partnership@simulasinasional.com</p>
    </div>
 </div>
