<?php

use app_simulnas\assets_manager\RequiredAsset;
use yii\helpers\Html;
use technosmart\yii\widgets\Menu as MenuWidget;
use yii\widgets\Breadcrumbs;
use technosmart\models\Menu;

RequiredAsset::register($this);
$this->beginPage();

if (Yii::$app->session->hasFlash('success'))
    $this->registerJs(
        'fn.alert("Success", "' . Yii::$app->session->getFlash('success') . '", "success");',
        3
    );
if (Yii::$app->session->hasFlash('info'))
    $this->registerJs(
        'fn.alert("Info", "' . Yii::$app->session->getFlash('info') . '", "info");',
        3
    );
if (Yii::$app->session->hasFlash('warning'))
    $this->registerJs(
        'fn.alert("Warning", "' . Yii::$app->session->getFlash('warning') . '", "warning");',
        3
    );
if (Yii::$app->session->hasFlash('error'))
    $this->registerJs(
        'fn.alert("Error", "' . Yii::$app->session->getFlash('error') . '", "danger");',
        3
    );
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="<?= Yii::$app->params['app.description'] ?>">
        <meta name="keywords" content="<?= Yii::$app->params['app.keywords'] ?>">
        <meta name="author" content="<?= Yii::$app->params['app.author'] ?>">
        <?= Html::csrfMetaTags() ?>
        <meta name="base-url" content="<?= yii\helpers\Url::home(true) ?>">
        <title><?= $this->title ? strip_tags($this->title) . ' | ' : null ?><?= Yii::$app->params['app.name'] ?><?= Yii::$app->params['app.description'] ? ' - ' . Yii::$app->params['app.description'] : null ?></title>
        <link rel="shortcut icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.ico">
        <?php $this->head() ?>
    </head>

    <body>
        <?php $this->beginBody() ?>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- START @ALERT & CONFIRM -->
        <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="title-alert">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-alert"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="title-confirm">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-confirm"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default modal-yes" data-dismiss="modal">Yes</button>
                        <button type="button" class="btn btn-default modal-no" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /END @ALERT & CONFIRM -->

        <div class="wrapper">
            <div class="header bg-red hidden-sm-less">
                <div class="container">
                    <div class="pull-left padding-top-15">
                        <a href="<?= Yii::$app->urlManager->createUrl('site/index') ?>" class="a-nocolor">
                            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo.png" width="30px;" class="circle padding-5 margin-right-5 bg-lightest text-middle">
                            <span class="fw-bold fs-18 text-middle"><?= Yii::$app->params['app.name'] ?></span>
                        </a>
                    </div>
                    <?php $menu['header'] = [
                        /*[
                            'code' => 'peserta',
                            'id' => '1',
                            'parent' => null,
                            'order' => '0',
                            'enable' => '1',
                            'title' => '<i class="fa fa-rocket margin-right-5"></i> Pendaftaran',
                            'icon' => null,
                            'url' => null,
                            'url_controller' => 'peserta',
                            'url_action' => 'pendaftaran',
                            'param_key_1' => null,
                            'param_value_1' => null,
                            'param_key_2' => null,
                            'param_value_2' => null,
                            'param_key_3' => null,
                            'param_value_3' => null,
                        ],
                        [
                            'code' => 'peserta',
                            'id' => '2',
                            'parent' => null,
                            'order' => '0',
                            'enable' => '1',
                            'title' => '<i class="fa fa-tasks margin-right-5"></i> Konfirmasi',
                            'icon' => null,
                            'url' => null,
                            'url_controller' => 'peserta',
                            'url_action' => 'konfirmasi',
                            'param_key_1' => null,
                            'param_value_1' => null,
                            'param_key_2' => null,
                            'param_value_2' => null,
                            'param_key_3' => null,
                            'param_value_3' => null,
                        ],*/
                        [
                            'code' => 'peserta',
                            'id' => '3',
                            'parent' => null,
                            'order' => '0',
                            'enable' => '1',
                            'title' => '<i class="fa fa-qrcode margin-right-5"></i> Cek Nilai',
                            'icon' => null,
                            'url' => null,
                            'url_controller' => 'peserta',
                            'url_action' => 'cek-status',
                            'param_key_1' => null,
                            'param_value_1' => null,
                            'param_key_2' => null,
                            'param_value_2' => null,
                            'param_key_3' => null,
                            'param_value_3' => null,
                        ],
                        /*[
                            'code' => 'peserta',
                            'id' => '3',
                            'parent' => null,
                            'order' => '0',
                            'enable' => '1',
                            'title' => 'Pendaftaran sedang ditutup',
                            'icon' => null,
                            'url' => null,
                            'url_controller' => 'site',
                            'url_action' => 'index',
                            'param_key_1' => null,
                            'param_value_1' => null,
                            'param_key_2' => null,
                            'param_value_2' => null,
                            'param_key_3' => null,
                            'param_value_3' => null,
                        ],*/
                    ]; ?>
                    <?= MenuWidget::widget([
                        'items' => Menu::menuTree($menu['header'], null, true),
                        'options' => [
                            'class' => 'pull-right menu-x menu-space-y-20 fs-14
                                menu-hover-bg-lighter menu-hover-text-red menu-active-bg-lightest menu-active-text-red
                                submenu-text-red submenu-hover-bg-lighter submenu-hover-text-red submenu-active-bg-lightest submenu-active-text-red',
                        ],
                        'activateItems' => true,
                        'openParents' => true,
                        'parentsCssClass' => 'has-submenu',
                        'encodeLabels' => false,
                        'labelTemplate' => '<a>{label}</a>',
                        'hideEmptyItems' => true,
                    ]); ?>
                </div>
            </div>

            <div class="header visible-sm-less">
                <div class="text-center padding-y-10 bg-red darker-20">
                    <a href="<?= Yii::$app->urlManager->createUrl("site/index") ?>" class="a-nocolor">
                        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo.png" width="30px;" class="circle padding-5 margin-right-5 bg-lightest text-middle">
                        <span class="fw-bold fs-18 text-middle"><?= Yii::$app->params['app.name'] ?></span>
                    </a>
                </div>
                <div class="bg-red text-center padding-top-15">
                    <?= MenuWidget::widget([
                        'items' => Menu::menuTree($menu['header'], null, true),
                        'options' => [
                            'class' => 'menu-x menu-space-y-10 fs-13 inline-block margin-bottom-min-5
                                menu-active-border-lightest
                                submenu-text-red submenu-hover-bg-lighter submenu-hover-text-red submenu-active-bg-lightest submenu-active-text-red',
                        ],
                        'activateItems' => true,
                        'openParents' => true,
                        'parentsCssClass' => 'has-submenu',
                        'encodeLabels' => false,
                        'labelTemplate' => '<a>{label}</a>',
                        'hideEmptyItems' => true,
                    ]); ?>
                </div>
            </div>

            <div class="body">
                <div class="page-wrapper">
                    <?= $content ?>
                </div>
            </div>

            <footer class="footer has-bg-img padding-y-10 text-lightest text-center border-darkest border-bottom">
                <div class="bg-img" style="background-image: url('<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/footer.jpg');"></div>
                <div class="bg-img darker-50"></div>
                <div class="bg-img darker-50"></div>
                <div class="container">
                    <div class="box box-space-lg box-gutter box-break-sm box-equal text-center">
                        <div class="box-6 padding-30">
                            <div>
                                <div class="circle-icon border-none darker-50 fs-50 m-fs-30"><i class="fa fa-instagram"></i></div>
                            </div>
                            <div class="margin-top-15"></div>
                            <div>instagram</div>
                            <div class="margin-top-15"></div>
                            <div class="fs-40 m-fs-18 fw-bold">
                                <span class="padding-y-10 padding-x-20 rounded-lg border-dark">@simulasinasional</span>
                            </div>
                        </div>
                        <div class="box-6 padding-30">
                            <div>
                                <div class="circle-icon border-none darker-50 fs-50 m-fs-30"><i class="fa fa-commenting-o"></i></div>
                            </div>
                            <div class="margin-top-15"></div>
                            <div>line</div>
                            <div class="margin-top-15"></div>
                            <div class="fs-40 m-fs-18 fw-bold">
                                <span class="padding-y-10 padding-x-20 rounded-lg border-dark">@simulasinasional</span>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <?php if (YII_ENV == 'prod') : ?>
            <div class="back-to-top bg-lighter fs-13 border-azure text-center text-azure rounded-xs padding-y-10" style="width:40px; height:40px;">▲</div>
        <?php elseif (YII_ENV == 'dev') : ?>
            <div class="back-to-top bg-lighter fs-13 border-red text-center text-red rounded-xs padding-y-10" style="width:40px; height:40px;">▲</div>
        <?php endif; ?>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>