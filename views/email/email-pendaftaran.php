<div style="box-sizing:border-box;padding:15px!important;color: #595959;background-color: #fafafa">
    <div style="box-sizing:border-box;width:100%;max-width:600px;margin:30px auto!important;background:#fff;border:1px solid #b83333;border-radius: 8px">
        <div style="box-sizing:border-box;padding:15px!important;text-align:center;">
            <img alt="<?= Yii::$app->params['app.name'] ?> logo" src="https://simulasinasional.com/img/kartu-ujian.png" style="width:100%;max-width:300px;height:auto">
        </div>

        <hr style="box-sizing: content-box;height:0;margin-top:15px;margin-bottom:15px;border:0;border-top: 1px solid #ededed">

        <h1 style="box-sizing:border-box;margin-right:0;margin-left:0;max-width:100%;font-weight:700;word-break:normal;word-wrap:break-word;margin-top:15px;margin-bottom:15px;font-size:32px;line-height:38px;text-align:center;color:#b83333;padding:0 30px">Pendaftaran Sukses, Cara Pembayaran & Cara Konfirmasi</h1>

        <hr style="box-sizing: content-box;height:0;margin-top:15px;margin-bottom:15px;border:0;border-top: 1px solid #ededed">

        <div style="box-sizing:border-box;padding:15px!important;">
            <h4 style="box-sizing:border-box;max-width:100%;font-weight:700;word-break:normal;word-wrap:break-word;margin-top:15px;margin-bottom:15px;font-size:16px;line-height:22px;">Pendaftaran Sukses !</h4>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;"><b style="box-sizing:border-box;font-weight:bolder;">Selamat !</b> Anda berhasil melakukan pendaftaran <?= Yii::$app->params['app.name'] ?>. Pastikan data peserta yang kamu isi telah sesuai.</p>

            <table class="table" style="box-sizing:border-box;width:100%;margin-bottom:15px;border-collapse:collapse;">
                <thead style="box-sizing:border-box;">
                    <tr style="box-sizing:border-box;border-bottom: 1px solid #dbdbdb;">
                        <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;">Kode & Nama Peserta</th>
                        <!-- <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;">Nama</th> -->
                        <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;min-width:90px!important;word-break:break-all">Kontak</th>
                        <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;">Jenis</th>
                        <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;">Lokasi</th>
                    </tr>
                </thead>
                <tbody style="box-sizing:border-box;">
                    <tr style="box-sizing:border-box;">
                        <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                            <div class="fw-bold text-red fs-18" style="box-sizing:border-box;font-size:18px;line-height:24px;font-weight:700;color:#b83333;"><?= $model['peserta']->kode ?></div>
                            <div class="fw-bold fs-14" style="box-sizing:border-box;font-size:14px;line-height:20px;font-weight:700;"><?= $model['peserta']->nama ?></div>
                        </td>
                        <!-- <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                        </td> -->
                        <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;min-width:90px!important;word-break:break-all">
                            <div style="box-sizing:border-box;"><?= $model['peserta']->email ?></div>
                            <div style="box-sizing:border-box;"><?= $model['peserta']->handphone ?></div>
                        </td>
                        <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                            <div class="fw-bold" style="box-sizing:border-box;font-weight:700;"><?= $model['peserta']->periodeJenis->nama ?></div>
                            <div style="box-sizing:border-box;">
                                <span class="text-gray" style="box-sizing:border-box;color:#878787;">Rp </span>
                                <span class="text-spring" style="box-sizing:border-box;color:#33b876;"><?= number_format($model['peserta']->harga, 2) ?></span>
                            </div>
                        </td>
                        <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                            <div class="fw-bold" style="box-sizing:border-box;font-weight:700;"><?= $model['peserta']->periodeKota->nama ?></div>
                        </td>
                    </tr>
                    <?php foreach ($model['peserta']->pesertaTambahans as $key => $pesertaTambahan) : ?>
                        <tr style="box-sizing:border-box;border-top: 1px solid #ededed;">
                            <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                                <div class="fw-bold text-red fs-18" style="box-sizing:border-box;font-size:18px;line-height:24px;font-weight:700;color:#b83333;"><?= $pesertaTambahan->kode ?></div>
                                <div class="fw-bold fs-14" style="box-sizing:border-box;font-size:14px;line-height:20px;font-weight:700;"><?= $pesertaTambahan->nama ?></div>
                            </td>
                            <!-- <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                            </td> -->
                            <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;min-width:90px!important;word-break:break-all">
                                <div style="box-sizing:border-box;"><?= $pesertaTambahan->email ?></div>
                                <div style="box-sizing:border-box;"><?= $pesertaTambahan->handphone ?></div>
                            </td>
                            <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                                <div class="fw-bold" style="box-sizing:border-box;font-weight:700;"><?= $pesertaTambahan->periodeJenis->nama ?></div>
                                <div style="box-sizing:border-box;">
                                    <span class="text-gray" style="box-sizing:border-box;color:#878787;">Rp </span>
                                    <span class="text-spring" style="box-sizing:border-box;color:#33b876;"><?= number_format($pesertaTambahan->harga, 2) ?></span>
                                </div>
                            </td>
                            <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                                <div class="fw-bold" style="box-sizing:border-box;font-weight:700;"><?= $pesertaTambahan->periodeKota->nama ?></div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <h4 style="box-sizing:border-box;max-width:100%;font-weight:700;word-break:normal;word-wrap:break-word;margin-top:15px;margin-bottom:15px;font-size:16px;line-height:22px;">Cara Pembayaran : Bank Transfer</h4>

            <ul class="list-unstyled" style="box-sizing:border-box;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;padding-left:0;list-style-type:none;list-style-position:outside;list-style-image:none;">
                <li style="box-sizing:border-box;">
                    <span class="inline-block" style="display:inline-block;width: 150px;">Total tiket :</span>
                    <span class="fw-bold text-dark" style="font-weight:700;color:#363636;"><?= $model['peserta']->jumlah_tiket ?></span>
                </li>
                <!-- <li style="box-sizing:border-box;">
                    <span class="inline-block" style="display:inline-block;width: 150px;">Harga awal :</span>
                    <span class="" style="">Rp <del><?= number_format($model['peserta']->tagihan, 2) ?></del></span>
                </li>
                <li style="box-sizing:border-box;">
                    <span class="inline-block" style="display:inline-block;width: 150px;">Anda hemat :</span>
                    <span class="text-azure" style="color:#3376b8;">Rp <?= number_format((int)($model['peserta']->tagihan - $model['peserta']->tagihan), 2) ?></span>
                </li> -->
                <li style="box-sizing:border-box;">
                    <span class="inline-block" style="display:inline-block;width: 150px;">Total tagihan :</span>
                    <span class="fw-bold text-spring" style="font-weight:700;color:#33b876;">Rp <?= number_format($model['peserta']->tagihan, 2) ?></span>
                </li>
                <li style="box-sizing:border-box;">
                    <span class="inline-block" style="display:inline-block;width: 150px;">Status pembayaran :</span>
                    <span class="fw-bold text-red" style="font-weight:700;color:#b83333;"><?= $model['peserta']->status_bayar ?></span>
                </li>
            </ul>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;">Segera lakukan transfer sebesar <span class="fw-bold text-spring" style="box-sizing:border-box;font-weight:700;color:#33b876;">Rp <?= number_format($model['peserta']->tagihan, 2) ?></span> ke salah satu rekening dibawah ini:</p>

            <ul class="list-unstyled" style="box-sizing:border-box;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;padding-left:0;list-style-type:none;list-style-position:outside;list-style-image:none;">
                <li style="box-sizing:border-box;margin-bottom:10px!important">
                    1. <span class="fw-bold" style="box-sizing:border-box;font-weight:700;">Bank Mandiri</span>
                    <br>
                    <span class="margin-x-5" style="box-sizing:border-box;margin-right:5px!important;margin-left:5px!important;"></span>
                    <span class="fw-bold text-azure" style="box-sizing:border-box;font-weight:700;color:#3376b8;">1320021383733</span>
                    <br>
                    <span class="margin-x-5" style="box-sizing:border-box;margin-right:5px!important;margin-left:5px!important;"></span>
                    (a.n. <i style="box-sizing:border-box;">Siti Dewi Winata</i>)
                </li>
                <li style="box-sizing:border-box;margin-bottom:10px!important">
                    2. <span class="fw-bold" style="box-sizing:border-box;font-weight:700;">Bank BCA</span>
                    <br>
                    <span class="margin-x-5" style="box-sizing:border-box;margin-right:5px!important;margin-left:5px!important;"></span>
                    <span class="fw-bold text-azure" style="box-sizing:border-box;font-weight:700;color:#3376b8;">5150309087</span>
                    <br>
                    <span class="margin-x-5" style="box-sizing:border-box;margin-right:5px!important;margin-left:5px!important;"></span>
                    (a.n. <i style="box-sizing:border-box;">Siti Dewi Winata</i>)
                </li>
                <li style="box-sizing:border-box;margin-bottom:10px!important">
                    3. <span class="fw-bold" style="box-sizing:border-box;font-weight:700;">Bank BNI</span>
                    <br>
                    <span class="margin-x-5" style="box-sizing:border-box;margin-right:5px!important;margin-left:5px!important;"></span>
                    <span class="fw-bold text-azure" style="box-sizing:border-box;font-weight:700;color:#3376b8;">1997212129</span>
                    <br>
                    <span class="margin-x-5" style="box-sizing:border-box;margin-right:5px!important;margin-left:5px!important;"></span>
                    (a.n. <i style="box-sizing:border-box;">Siti Dewi Winata</i>)
                </li>
                <li style="box-sizing:border-box;margin-bottom:10px!important">
                    4. <span class="fw-bold" style="box-sizing:border-box;font-weight:700;">Bank BRI</span>
                    <br>
                    <span class="margin-x-5" style="box-sizing:border-box;margin-right:5px!important;margin-left:5px!important;"></span>
                    <span class="fw-bold text-azure" style="box-sizing:border-box;font-weight:700;color:#3376b8;">438901010287535</span>
                    <br>
                    <span class="margin-x-5" style="box-sizing:border-box;margin-right:5px!important;margin-left:5px!important;"></span>
                    (a.n. <i style="box-sizing:border-box;">Siti Dewi Winata</i>)
                </li>
            </ul>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;">Total pembayaran harus sesuai dengan tagihan yang sudah diberikan, tidak boleh kurang ataupun lebih, jika tidak sesuai maka pembayaran <b style="box-sizing:border-box;font-weight:bolder;">TIDAK</b> akan diproses.</p>

            <h4 style="box-sizing:border-box;max-width:100%;font-weight:700;word-break:normal;word-wrap:break-word;margin-top:15px;margin-bottom:15px;font-size:16px;line-height:22px;">Cara Konfirmasi</h4>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;">Lakukan konfirmasi pembayaran disini <br> <a href="https://simulasinasional.com/peserta/konfirmasi" class="button border-red bg-red text-lightest rounded-sm" style="box-sizing:border-box;text-decoration:none;cursor: pointer;color: white;background-color: #b83333;border: 1px solid #b83333;border-radius: 8px;display: inline-block;text-align: center;white-space: nowrap;padding: 5px 15px;margin-top:5px;font-size: 18px;">Konfirmasi</a></p>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;">atau klik tautan ini <br> https://simulasinasional.com/peserta/konfirmasi</p>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;">Tiket akan kami kirimkan paling lambat 3 hari kerja setelah kamu melakukan konfirmasi pembayaran.</p>

            <h4 style="box-sizing:border-box;max-width:100%;font-weight:700;word-break:normal;word-wrap:break-word;margin-top:15px;margin-bottom:15px;font-size:16px;line-height:22px;">Punya Pertanyaan ?</h4>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;">Pertanyaan lebih lanjut dapat ditanyakan melalui akun LINE : @simulasinasional (pakai @), atau klik <a href="https://line.me/ti/p/~@simulasinasional" class="fw-bold" style="box-sizing:border-box;background-color:transparent;color:#3376b8;text-decoration:none;font-weight:700;">disini</a></p>
        </div>
    </div>
</div>