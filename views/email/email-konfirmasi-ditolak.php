<div style="box-sizing:border-box;padding:15px!important;color: #595959;background-color: #fafafa">
    <div style="box-sizing:border-box;width:100%;max-width:600px;margin:30px auto!important;background:#fff;border:1px solid #b83333;border-radius: 8px">
        <div style="box-sizing:border-box;padding:15px!important;text-align:center;">
            <img alt="<?= Yii::$app->params['app.name'] ?> logo" src="https://simulasinasional.com/img/kartu-ujian.png" style="width:100%;max-width:300px;height:auto">
        </div>

        <hr style="box-sizing: content-box;height:0;margin-top:15px;margin-bottom:15px;border:0;border-top: 1px solid #ededed">

        <h1 style="box-sizing:border-box;margin-right:0;margin-left:0;max-width:100%;font-weight:700;word-break:normal;word-wrap:break-word;margin-top:15px;margin-bottom:15px;font-size:32px;line-height:38px;text-align:center;color:#b83333;padding:0 30px">Pembayaran Ditolak/Bermasalah</h1>

        <hr style="box-sizing: content-box;height:0;margin-top:15px;margin-bottom:15px;border:0;border-top: 1px solid #ededed">

        <div style="box-sizing:border-box;padding:15px!important;">
            <?php if (isset($model['peserta']->catatan)) : ?>
                <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;font-size: 16px"><b>Keterangan dari Admin:</b><br> <span style=";color:#3376b8"><?= $model['peserta']->catatan ?></span></p>
            <?php else : ?>
                <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;font-size: 16px"><span style=";color:#3376b8">Pembayaran Kamu gagal diverifikasi.</span></p>
            <?php endif; ?>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;">Detail tiket :</p>

            <ul class="list-unstyled" style="box-sizing:border-box;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;padding-left:0;list-style-type:none;list-style-position:outside;list-style-image:none;">
                <li style="box-sizing:border-box;">
                    <span class="inline-block" style="display:inline-block;width: 150px;">Total tiket :</span>
                    <span class="fw-bold text-dark" style="font-weight:700;color:#363636;"><?= $model['peserta']->jumlah_tiket ?></span>
                </li>
                <li style="box-sizing:border-box;">
                    <span class="inline-block" style="display:inline-block;width: 150px;">Total tagihan :</span>
                    <span class="fw-bold text-spring" style="font-weight:700;color:#33b876;">Rp <?= number_format($model['peserta']->tagihan, 2) ?></span>
                </li>
                <li style="box-sizing:border-box;">
                    <span class="inline-block" style="display:inline-block;width: 150px;">Status pembayaran :</span>
                    <span class="fw-bold text-red" style="font-weight:700;color:#b87633;"><?= $model['peserta']->status_bayar ?></span>
                </li>
            </ul>

            <table class="table" style="box-sizing:border-box;width:100%;margin-bottom:15px;border-collapse:collapse;">
                <thead style="box-sizing:border-box;">
                    <tr style="box-sizing:border-box;border-bottom: 1px solid #dbdbdb;">
                        <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;">Kode & Nama Peserta</th>
                        <!-- <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;">Nama</th> -->
                        <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;min-width:90px!important;word-break:break-all">Kontak</th>
                        <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;">Jenis</th>
                        <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;">Lokasi</th>
                    </tr>
                </thead>
                <tbody style="box-sizing:border-box;">
                    <tr style="box-sizing:border-box;">
                        <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                            <div class="fw-bold text-red fs-18" style="box-sizing:border-box;font-size:18px;line-height:24px;font-weight:700;color:#b83333;"><?= $model['peserta']->kode ?></div>
                            <div class="fw-bold fs-14" style="box-sizing:border-box;font-size:14px;line-height:20px;font-weight:700;"><?= $model['peserta']->nama ?></div>
                        </td>
                        <!-- <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                        </td> -->
                        <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;min-width:90px!important;word-break:break-all">
                            <div style="box-sizing:border-box;"><?= $model['peserta']->email ?></div>
                            <div style="box-sizing:border-box;"><?= $model['peserta']->handphone ?></div>
                        </td>
                        <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                            <div class="fw-bold" style="box-sizing:border-box;font-weight:700;"><?= $model['peserta']->periodeJenis->nama ?></div>
                            <div style="box-sizing:border-box;">
                                <span class="text-gray" style="box-sizing:border-box;color:#878787;">Rp </span>
                                <span class="text-spring" style="box-sizing:border-box;color:#33b876;"><?= number_format($model['peserta']->harga, 2) ?></span>
                            </div>
                        </td>
                        <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                            <div class="fw-bold" style="box-sizing:border-box;font-weight:700;"><?= $model['peserta']->periodeKota->nama ?></div>
                        </td>
                    </tr>
                    <?php foreach ($model['peserta']->pesertaTambahans as $key => $pesertaTambahan) : ?>
                        <tr style="box-sizing:border-box;border-top: 1px solid #ededed;">
                            <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                                <div class="fw-bold text-red fs-18" style="box-sizing:border-box;font-size:18px;line-height:24px;font-weight:700;color:#b83333;"><?= $pesertaTambahan->kode ?></div>
                                <div class="fw-bold fs-14" style="box-sizing:border-box;font-size:14px;line-height:20px;font-weight:700;"><?= $pesertaTambahan->nama ?></div>
                            </td>
                            <!-- <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                            </td> -->
                            <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;min-width:90px!important;word-break:break-all">
                                <div style="box-sizing:border-box;"><?= $pesertaTambahan->email ?></div>
                                <div style="box-sizing:border-box;"><?= $pesertaTambahan->handphone ?></div>
                            </td>
                            <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                                <div class="fw-bold" style="box-sizing:border-box;font-weight:700;"><?= $pesertaTambahan->periodeJenis->nama ?></div>
                                <div style="box-sizing:border-box;">
                                    <span class="text-gray" style="box-sizing:border-box;color:#878787;">Rp </span>
                                    <span class="text-spring" style="box-sizing:border-box;color:#33b876;"><?= number_format($pesertaTambahan->harga, 2) ?></span>
                                </div>
                            </td>
                            <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                                <div class="fw-bold" style="box-sizing:border-box;font-weight:700;"><?= $pesertaTambahan->periodeKota->nama ?></div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;font-size: 16px">Silahkan melakukan konfirmasi ulang atau hubungi admin melalui akun LINE : @simulasinasional (pakai @), atau klik <a href="https://line.me/ti/p/~@simulasinasional" class="fw-bold" style="box-sizing:border-box;background-color:transparent;color:#3376b8;text-decoration:none;font-weight:700;">disini</a></p>
        </div>
    </div>
</div>