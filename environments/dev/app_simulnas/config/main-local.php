<?php
$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=tryout',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'mail' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/views',
            'htmlLayout' => 'layouts/email-html',
            'textLayout' => 'layouts/email-text',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'pradana.fandy@gmail.com',
                'password' => 'dndnqyxlvnjetzyo',
                'port' => '587',
                'encryption' => 'tls',
            ],
        ],
        'mailDeveloper' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/views',
            'htmlLayout' => 'layouts/email-html',
            'textLayout' => 'layouts/email-text',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'pradana.fandy@gmail.com',
                'password' => 'dndnqyxlvnjetzyo',
                'port' => '587',
                'encryption' => 'tls',
            ],
        ],
    ],
];

return $config;