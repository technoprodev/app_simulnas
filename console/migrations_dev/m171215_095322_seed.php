<?php

use yii\db\Migration;

class m171215_095322_seed extends Migration
{
    public function safeUp()
    {
        $this->execute(file_get_contents('seed.sql', true));
    }

    public function safeDown()
    {
        $this->execute(file_get_contents('seed-down.sql', true));
    }
}
