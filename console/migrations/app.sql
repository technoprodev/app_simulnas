-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

INSERT INTO `configuration_file` (`target`, `alias_upload_root`, `alias_download_base_url`) VALUES
('dev-file',  '@upload-dev-file', '@download-dev-file');

INSERT INTO `sequence` (`target`, `last_sequence`, `first`, `second`, `third`) VALUES
('dev-id_combination',  '0A', '0',  'A',  NULL);

DROP TABLE IF EXISTS `user_extend`;
CREATE TABLE `user_extend` (
  `id` int(11) NOT NULL,
  `sex` enum('Male','Female') NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `user_extend_ibfk_1` FOREIGN KEY (`id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user_extend` (`id`, `sex`) VALUES
(1, 'Male');
